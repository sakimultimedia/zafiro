<?php require_once("includes/connection.php"); ?>
<?php include "header.php" ?>
<!--
=========================================PHP=================================================
====================================datos basicos============================================
=========================================PHP=================================================
-->

<?php

if(!isset($_SESSION["session_username"])){
// echo "Session is set"; // for testing purposes
	echo "<script> location.href='login.php'; </script>";
}

#====================================datos basicos============================================
#====================================datos basicos============================================
#====================================datos basicos============================================
if ($profesion == "1") {
    include "profesiones/neuropsicologia/nueva-historia_neuropsicologia_Part1.php";
}elseif ($profesion == "2") {
    include "profesiones/medicina_veterinaria/nueva-historia_veterinaria_Part1.php";
}

?>


<!--====================================
========================================
Sección de menú -->

<section id="change-section">
	<div class="container" style="display: flex; flex-wrap:wrap;justify-content: flex-end;">
	<!--
		<a href="nueva-historia.php">Datos Básicos</a>
		<a href="datos-procedimiento.php">Info. de la orden</a>
		<a href="historia.php">Hist. Clínica</a>
	-->
		<?php 
		#restringir acceso---------------------------------
		if ($_SESSION["session_type"] != 'administrador') {
			echo '<a href="nueva-historia.php">Datos Básicos</a>';
			echo '<a href="info-orden.php">Info. de la orden</a>'; 
		}else{
			echo '<a href="nueva-historia.php" style="background-color:#FF5831">Datos Básicos</a>';
			echo '<a href="datos-procedimiento.php">Info. de la orden</a>';
			echo '<a href="historia.php">Hist. Clínica</a>';
		}	
		?>	
	</div>
</section>


<!--
		==========================================Datos básicos================================================
		==========================================Datos básicos================================================
		==========================================Datos básicos================================================
		==========================================Datos básicos================================================
		==========================================Datos básicos================================================
		==========================================Datos básicos================================================
		==========================================Datos básicos================================================
		==========================================Datos básicos================================================
-->

	<?php
	if ($profesion == "1") {
	    include "profesiones/neuropsicologia/nueva-historia_neuropsicologia_Part2.php";
	}elseif ($profesion == "2") {
	    include "profesiones/medicina_veterinaria/nueva-historia_veterinaria_Part2.php";
	}
	?>

	<!--===========================================
	===============================================
	Footer -->

	<?php include "footer.php" ?>

	
	

<?php require_once("includes/connection.php"); ?>
<?php include "header.php" ?>


<?php 
	#restringir acceso---------------------------------
if ($_SESSION["session_type"] != 'administrador') {
	header("Location: historia-estatica.php");
}


if ($profesion == "1") {
    include "profesiones/neuropsicologia/datos-procedimiento_neuropsicologia_Part1.php";
}elseif ($profesion == "2") {
    include "profesiones/medicina_veterinaria/datos-procedimiento_veterinaria_Part1.php";
}

?>
<!--===========================================
===============================================
Seccion 1 -->

<section id="change-section">
	<div class="container" style="display: flex; flex-wrap:wrap;justify-content: flex-end;">

	<?php 
		#restringir acceso---------------------------------
	if ($_SESSION["session_type"] != 'administrador') {
		echo '<a href="nueva-historia.php">Datos Básicos</a>';
	}else{
		echo '<a href="nueva-historia.php">Datos Básicos</a>';
		echo '<a href="datos-procedimiento.php" style="background-color:#FF5831">Info. de la orden</a>';
		echo '<a href="historia.php">Hist. Clínica</a>';
	}	
	?>	
</div>
</section>


<section id="medical_history">
	<div class="container">
		<div class="col-sm-12">
			<div class="section-label">
				Información de la Orden
			</div>
			<div class="title-separador"></div>
		</div><!-- end col -->
		
		<?php 
		$Nom1 = $db->getOne("SELECT primer_nombre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
		$Nom2 = $db->getOne("SELECT segundo_nombre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
		$Ape1 = $db->getOne("SELECT primer_apellido FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
		$Ape2 = $db->getOne("SELECT segundo_apellido FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);

		$numPaciente = $db->getOne("SELECT numero_paciente FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento,$varSessionBoss);
		$NombreCompleto = $Nom1;
		if (isset_and_empty($Nom2)) {$NombreCompleto = $NombreCompleto." ".$Nom2;}
		if (isset_and_empty($Ape1)) {$NombreCompleto = $NombreCompleto." ".$Ape1;}
		if (isset_and_empty($Ape2)) {$NombreCompleto = $NombreCompleto." ".$Ape2;} 
		?>
		<div class="row" style="padding: 0 10px;">
			<div class="big-label">
				Información de la orden del paciente <?php echo "# $numPaciente: $NombreCompleto"; ?>
			</div>
		</div>
		<?php
		if ($profesion == "1") {
		    include "profesiones/neuropsicologia/datos-procedimiento_neuropsicologia_Part2.php";
		}elseif ($profesion == "2") {
		    include "profesiones/medicina_veterinaria/datos-procedimiento_veterinaria_Part2.php";
		}
		?>

		<!-- HISTORIAL DE PROCEDIMIENTOS DEL PACIENTE-->
		<?php
		if ($profesion == "1") {
		    include "profesiones/neuropsicologia/datos-procedimiento_neuropsicologia_Part3.php";
		}elseif ($profesion == "2") {
		    include "profesiones/medicina_veterinaria/datos-procedimiento_veterinaria_Part3.php";
		}
		?>

	</div><!-- end container-->
</section>


 <!--===========================================
===============================================
Footer -->

<?php include "footer.php" ?>

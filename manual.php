<?php require_once("includes/connection.php"); ?>
<?php include "header.php" ?>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="section-label">
				Manual de uso
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<p>
				A continuación se explicará el uso de la plataforma médica web para la gestión de historias clínicas “Zafiro”. 
			</p>
			<div class="contenido-manual">
				<p>
					1.	Abrir su navegador web.<br>
					2.	Acceder a la plataforma web: http://zafiroweb.co<br>
					3.	Iniciar sesión en la plataforma utilizando el nombre de usuario y contraseña que le han sido previamente asignados al solicitar el servicio de la web app.<br>
					4.	Su nombre de usuario aparecerá arriba a la izquierda.<br>
				</p>
				<img src="" alt="Nombre de usuario"/>
				<p>
					5.	Será redirigido a la página principal en la cual tiene acceso al menú principal de Zafiro en la parte superior de la página. En la página principal hay un campo donde se pide el número de identidad del paciente. Una vez llene el espacio debe hacer click en el botón” Continuar”.
				</p>
				<img src="" alt="Buscar historia"/>
				<p>
					6.	El sistema evalúa si ese número de identidad ya existe y lo re-direcciona a una página donde puede diligenciar los datos básicos del paciente. Si el paciente es nuevo, sus datos personales estarán vacíos y deberá diligenciarlos. Si por el contrario el paciente ya existe en el sistema, los datos que fueron previamente ingresados serán cargados de la base de datos para visualizarlos.
				</p>
				<img src="" alt="Dropdown buscar"/>
				<p>
					7.	En esta pantalla de datos básicos, estos datos podrán ser modificados para actualizarlos.
				</p>
				<img src="" alt="Datos básicos"/>
				<p>
					8.	Una vez termine de llenar los datos, debe ir hasta la parte inferior de la página y buscar el botón “Guardar” para guardar los datos ingresados en la base de datos. 
				</p>
				<img src="" alt="Guardar"/>
				<p>
					9.	Se ha creado un paciente nuevo.
				</p>
			</div><!-- end contenido-manual-->

			<div class="big-label">
				Si se tiene acceso restringido (ejemplo: Asistente, secretaria, etc…) 
			</div>
			<p>
				<b>Para editar un paciente:</b>
			</p>
			<div class="contenido-manual">
				<p>
					1.	Las opciones del menú superior serán diferentes ya que serán restringidas. Este usuario puede crear un paciente nuevo siguiendo los pasos anteriores, sin embargo, solo podrá acceder y editar los datos básicos de la historia médica.<br>
					2.	En el menú superior debe hace click en “Historia” y después en “Editar paciente”.
				</p>
				<img src="" alt="Menú principal restringido"/>
				<p>
					3.	Puede buscar la historia buscando por el nombre del paciente y hacer click en “Buscar por nombre” o también puede buscar al paciente usando su número de documento y haciendo click en “Buscar por documento”. Recuerde, no debe llenar ambos campos, solo uno.
				</p>
				<img src="" alt="Buscar historia"/>
				<p>
					4.	Aparecerá nuevamente en la pantalla de datos básicos del paciente seleccionado para poderlos editar.
				</p>
				<img src="" alt="Datos básicos"/>
			</div>
			<div class="separador"></div>
			<p>
				<b>Para buscar un paciente o una historia clínica:</b>
			</p>
			<div class="contenido-manual">
				<p>
					1.	En el menú superior debe hace click en “Historia” y después en “Buscar Historia”.
				</p>
				<img src="" alt="Menú principal restringido"/>
				<p>
					2.	Puede buscar la historia buscando por el nombre del paciente y hacer click en “Buscar por nombre” o también puede buscar al paciente usando su número de documento y haciendo click en “Buscar por documento”. Recuerde, no debe llenar ambos campos, solo uno.
				</p>
				<img src="" alt="Buscar historia"/>
				<p>
					3.	Será redirigido a una página en la que podrá ver la historia clínica pero no tendrá el espacio para agregar nuevas entradas. En esta página solo se muestran los datos de usuario y la información de la consulta sin poderse editar.
				</p>
				<img src="" alt="Historia médica"/>
				<p>
					4.	Desde ahí busque en su navegador la opción de imprimir la página.
				</p>
				<img src="" alt="Descargar/imprimir"/>
			</div>
			<div class="big-label">
				Si se tiene acceso completo (Profesional a cargo de la consulta)
			</div>
		</div>
	</div>
</div>
 <!--===========================================
===============================================
Footer -->

<?php include "footer.php" ?>

<section id="procedimientos-pasados">
    <?php 

    $query_info_orden = $db->query("SELECT * FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
    $numrows = $db->numRows($query_info_orden);
    
    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
    

    for ($i=$numrows; $i > 0 ; $i--) {
        $row_info_orden = $db->fetch($query_info_orden);

        $varNumAutorizacion = $row_info_orden["numero_autorizacion"];
        $varTipoConsulta = $row_info_orden["tipo_consulta"];
        $varEspecialidad = $row_info_orden["especialidad"];
        $varProcedimiento = $row_info_orden["procedimiento"];
        $varSesiones = $row_info_orden["num_sesiones"];
        $varPersonalAtiende = $row_info_orden["personal_atiende"];
        $varDxPrev = $row_info_orden["dx_previo"];
        $varDxAdicional = $row_info_orden["dx_adicional"];

        
        $row = $db->fetch($query);
        
        $varEntidad = $row{"entidad"};
        $varRemitido = $row{"remitido_por"};
    ?>
    <div class="row">
        <div class="basic-hist">
            <?php 
            if ($varNumAutorizacion != '' && $varNumAutorizacion != ' ') 
                { 
            ?>
                    <div class="col-sm-12">
                        <div class="sub-label">
                            Información de la orden
                        </div>
                        <div class="subtitle-separador"></div>
                    </div>
            <?php                             
                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Número de autorización</div>"."<div class='the-info'>".$varNumAutorizacion."</div>"."</div>";
            
                if ($varEntidad != '' && $varEntidad != ' ') {
                    $varEntidad = $db->getOne("SELECT nombre FROM entidad WHERE id=?s",$varEntidad);
                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Entidad</div>"."<div class='the-info'>".$varEntidad."</div>"."</div>";
                }
                if ($varTipoConsulta != '' && $varTipoConsulta != ' ') {
                    $varTipoConsulta = $db->getOne("SELECT nombre FROM tipo_consulta WHERE id=?s",$varTipoConsulta);
                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Tipo de consulta</div>"."<div class='the-info'>".$varTipoConsulta."</div>"."</div>";
                }
                if ($varRemitido != '' && $varRemitido != ' ') {
                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Remitido por</div>"."<div class='the-info'>".$varRemitido."</div>"."</div>";
                }
                if ($varEspecialidad != '' && $varEspecialidad != ' ') {
                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Especialidad</div>"."<div class='the-info'>".$varEspecialidad."</div>"."</div>";
                }
                if ($varProcedimiento != '' && $varProcedimiento != ' ') {
                /*
                #En el caso que se aplique solo el id de la fila entonces se puede usar este codigo. 

                $idProcedimientoTmp = explode(" - ", $varProcedimiento);
                $idProcedimiento = $idProcedimientoTmp[0];
                $IdProcedimiento = mysqli_query($con,"SELECT * FROM cup_table WHERE id='".$idProcedimiento."'")->fetch_object()->id;
                $NombreProcedimiento = mysqli_query($con,"SELECT * FROM cup_table WHERE id='".$idProcedimiento."'")->fetch_object()->nombre;
                $varProcedimiento = $IdProcedimiento." - ".$NombreProcedimiento;
                */
                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Procedimiento</div>"."<div class='the-info'>".$varProcedimiento."</div>"."</div>";
                }
                if ($varSesiones != '' && $varSesiones != ' ') {
                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Sesiones</div>"."<div class='the-info'>".$varSesiones."</div>"."</div>";
                }
                if ($varPersonalAtiende  != '' && $varPersonalAtiende  != ' ') {
                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Personal que lo atiende</div>"."<div class='the-info'>".$varPersonalAtiende ."</div>"."</div>";
                }
                if ($varDxPrev != '' && $varDxPrev != ' ') {
                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Diagnóstico previo</div>"."<div class='the-info'>".$varDxPrev."</div>"."</div>";
                }
                if ($varDxAdicional != '' && $varDxAdicional != ' ') {
                /*
                #En el caso que se aplique solo el id de la fila entonces se puede usar este codigo. 
                
                $idDxAdicionalTmp = explode(" - ", $varDxAdicional);
                $idDxAdicional = $idDxAdicionalTmp[0];
                $IdDxAdicional = mysqli_query($con,"SELECT * FROM diagnosticos WHERE id='".$idDxAdicional."'")->fetch_object()->id;
                $NombreProcedimiento = mysqli_query($con,"SELECT * FROM diagnosticos WHERE id='".$idDxAdicional."'")->fetch_object()->nombre;
                $varDxAdicional = $IdDxAdicional." - ".$NombreProcedimiento;
                */
                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Diagnóstico adicional</div>"."<div class='the-info'>".$varDxAdicional."</div>"."</div>";
                }
            }
            ?>
            <div class="col-sm-12"> 
                <div > 
                    <a class="editar-orden" href="#basic-data" onclick="search_procediento2(<?php echo $varNumAutorizacion; ?>)">EDITAR ESTA ORDEN</a>
                </div>
            </div>
        </div><!-- end basic-hist -->
        
    </div><!-- end row -->
    <?php
    }
    ?>

</section><!-- end procedimientos pasadas -->
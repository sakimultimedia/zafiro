<form class="ajaxform" action= "<?echo $ZafiroRute?>nueva-historia.php" method="POST" id="basic-data" mensajeExito = "Los datos básicos han sido correctamente guardados." mensajeError = "Ha habido un error y tus datos no fueron guardados. intentalo más tarde.">

    <div class="container">
        <section id="info-basica">
            <div class="col-sm-12">
                <div class="section-label">
                    Datos básicos 
                </div>
                <div class="title-separador"></div>
            </div><!-- end col -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Documento de identidad <span style="color:red;">*</span>
                    </div>
                    <input value= "<?php echo $_SESSION['documento']?>" type="number" min="0" name="documento" id="documento" placeholder="Escribe el número de documento" required>
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Tipo de Documento<span style="color:red;">*</span>
                    </div>
                    <select name="ID" id="ID" class="" required>
                        <?php 
                        if (!isset($_SESSION['tipoDocumento'])) {
                            echo "<option value='' default class='selected' selected>Tipo de documento</option>";
                        }else{
                            echo "<option value=''>Tipo de documento</option>";
                        }
                        $query = $db->query("SELECT * FROM tipo_documento");
                        $numrows = $db->numRows($query);
                                #$query=mysqli_query($con,"SELECT * FROM tipo_documento");
                                #$numrows=mysqli_num_rows($query);

                        for ($i=1; $i <= $numrows ; $i++) {
                            $search = $db->getOne("SELECT nombre FROM tipo_documento WHERE id=?s",$i); 
                                    #$search = mysqli_query($con,"SELECT * FROM tipo_documento WHERE id='".$i."'");

                            if ($_SESSION['tipoDocumento'] == $i) {
                                echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                            }else{
                                echo "<option value=".$i.">".$search."</option>";
                            }
                        }
                        ?>
                    </select>
                </div><!-- end col -->  
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Primer Nombre<span style="color:red;">*</span>
                    </div>
                    <input value="<?php echo $_SESSION['Nombre_1']; ?>" type="text" name="nombre_1" id="nombre_1" placeholder="Escribe el primer nombre del paciente" required>
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Segundo Nombre
                    </div>
                    <input value="<?php echo $_SESSION['Nombre_2']; ?>" type="text" name="nombre_2" id="nombre_2" placeholder="Escribe el segundo nombre del paciente">
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Primer Apellido<span style="color:red;">*</span>
                    </div>
                    <input value="<?php echo $_SESSION['Apellido_1']; ?>" type="text" name="apellido_1" id="apellido_1" placeholder="Escribe el primer apellido del paciente" required>
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Segundo Apellido<span style="color:red;">
                    </div>
                    <input value="<?php echo $_SESSION['Apellido_2']; ?>" type="text" name="apellido_2" id="apellido_2" placeholder="Escribe el segundo apellido del paciente">
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Fecha de nacimiento<span style="color:red;">*</span>
                    </div>
                    <input value="<?php echo $_SESSION['nacimiento']; ?>" type="date" max="<?php echo today_date();?>" name="fecha-nacimiento" id="fecha-nacimiento" placeholder="Define es la fecha de nacimiento del paciente" required>
                </div><!-- end col -->

                <div class="col-sm-6">

                    <div class="top-label">
                        Edad
                    </div>
                    <p id="edad" style="font-size: 1.2em;padding-left: 15px;" onkeyup="$_SESSION['Edad'] =calcular_edad($_POST['fecha-nacimiento']);">
                        <?php 
                        if ($_SESSION['Edad'] != "" || $_SESSION['Edad'] != 0) {
                            echo $_SESSION['Edad'];
                            }else{
                            echo "La edad sera calculada automáticamente";
                        }
                        ?></p>

                        <!--
                            <input value="<?php #echo $_SESSION['Edad']; ?>" type="number" min="0" name="edad" id="edad" placeholder="Edad del paciente">
                        -->
                    </div><!-- end col -->

                </div><!-- end row -->

                <div class="row">
                    <div class="col-sm-6">
                        <div class="top-label">
                            Sexo<span style="color:red;">*</span>
                        </div>
                        <?php 
                        if ($_SESSION['Genero'] == "Femenino") {
                            ?>
                            <div class="col-sm-6">                  
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="sexo_paciente" id="femenino" value="Femenino" checked required>
                                        <div class="radio-label">
                                            Femenino
                                        </div>
                                    </label>
                                </div><!-- end radio -->    
                            </div><!-- end col -->  

                            <div class="col-sm-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="sexo_paciente" id="masculino" value="Masculino" required>
                                        <span class="radio-label">
                                            Masculino
                                        </span>                         
                                    </label>
                                </div><!-- end radio -->
                            </div><!-- end col -->  
                            <?php
                        }else if ($_SESSION['Genero'] == "Masculino") {
                            ?>
                            <div class="col-sm-6">                  
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="sexo_paciente" id="femenino" value="Femenino" required>
                                        <div class="radio-label">
                                            Femenino
                                        </div>
                                    </label>
                                </div><!-- end radio -->    
                            </div><!-- end col -->  

                            <div class="col-sm-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="sexo_paciente" id="masculino" value="Masculino" checked required>
                                        <span class="radio-label">
                                            Masculino
                                        </span>                         
                                    </label>
                                </div><!-- end radio -->
                            </div><!-- end col -->  
                            <?php
                        }else{
                            ?>
                            <div class="col-sm-6">                  
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="sexo_paciente" id="femenino" value="Femenino" required>
                                        <div class="radio-label">
                                            Femenino
                                        </div>
                                    </label>
                                </div><!-- end radio -->    
                            </div><!-- end col -->  

                            <div class="col-sm-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="sexo_paciente" id="masculino" value="Masculino" required>
                                        <span class="radio-label">
                                            Masculino
                                        </span>                         
                                    </label>
                                </div><!-- end radio -->
                            </div><!-- end col -->  
                            <?php
                        }
                        ?>
                    </div><!-- end col -->  

                    <div class="col-sm-6">
                        <div class="top-label">
                            Estado Civil<span style="color:red;">*</span>
                        </div>
                        <select name="estado-civil" id="estado-civil" class="" required>
                            <?php 
                            if (!isset($_SESSION['EstadoCivil'])) {
                                echo "<option value='' default class='selected' selected>Estado Civil</option>";
                            }else{
                                echo "<option value=''>Estado Civil</option>";
                            }
                            $query = $db->query("SELECT * FROM estado_civil");
                            $numrows = $db->numRows($query);
                                #$query=mysqli_query($con,"SELECT * FROM estado_civil");
                                #$numrows=mysqli_num_rows($query);

                            for ($i=1; $i <= $numrows ; $i++) { 
                                $search = $db->getOne("SELECT nombre FROM estado_civil WHERE id=?s",$i);
                                    #$search = mysqli_query($con,"SELECT * FROM estado_civil WHERE id='".$i."'");

                                if ($_SESSION['EstadoCivil'] == $i) {
                                    echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                                }else{
                                    echo "<option value=".$i.">".$search."</option>";
                                }
                            }
                            ?>
                        </select>
                    </div><!-- end col -->
                </div><!-- end row -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="top-label">
                            EPS
                        </div>
                        <?php 
                        if (isset_and_empty($_SESSION['EPS'])) {
                            $eps = $db->getOne("SELECT nombre FROM eps WHERE id=?s",$_SESSION['EPS']);
                        }else{$eps="";}
                        ?>
                        <input value="<?php echo $eps;?>" list="eps_list" name="eps" id="eps" data-minchars="1" class="awesomplete" placeholder="EPS"  onblur="eps_list()">
                        <datalist id="eps_list">
                            <?php 
                            $query = $db->query("SELECT * FROM eps");
                            $numrows = $db->numRows($query);
                            for ($i=1; $i <= $numrows ; $i++) { 
                                $row = $db->fetch($query);
                                echo "<option value='".$row['nombre']."'>";   
                            }
                            ?>
                        </datalist>
                    </div><!-- end col -->
            </div><!-- end row -->
        </section>
        <script type="text/javascript">
        function eps_list(){
            var ok = false;
            var value = $("#eps").val();
            var epsArray = new Array();
            var eps = document.getElementById("eps_list");

            for (i = 0; i < eps.options.length; i++) {
               epsArray[i] = eps.options[i].value;
           }

           for (i = 0; i < epsArray.length; i++) {
               if (value == epsArray[i]) {
                    ok = true;
                    break;
                }
            }

            if (!ok && $("#eps").val() != "") {
                create_popup("porfavor digita una de las eps's de la lista");
                switch (3){
                    case 1: $("#eps").val("");
                    break;

                    case 2: document.getElementById("eps").focus();
                    break;

                    case 3: $("#eps").val("");
                            document.getElementById("eps").focus();
                    break;
                }
            }
        }
        </script>
        <div class="separador"></div>

            <!--
            ==========================================Info Personal================================================
            ==========================================Info Personal================================================
            ==========================================Info Personal================================================
            ==========================================Info Personal================================================
            ==========================================Info Personal================================================
            ==========================================Info Personal================================================
            ==========================================Info Personal================================================
            ==========================================Info Personal================================================
        -->

        <section id="info-personal">
            <div class="col-sm-12">
                <div class="section-label">
                    Información Personal
                </div>
                <div class="title-separador"></div>
            </div><!-- end col -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Dirección<span style="color:red;">*</span>
                    </div>
                    <input value="<?php echo $_SESSION['Dir_1']; ?>" type="text" name="direccion_1" id="direccion_1" placeholder="Escribe la dirección del paciente" required>
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Dirección alternativa
                    </div>
                    <input value="<?php echo $_SESSION['Dir_2']; ?>" type="text" name="direccion_2" id="direccion_2" placeholder="Escribe la dirección alternativa del paciente">
                </div><!-- end col -->  
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Teléfono<span style="color:red;">*</span>
                    </div>
                    <input value="<?php echo $_SESSION['Telefono_paciente']; ?>" type="number" min="0" name="telefono" id="telefono" placeholder="Escribe el teléfono del parciente" required>
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Celular
                    </div>
                    <input value="<?php echo $_SESSION['Celular_paciente']; ?>" type="number" min="0" name="celular" id="celular" placeholder="Escribe el celular del paciente">
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Ciudad<span style="color:red;">*</span>
                    </div>

                    <?php 
                    if (isset_and_empty($_SESSION['Ciudad_paciente'])) {
                        $ciudad = $db->getOne("SELECT nombre FROM ciudad WHERE id=?s",$_SESSION['Ciudad_paciente']);
                    }else{$ciudad="";}
                    ?>
                    <input value="<?php echo $ciudad;?>" list="ciudad_list" name="ciudad" id="ciudad" data-minchars="1" class="awesomplete" onblur="ciudad_list()" required>
                    <datalist id="ciudad_list">
                        <?php 
                        $query = $db->query("SELECT * FROM ciudad");
                        $numrows = $db->numRows($query);
                        for ($i=1; $i <= $numrows ; $i++) { 
                            $row = $db->fetch($query);
                            echo "<option value='".$row['nombre']."'>";   
                        }
                        ?>
                    </datalist>
                    <script type="text/javascript">
                        function ciudad_list(){
                            var ok = false;
                            var value = $("#ciudad").val();
                            var ciudadArray = new Array();
                            var ciudad = document.getElementById("ciudad_list");

                            for (i = 0; i < ciudad.options.length; i++) {
                               ciudadArray[i] = ciudad.options[i].value;
                           }

                           for (i = 0; i < ciudadArray.length; i++) {
                               if (value == ciudadArray[i]) {
                                    ok = true;
                                    break;
                                }
                            }

                            if (!ok && $("#ciudad").val() != "") {
                                create_popup("porfavor digita una de las ciudad's de la lista");
                                switch (3){
                                    case 1: $("#ciudad").val("");
                                    break;

                                    case 2: document.getElementById("ciudad").focus();
                                    break;

                                    case 3: $("#ciudad").val("");
                                            document.getElementById("ciudad").focus();
                                    break;
                                }
                            }
                        }
                    </script>
                </div><!-- end col -->


                <div class="col-sm-6">
                    <div class="top-label">
                        Médico<span style="color:red;">*</span>
                    </div>
                    <!-- Carga médicos de la BD!!!!! -->
                    <select name="medico" id="medico" class="" required>
                        <?php 
                        echo "<option value='' default class='selected' selected>Escoge el médico</option>";
                        $query = $db->query("SELECT * FROM nombre_medico");
                        $numrows = $db->numRows($query);
                    #$query=mysqli_query($con,"SELECT * FROM nombre_medico");
                    #$numrows=mysqli_num_rows($query);

                        for ($i=1; $i <= $numrows ; $i++) { 
                            $search = $db->getOne("SELECT nombre FROM nombre_medico WHERE id=?s",$i); 
                        #$search = mysqli_query($con,"SELECT * FROM nombre_medico WHERE id='".$i."'");

                            if ($_SESSION['Medico'] == $i) {
                                echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                            }else{
                                echo "<option value=".$i.">".$search."</option>";
                            }

                        }
                        ?>
                    </select>


                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Entidad<span style="color:red;">*</span>
                    </div>


                    <!-- Carga entidad de la BD!!!!! -->
                    <select name="entidad" id="entidad" class="" required>
                        <?php 
                        echo "<option value='' default class='selected'>Con qué entidad viene</option>";
                        $query = $db->query("SELECT * FROM entidad");
                        $numrows = $db->numRows($query);
                    #$query=mysqli_query($con,"SELECT * FROM entidad");
                    #$numrows=mysqli_num_rows($query);

                        for ($i=1; $i <= $numrows ; $i++) { 
                            $search = $db->getOne("SELECT nombre FROM entidad WHERE id=?s",$i); 
                        #$search = mysqli_query($con,"SELECT * FROM entidad WHERE id='".$i."'");

                            if ($_SESSION['Entidad'] == $i) {
                                echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                            }else{
                                echo "<option value=".$i.">".$search."</option>";
                            }
                        }
                        ?>
                    </select>

                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Régimen<span style="color:red;">*</span>
                    </div>
                    <!-- Carga regimen de la BD!!!!! -->
                    <select name="regimen" id="regimen" class="" required>
                        <?php
                        echo "<option value='' default class='selected'>Régimen del paciente</option>";
                        $query = $db->query("SELECT * FROM regimen");
                        $numrows = $db->numRows($query);
                    #$query=mysqli_query($con,"SELECT * FROM regimen");
                    #$numrows=mysqli_num_rows($query);

                        for ($i=1; $i <= $numrows ; $i++) { 
                            $search = $db->getOne("SELECT nombre FROM regimen WHERE id=?s",$i); 
                        #$search = mysqli_query($con,"SELECT * FROM regimen WHERE id='".$i."'");

                            if ($_SESSION['Regimen'] == $i) {
                                echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                            }else{
                                echo "<option value=".$i.">".$search."</option>";
                            }

                        }
                        ?>
                    </select>
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-12">
                    <input type="submit" name="guardar" value="Guardar" class="send-btn">
                </div>
            </div>

        </section>

        <div class="separador"></div>

        <!--
        ==========================================Ocupación================================================
        ==========================================Ocupación================================================
        ==========================================Ocupación================================================
        ==========================================Ocupación================================================
        ==========================================Ocupación================================================
        ==========================================Ocupación================================================
        ==========================================Ocupación================================================
        ==========================================Ocupación================================================
    -->

    <section id="ocupacion">
        <div class="col-sm-12">
            <div class="section-label">
                Ocupación
            </div>
            <div class="title-separador"></div>
        </div><!-- end col -->

        <div class="row" style="padding: 0 10px;">
            <div class="big-label">
                Información del acompañante
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="top-label">
                    Remitido por
                </div>
                <input value="<?php echo $_SESSION['Remitido']; ?>" type="text" name="remitido" id="remitido" placeholder="Escribe el nombre del médico">
            </div><!-- end col -->
            <div class="col-sm-6">
                <div class="top-label">
                    Acompañante
                </div>
                <input value="<?php echo $_SESSION['AcompananteNombre']; ?>" type="text" name="acompana" id="acompana" placeholder="Escribe el nombre del acompañante">
            </div><!-- end col -->
        </div><!-- end row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="top-label">
                    Teléfono del acompañante
                </div>
                <input value="<?php echo $_SESSION['AcompananteTel']; ?>" type="number" min="0" name="acompana-tel" id="acompana-tel" placeholder="Escribe el telefono del acompañante">
            </div><!-- end col -->
            <div class="col-sm-6">
                <div class="top-label">
                    Parentesco del acompañante
                </div>
                <input value="<?php echo $_SESSION['acompananteParentezco']; ?>" type="text" name="acompana-parentezco" id="acompana-parentezco" placeholder="Escribe el parentezco con el paciente">
            </div><!-- end col -->
        </div><!-- end row -->

        <div class="row" style="padding: 0 10px;">
            <div class="big-label">
                Información del paciente
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="top-label">
                    E-mail
                </div>
                <input value="<?php echo $_SESSION['Mail']; ?>" type="e-mail" name="mail" id="mail" placeholder="Escribe el mail del paciente">
            </div><!-- end col -->  
            <div class="col-sm-6">
                <div class="top-label">
                    Ciudad de nacimiento
                </div>
                <input value="<?php echo $_SESSION['Origen']; ?>" type="text" name="origen" id="origen" placeholder="Escribe el nombre del paciente">
            </div><!-- end col -->
        </div><!-- end row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="top-label">
                    Nacionalidad                    </div>
                    <input value="<?php echo $_SESSION['Nacionalidad']; ?>" type="text" name="nacionalidad" id="nacionalidad" placeholder="Escribe la nacionalidad del paciente">
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Ocupación                    </div>
                        <input value="<?php echo $_SESSION['Ocupacion']; ?>" type="text" name="ocupacion" id="ocupacion" placeholder="Escribe la profesión del paciente">
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-sm-6">
                        <div class="top-label">
                            Empresa
                        </div>
                        <input value="<?php echo $_SESSION['Empresa']; ?>" type="text" name="empresa" id="empresa" placeholder="Escribe la empresa del paciente">
                    </div><!-- end col -->
                    <div class="col-sm-6">
                    </div><!-- end col -->
                </div><!-- end row -->

            </section>

            <div class="separador"></div>

            <!--
            ==========================================Info Familiar================================================
            ==========================================Info Familiar================================================
            ==========================================Info Familiar================================================
            ==========================================Info Familiar================================================
            ==========================================Info Familiar================================================
            ==========================================Info Familiar================================================
            ==========================================Info Familiar================================================
            ==========================================Info Familiar================================================
        -->

        <section id="familiar">
            <div class="col-sm-12">
                <div class="section-label">
                    Información Familiar
                </div>
                <div class="title-separador"></div>
            </div><!-- end col -->

            <div class="row" style="padding: 0 10px;">
                <div class="big-label">
                    Información del conyugue
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Nombre del conyugue
                    </div>
                    <input value="<?php echo $_SESSION['ConyugueNombre']; ?>" type="text" name="conyugue" id="conyugue" placeholder="Escribe el nombre del conyugue">
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Documento de identidad del conyugue
                    </div>
                    <input value="<?php echo $_SESSION['ConyugueDocumento']; ?>" type="number" min="0" name="conyugue_doc" id="conyugue_doc" placeholder="Escribe el número del id del conyugue">
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Fecha de nacimiento del conyugue
                    </div>
                    <input value="<?php echo $_SESSION['ConyugueNacimiento']; ?>" type="date" max="<?php echo today_date();?>" name="conyugue_nacim" id="conyugue_nacim" placeholder="Escribe la fecha de nacimiento del conyugue">
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Dirección
                    </div>
                    <input value="<?php echo $_SESSION['ConyugueDireccion']; ?>" type="text" name="conyugue_direccion" id="conyugue_direccion" placeholder="Escribe la dirección del conyugue">
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Teléfono del conyugue
                    </div>
                    <input value="<?php echo $_SESSION['ConyugueTelefono']; ?>" type="number" min="0" name="conyugue_tel" id="conyugue_tel" placeholder="Escribe el teléfono del conyugue">
                </div><!-- end col -->  
                <div class="col-sm-6">
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row" style="padding: 0 10px;">
                <div class="big-label">
                    Información de los hijos
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Nombre del primer hijo
                    </div>
                    <input value="<?php echo $_SESSION['Hijo1Nombre']; ?>" type="text" name="hijo_1" id="hijo_1" placeholder="Escribe el nombre del hijo">
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Nombre del segundo hijo
                    </div>
                    <input value="<?php echo $_SESSION['Hijo2Nombre']; ?>" type="text" name="hijo_2" id="hijo_2" placeholder="Escribe el nombre del hijo">
                </div><!-- end col -->

            </div><!-- end row -->

            <div class="row">               
                <div class="col-sm-6">
                    <div class="top-label">
                        Sexo del primer hijo
                    </div>
                    <?php 
                    if ($_SESSION['Hijo1Sexo'] == "Femenino") {
                        ?>
                        <div class="col-sm-6">                  
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_1" id="femenino" value="Femenino" checked>
                                    <span class="radio-label">
                                        Femenino
                                    </span>
                                </label>
                            </div><!-- end radio -->    
                        </div><!-- end col -->  

                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_1" id="Masculino" value="Masculino">
                                    <span class="radio-label">
                                        Masculino
                                    </span>                         
                                </label>
                            </div><!-- end radio -->
                        </div><!-- end col -->
                        <?php
                    }else if ($_SESSION['Hijo1Sexo'] == "Masculino") {
                        ?>
                        <div class="col-sm-6">                  
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_1" id="femenino" value="Femenino">
                                    <span class="radio-label">
                                        Femenino
                                    </span>
                                </label>
                            </div><!-- end radio -->    
                        </div><!-- end col -->  

                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_1" id="Masculino" value="Masculino" checked>
                                    <span class="radio-label">
                                        Masculino
                                    </span>                         
                                </label>
                            </div><!-- end radio -->
                        </div><!-- end col -->
                        <?php
                    }else{
                        ?>
                        <div class="col-sm-6">                  
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_1" id="femenino" value="Femenino">
                                    <span class="radio-label">
                                        Femenino
                                    </span>
                                </label>
                            </div><!-- end radio -->    
                        </div><!-- end col -->  

                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_1" id="Masculino" value="Masculino">
                                    <span class="radio-label">
                                        Masculino
                                    </span>                         
                                </label>
                            </div><!-- end radio -->
                        </div><!-- end col -->
                        <?php
                    }
                    ?>
                </div><!-- end col -->

                <div class="col-sm-6">
                    <div class="top-label">
                        Sexo del segundo hijo
                    </div>
                    <?php 
                    if ($_SESSION['Hijo2Sexo'] == "Femenino") {
                        ?>
                        <div class="col-sm-6">                  
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_2" id="femenino" value="Femenino" checked>
                                    <span class="radio-label">
                                        Femenino
                                    </span>
                                </label>
                            </div><!-- end radio -->    
                        </div><!-- end col -->  

                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_2" id="Masculino" value="Masculino">
                                    <span class="radio-label">
                                        Masculino
                                    </span>                         
                                </label>
                            </div><!-- end radio -->
                        </div><!-- end col -->
                        <?php
                    }else if ($_SESSION['Hijo2Sexo'] == "Masculino") {
                        ?>
                        <div class="col-sm-6">                  
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_2" id="femenino" value="Femenino">
                                    <span class="radio-label">
                                        Femenino
                                    </span>
                                </label>
                            </div><!-- end radio -->    
                        </div><!-- end col -->  

                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_2" id="Masculino" value="Masculino" checked>
                                    <span class="radio-label">
                                        Masculino
                                    </span>                         
                                </label>
                            </div><!-- end radio -->
                        </div><!-- end col -->
                        <?php
                    }else{
                        ?>
                        <div class="col-sm-6">                  
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_2" id="femenino" value="Femenino">
                                    <span class="radio-label">
                                        Femenino
                                    </span>
                                </label>
                            </div><!-- end radio -->    
                        </div><!-- end col -->  

                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sexo_hijo_2" id="Masculino" value="Masculino">
                                    <span class="radio-label">
                                        Masculino
                                    </span>                         
                                </label>
                            </div><!-- end radio -->
                        </div><!-- end col -->
                        <?php
                    }
                    ?>
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Edad del primer hijo
                    </div>
                    <input value="<?php echo $_SESSION['Hijo1Edad']; ?>" type="number" min="0" name="hijo_1_edad" id="hijo_1_edad" placeholder="Escribe la edad del hijo">
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Edad del segundo hijo
                    </div>
                    <input value="<?php echo $_SESSION['Hijo2Edad']; ?>" type="number" min="0" name="hijo_2_edad" id="hijo_2_edad" placeholder="Escribe la edad del hijo">
                </div><!-- end col -->

            </div><!-- end row -->

            <div class="row" style="padding: 0 10px;">
                <div class="big-label">
                    Información de los padres
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Nombre del Padre
                    </div>
                    <input value="<?php echo $_SESSION['Padre']; ?>" type="text" name="nombre_padre" id="nombre_padre" placeholder="Escribe el nombre del padre">
                </div><!-- end col -->  
                <div class="col-sm-6">
                    <div class="top-label">
                        Edad del Padre
                    </div>
                    <input value="<?php echo $_SESSION['PadreEdad']; ?>" type="number" min="0" name="edad_padre" id="edad_padre" placeholder="Escribe la edad del padre">
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="top-label">
                        Nombre de la Madre
                    </div>
                    <input value="<?php echo $_SESSION['Madre']; ?>" type="text" name="nombre_madre" id="nombre_madre" placeholder="Escribe el nombre de la madre">
                </div><!-- end col -->
                <div class="col-sm-6">
                    <div class="top-label">
                        Edad de la Madre
                    </div>
                    <input value="<?php echo $_SESSION['MadreEdad']; ?>" type="number" min="0" name="edad_madre" id="edad_madre" placeholder="Escribe la edad de la madre">
                </div><!-- end col -->
            </div><!-- end row -->

        </section>

        <div class="separador"></div>

        <!--
        ==========================================Otra Info================================================
        ==========================================Otra Info================================================
        ==========================================Otra Info================================================
        ==========================================Otra Info================================================
        ==========================================Otra Info================================================
        ==========================================Otra Info================================================
        ==========================================Otra Info================================================
        ==========================================Otra Info================================================
    -->

    <section id="otro">
        <div class="col-sm-12">
            <div class="section-label">
                Otra información
            </div>
            <div class="title-separador"></div>
        </div><!-- end col -->



        <div class="row">
            <div class="col-sm-6">
                <div class="top-label">
                    Hobbie
                </div>
                <input value="<?php echo $_SESSION['Hobbie']; ?>" type="text" name="hobbie" id="hobbie" placeholder="Escribe el hobbie">
            </div><!-- end col -->
            <div class="col-sm-6">
                <div class="top-label">
                    Etnia
                </div>
                <!-- Carga regimen de la BD!!!!! -->
                <select name="etnia" id="etnia" class="">

                    <?php 
                    if (!isset($_SESSION['Etnia'])) {
                        echo "<option value='' default class='selected' selected>Etnia del paciente</option>";
                    }else{
                        echo "<option value=''>Etnia del paciente</option>";
                    }
                    $query = $db->query("SELECT * FROM etnia");
                    $numrows = $db->numRows($query);

                        #$query=mysqli_query($con,"SELECT * FROM etnia");
                        #$numrows=mysqli_num_rows($query);

                    for ($i=1; $i <= $numrows ; $i++) { 
                        $search = $db->getOne("SELECT nombre FROM etnia WHERE id=?s",$i);
                            #$search = mysqli_query($con,"SELECT * FROM etnia WHERE id='".$i."'");

                        if ($_SESSION['Etnia'] == $i) {
                            echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                        }else{
                            echo "<option value=".$i.">".$search."</option>";
                        }

                    }
                    ?>
                </select>
            </div><!-- end col -->
        </div><!-- end row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="top-label">
                    Dominancia
                </div>

                <select name="dominancia" id="dominancia" class="">
                    <?php 
                    if (!isset($_SESSION['Dominancia'])) {
                        echo "<option value='' default class='selected' selected>Dominancia del paciente</option>";
                    }else{
                        echo "<option value=''>Dominancia del paciente</option>";
                    }
                    $query = $db->query("SELECT * FROM dominancia");
                    $numrows = $db->numRows($query);
                    #$query=mysqli_query($con,"SELECT * FROM dominancia");
                    #$numrows=mysqli_num_rows($query);

                    for ($i=1; $i <= $numrows ; $i++) { 
                        $search = $db->getOne("SELECT nombre FROM dominancia WHERE id=?s",$i);
                        #$search = mysqli_query($con,"SELECT * FROM dominancia WHERE id='".$i."'");

                        if ($_SESSION['Dominancia'] == $i) {
                            echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                        }else{
                            echo "<option value=".$i.">".$search."</option>";
                        }

                    }
                    ?>
                </select>
            </div><!-- end col -->
            <div class="col-sm-6">
                <div class="top-label">
                    Religión
                </div>


                <!-- Carga regimen de la BD!!!!! -->
                <select name="religion" id="religion" class="">

                    <?php 
                    if (!isset($_SESSION['Region'])) {
                        echo "<option value='' default class='selected' selected>Religión del paciente</option>";
                    }else{
                        echo "<option value=''>Religión del paciente</option>";
                    }
                    $query = $db->query("SELECT * FROM religion");
                    $numrows = $db->numRows($query);
                        #$query=mysqli_query($con,"SELECT * FROM religion");
                        #$numrows=mysqli_num_rows($query);

                    for ($i=1; $i <= $numrows ; $i++) { 
                        $search = $db->getOne("SELECT nombre FROM religion WHERE id=?s",$i);
                            #$search = mysqli_query($con,"SELECT * FROM religion WHERE id='".$i."'");

                        if ($_SESSION['Region'] == $i) {
                            echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                        }else{
                            echo "<option value=".$i.">".$search."</option>";
                        }

                    }
                    ?>
                </select>
            </div><!-- end col -->
        </div><!-- end row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="top-label">
                    Deporte
                </div>
                <!-- Carga regimen de la BD!!!!! -->
                <select name="deporte" id="deporte" class="">

                    <?php 
                    if (!isset($_SESSION['Deporte'])) {
                        echo "<option value='' default class='selected' selected>Escribe qué deporte practíca el paciente</option>";
                    }else{
                        echo "<option value=''>Escribe qué deporte practíca el paciente</option>";
                    }
                    $query = $db->query("SELECT * FROM deportes");
                    $numrows = $db->numRows($query);
                        #$query=mysqli_query($con,"SELECT * FROM deportes");
                        #$numrows=mysqli_num_rows($query);

                    for ($i=1; $i <= $numrows ; $i++) { 
                        $search = $db->getOne("SELECT nombre FROM deportes WHERE id=?s",$i);
                            #$search = mysqli_query($con,"SELECT * FROM deportes WHERE id='".$i."'");

                        if ($_SESSION['Deporte'] == $i) {
                            echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                        }else{
                            echo "<option value=".$i.">".$search."</option>";
                        }
                    }
                    ?>
                </select>
            </div><!-- end col -->  
            <div class="col-sm-6">
                <div class="top-label">
                    Colegio
                </div>
                <input value="<?php echo $_SESSION['Colegio']; ?>" type="text" name="colegio" id="colegio" placeholder="Escribe el nombre del colegio">
            </div><!-- end col -->
        </div><!-- end row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="top-label">
                    Escolaridad
                </div>
                <input value="<?php echo $_SESSION['Escolaridad']; ?>" type="text" name="escolaridad" id="escolaridad" placeholder="Escribe la escolaridad del paciente">
            </div><!-- end col -->
            <div class="col-sm-6">
                <div class="top-label">
                    Rendimiento
                </div>
                <input value="<?php echo $_SESSION['Rendimiento']; ?>" type="text" name="rendimiento" id="rendimiento" placeholder="Escribe cuál era el rendimiento del paciente">
            </div><!-- end col -->
        </div><!-- end row -->

        <div class="row">
            <div class="col-sm-12">
                <input type="submit" name="guardar" value="Guardar" class="send-btn">
            </div>
        </div>

    </section>
</div><!-- end container -->
</form><!-- end form -->

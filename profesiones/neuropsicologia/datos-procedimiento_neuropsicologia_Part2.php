<form class="ajaxform" action= "<?echo $ZafiroRute?>datos-procedimiento.php" method="POST" id="basic-data" mensajeExito = "Los datos básicos han sido correctamente guardados." mensajeError = "Ha habido un error y tus datos no fueron guardados. intentalo más tarde.">
    <div class="row">
        <div class="col-sm-6">
            <div class="top-label">
                Número de autorización
            </div>
            <div class="procedimiento-search-input">
                <input value="<?php echo $_SESSION['numAutorizacion'] ?>" list="autorizaciones" name="autorizacion" id="autorizacion" data-minchars="0" class="awesomplete" placeholder="Escribe el numero de autorización" required onblur="search_procediento()">
                <datalist id="autorizaciones">
                        <?php 
                        $query = $db->query("SELECT * FROM informacion_orden WHERE id_paciente=?s",$_SESSION['documento']);
                        $numrows = $db->numRows($query);

                        for ($i=1; $i <= $numrows ; $i++) { 
                            $row = mysqli_fetch_array($query);
                            $search = $row['numero_autorizacion'];
                            echo "<option value='$search'>";
                            echo $search;
                        }
                        ?>

                </datalist>
            </div>

            <div class="procedimiento-search-icon">
                <div class="search-procedimieto" onmouseup="search_procediento()">
                    <i class="fa fa-search" aria-hidden="true"></i>               
                </div>
            </div>
        </div><!-- end col -->
        <div class="col-sm-6">
            <div class="top-label">
                Entidad
            </div>
            <!-- Carga médicos de la BD!!!!! -->
            <select  name="entidad" id="entidad" class="disabled" required >
                <?php 
                if (!isset($_SESSION['Entidad'])) {
                    echo "<option value='' default class='selected' selected>Con qué entidad viene</option>";
                }else{
                    echo "<option value=''>Con qué entidad viene</option>";
                }

                $query = $db->query("SELECT * FROM entidad");
                $numrows = $db->numRows($query);
                for ($i=1; $i <= $numrows ; $i++) { 
                    $search = $db->getOne("SELECT nombre FROM entidad WHERE id=?s",$i);
                    if ($_SESSION['Entidad'] == $i) {
                        echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                    }else{
                        echo "<option value=".$i.">".$search."</option>";
                    }
                }
                ?>
            </select>
        </div><!-- end col -->
    </div><!-- end row -->

    <div class="row">
        <div class="col-sm-6">
            <div class="top-label">
                Médico
            </div>
            <!-- Carga médicos de la BD!!!!! -->
            <select  name="medico" id="medico" class="disabled" required >
                <?php 
                if (!isset($_SESSION['Medico'])) {
                    echo "<option value='' default class='selected' selected>Escoge el médico</option>";
                }else{
                    echo "<option value=''>Escoge el médico</option>";
                }

                $query = $db->query("SELECT * FROM nombre_medico");
                $numrows = $db->numRows($query);

                for ($i=1; $i <= $numrows ; $i++) { 
                    $search = $db->getOne("SELECT nombre FROM nombre_medico WHERE id=?s",$i);

                    if ($_SESSION['Medico'] == $i) {
                        echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                    }else{
                        echo "<option value=".$i.">".$search."</option>";
                    }

                }
                ?>
            </select>
        </div><!-- end col -->
        <div class="col-sm-6">
            <div class="top-label">
                Tipo de consulta
            </div>
            <!-- Carga médicos de la BD!!!!! -->
            <select name="tipo_consulta" id="tipo_consulta" class="" required>

                <?php 
                if (!isset($_SESSION['tipoConsulta'])) {
                    echo "<option value='' default class='selected' selected>Tipo de consulta</option>";
                }else{
                    echo "<option value=''>Tipo de consulta</option>";
                }
                $query = $db->query("SELECT * FROM tipo_consulta");
                $numrows = $db->numRows($query);

                for ($i=1; $i <= $numrows ; $i++) { 
                    $search = $db->getOne("SELECT nombre FROM tipo_consulta WHERE id=?s",$i);

                    if ($_SESSION['tipoConsulta'] == $i) {                              
                        echo "<option value=".$i." default class='selected' selected>".$search."</option>";
                    }else{
                        echo "<option value=".$i.">".$search."</option>";
                    }
                }
                ?>
            </select>
        </div><!-- end col -->
    </div><!-- end row -->
    
    <div class="row">
        <div class="col-sm-6">
            <div class="top-label">
                Referido por
            </div>
            <input value= "<?php echo $_SESSION['Remitido']?>" type="text" name="referido" id="referido" placeholder="Por quién fue referido el paciente" required >
        </div><!-- end col -->
        <div class="col-sm-6">
            <div class="top-label">
                Especialidad
            </div>
            <input value= "<?php echo $_SESSION['especialidad']?>" type="text" name="interno" id="interno" placeholder="Escribe la especialidad del médico" required>
        </div><!-- end col -->
    </div><!-- end row -->

    <div class="row">
        <div class="col-sm-12">
            <div class="top-label">
                Procedimiento
            </div>
            <!-- Carga médicos de la BD!!!!! -->

            <input value="<?php echo $_SESSION['procedimiento']?>" list="procedimientos" name="procedimiento" id="procedimiento" data-minchars="1" class="awesomplete" placeholder="Busca el procedimiento" required>
            <datalist id="procedimientos">
                <?php 
                $query = $db->query("SELECT * FROM cup_table");
                $numrows = $db->numRows($query);

                for ($i=1; $i <= $numrows ; $i++) { 
                    $row = mysqli_fetch_array($query);
                    $search = $db->getOne("SELECT nombre FROM cup_table WHERE id=?s",$row['id']);
                    echo "<option value='".$row['id']." - ".$search."'>";
                }
                ?>
            </datalist>
        </div><!-- end col -->
    </div><!-- end row -->

    <div class="row">
        <div class="col-sm-6">
            <div class="top-label">
                Sesiones
            </div>
            <input value="<?php echo $_SESSION['sesiones']?>" type="number" min="0" name="sesion" id="sesion" placeholder="Número de sesiones" required>
        </div><!-- end col -->
        <div class="col-sm-6">
            <div class="top-label">
                Personal que lo atiende
            </div>
            <input value="<?php echo $_SESSION['personalAtiende']?>" type="text" name="atiende" id="atiende" placeholder="Escribe quien atiende al paciente" required>
        </div><!-- end col -->
    </div><!-- end row -->

    <div class="row">
        <div class="col-sm-12">
            <div class="top-label">
                Dagnóstico previo
            </div>

            <input value="<?php echo $_SESSION['dxPrev']?>" list="diagnosticos" name="prev_dx" id="prev_dx" data-minchars="1" class="awesomplete" placeholder="Busca el diagnóstico" required>
            <datalist id="diagnosticos">
                <?php 
                $query = $db->query("SELECT * FROM diagnosticos");
                $numrows = $db->numRows($query);

                for ($i=1; $i <= $numrows ; $i++) { 
                    $row = mysqli_fetch_array($query);
                    $search = $db->getOne("SELECT nombre FROM diagnosticos WHERE id=?s",$row['id']);
                    echo "<option value='".$row['id']." - ".$search."'>";
                }
                ?>
            </datalist>
        </div><!-- end col -->
    </div><!-- end row -->

    <div class="row">
        <div class="col-sm-12">
            <div class="top-label">
                Dagnóstico adicional
            </div>

            <input value="<?php echo $_SESSION['dxAdicional']?>" list="diagnosticos" name="new_dx" id="new_dx" data-minchars="1" class="awesomplete" placeholder="Busca el diagnóstico">
            <datalist id="diagnosticos">
                <?php 

                $query = $db->query("SELECT * FROM diagnosticos");
                $numrows = $db->numRows($query);

                for ($i=1; $i <= $numrows ; $i++) { 
                    $row = mysqli_fetch_array($query);
                    $search = $db->getOne("SELECT nombre FROM diagnosticos WHERE id=?s",$row['id']);
                    echo "<option value='".$row['id']." - ".$search."'>";   
                }
                ?>
            </datalist>
        </div><!-- end col -->
    </div><!-- end row -->

    <div class="row">
        <div class="col-sm-12">
            <input type="submit" name="guardar" value="Guardar" class="send-btn">
        </div>
    </div>

</form><!-- end form -->

        
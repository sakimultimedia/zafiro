<?php
if (isset($_SESSION['documento'])) {
    $varDocumento = $_SESSION['documento'];
    
    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
    $numrows = $db->numRows($query);
 
    #$query=mysqli_query($con,"SELECT * FROM pacientes WHERE documento='$varDocumento' AND uploaded_by='$varSessionBoss'");
    #$numrows=mysqli_num_rows($query);

    if($numrows==1)
        #guardar datos del paciente para mostrarse en otras paginas
    {   $_SESSION['tipoDocumento'] = $db->getOne("SELECT tipo_de_documento FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Nombre_1'] = $db->getOne("SELECT primer_nombre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Nombre_2'] = $db->getOne("SELECT segundo_nombre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Apellido_1'] = $db->getOne("SELECT primer_apellido FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Apellido_2'] = $db->getOne("SELECT segundo_apellido FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['nacimiento'] = $db->getOne("SELECT fecha_nacimiento FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Edad'] = $db->getOne("SELECT edad FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Genero'] = $db->getOne("SELECT sexo FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['EstadoCivil'] = $db->getOne("SELECT estado_civil FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);

        $_SESSION['Dir_1'] = $db->getOne("SELECT direccion_1 FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Dir_2'] = $db->getOne("SELECT direccion_2 FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Telefono_paciente'] = $db->getOne("SELECT telefono FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Celular_paciente'] = $db->getOne("SELECT celular FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Ciudad_paciente'] = $db->getOne("SELECT ciudad FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Medico'] = $db->getOne("SELECT nombre_medico FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Entidad'] = $db->getOne("SELECT entidad FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Regimen'] = $db->getOne("SELECT regimen FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);

        $_SESSION['Remitido'] = $db->getOne("SELECT remitido_por FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['AcompananteNombre'] = $db->getOne("SELECT acompanante FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['AcompananteTel'] = $db->getOne("SELECT telefono_acomp FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['acompananteParentezco'] = $db->getOne("SELECT parentesco_acomp FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Mail'] = $db->getOne("SELECT email FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Origen'] = $db->getOne("SELECT origen FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Nacionalidad'] = $db->getOne("SELECT nacionalidad FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Ocupacion'] = $db->getOne("SELECT ocupacion FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Empresa'] = $db->getOne("SELECT empresa FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);

        $_SESSION['ConyugueNombre'] = $db->getOne("SELECT conyugue FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['ConyugueDocumento'] = $db->getOne("SELECT documento_conyugue FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['ConyugueNacimiento'] = $db->getOne("SELECT fecha_nacimiento_conyugue FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['ConyugueDireccion'] = $db->getOne("SELECT direccion_conyugue FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['ConyugueTelefono'] = $db->getOne("SELECT telefono_conyugue FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Hijo1Nombre'] = $db->getOne("SELECT nombre_hijo1 FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Hijo1Sexo'] = $db->getOne("SELECT sexo_hijo1 FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Hijo1Edad'] = $db->getOne("SELECT edad_hijo1 FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Hijo2Nombre'] = $db->getOne("SELECT nombre_hijo2 FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Hijo2Sexo'] = $db->getOne("SELECT sexo_hijo2 FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Hijo2Edad'] = $db->getOne("SELECT edad_hijo2 FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Padre'] = $db->getOne("SELECT padre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['PadreEdad'] = $db->getOne("SELECT edad_padre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Madre'] = $db->getOne("SELECT madre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['MadreEdad'] = $db->getOne("SELECT edad_madre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);

        $_SESSION['Hobbie'] = $db->getOne("SELECT hobbie FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Etnia'] = $db->getOne("SELECT etnia FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Dominancia'] = $db->getOne("SELECT dominancia FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Region'] = $db->getOne("SELECT region FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Deporte'] = $db->getOne("SELECT deporte FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Colegio'] = $db->getOne("SELECT colegio FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Escolaridad'] = $db->getOne("SELECT escolaridad FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $_SESSION['Rendimiento'] = $db->getOne("SELECT rendimiento FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);

        $_SESSION['numAutorizacion'] = $db->getOne("SELECT  numero_autorizacion FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
        $_SESSION['TipoConsulta'] = $db->getOne("SELECT tipo_consulta FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
        $_SESSION['especialidad'] = $db->getOne("SELECT especialidad FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
        $_SESSION['procedimiento'] = $db->getOne("SELECT procedimiento FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
        $_SESSION['sesiones'] = $db->getOne("SELECT num_sesiones FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
        $_SESSION['personalAtiende'] = $db->getOne("SELECT personal_atiende FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
        $_SESSION['dxPrev'] = $db->getOne("SELECT dx_previo FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
        $_SESSION['dxAdicional'] = $db->getOne("SELECT dx_adicional FROM informacion_orden WHERE id_paciente=?s",$varDocumento);

        $_SESSION['dxNuevo'] = $db->getOne("SELECT dx_posterior FROM historia_clinica WHERE documento_paciente=?s",$varDocumento);
    }

}

$varDocumento = isset_empty_and_set($_SESSION['documento']);
$varTipoDocumento = isset_empty_and_set($_SESSION['tipoDocumento']);
$varDocumento = isset_empty_and_set($_SESSION['documento']);
$varID = isset_empty_and_set($_SESSION['tipoDocumento']);
$varNom1 = isset_empty_and_set($_SESSION['Nombre_1']);
$varNom2 = isset_empty_and_set($_SESSION['Nombre_2']);
$varApe1 = isset_empty_and_set($_SESSION['Apellido_1']);
$varApe2 = isset_empty_and_set($_SESSION['Apellido_2']);
$varBorn = isset_empty_and_set($_SESSION['nacimiento']);
$varEdad = isset_empty_and_set($_SESSION['Edad']);
$varGenero = isset_empty_and_set($_SESSION['Genero']);
$varEstCivil = isset_empty_and_set($_SESSION['EstadoCivil']);
$varDireccion_1 = isset_empty_and_set($_SESSION['Dir_1']);
$varDireccion_2 = isset_empty_and_set($_SESSION['Dir_2']);
$varTelefono = isset_empty_and_set($_SESSION['Telefono_paciente']);
$varCelular = isset_empty_and_set($_SESSION['Celular_paciente']);
$varCiudad = isset_empty_and_set($_SESSION['Ciudad_paciente']);
$varMedico = isset_empty_and_set($_SESSION['Medico']);
$varEntidad = isset_empty_and_set($_SESSION['Entidad']);
$varRegimen = isset_empty_and_set($_SESSION['Regimen']);
$varRemitido = isset_empty_and_set($_SESSION['Remitido']);
$varAcompana = isset_empty_and_set($_SESSION['AcompananteNombre']);
$varAcopanaTel = isset_empty_and_set($_SESSION['AcompananteTel']);
$varAcopanaParentezco = isset_empty_and_set($_SESSION['acompananteParentezco']);
$varMail = isset_empty_and_set($_SESSION['Mail']);
$varOrigen = isset_empty_and_set($_SESSION['Origen']);
$varNacionalidad = isset_empty_and_set($_SESSION['Nacionalidad']);
$varOcupacion = isset_empty_and_set($_SESSION['Ocupacion']);
$varEmpresa = isset_empty_and_set($_SESSION['Empresa']);
$varConyugueNom = isset_empty_and_set($_SESSION['ConyugueNombre']);
$varConyugueDoc = isset_empty_and_set($_SESSION['ConyugueDocumento']);
$varConyugueNacim = isset_empty_and_set($_SESSION['ConyugueNacimiento']);
$varConyugueDir = isset_empty_and_set($_SESSION['ConyugueDireccion']);
$varConyugueTel = isset_empty_and_set($_SESSION['ConyugueTelefono']);
$varHijo1 = isset_empty_and_set($_SESSION['Hijo1Nombre']);
$varHijo1Sex = isset_empty_and_set($_SESSION['Hijo1Sexo']);
$varHijo1Edad = isset_empty_and_set($_SESSION['Hijo1Edad']);
$varHijo2 = isset_empty_and_set($_SESSION['Hijo2Nombre']);
$varHijo2Sex = isset_empty_and_set($_SESSION['Hijo2Sexo']);
$varHijo2Edad = isset_empty_and_set($_SESSION['Hijo2Edad']);
$varPadre = isset_empty_and_set($_SESSION['Padre']);
$varPadreEdad = isset_empty_and_set($_SESSION['PadreEdad']);
$varMadre = isset_empty_and_set($_SESSION['Madre']);
$varMadreEdad = isset_empty_and_set($_SESSION['MadreEdad']);
$varHobbie = isset_empty_and_set($_SESSION['Hobbie']);
$varEtnia = isset_empty_and_set($_SESSION['Etnia']);
$varDominancia = isset_empty_and_set($_SESSION['Dominancia']);
$varRegion = isset_empty_and_set($_SESSION['Region']);
$varDeporte = isset_empty_and_set($_SESSION['Deporte']);
$varColegio = isset_empty_and_set($_SESSION['Colegio']);
$varEscolaridad = isset_empty_and_set($_SESSION['Escolaridad']);
$varRendimiento = isset_empty_and_set($_SESSION['Rendimiento']);

$varNumAutorizacion = isset_empty_and_set($_SESSION['numAutorizacion']);
$varTipoConsulta = isset_empty_and_set($_SESSION['TipoConsulta']);
$varEspecialidad = isset_empty_and_set($_SESSION['especialidad']);
$varProcedimiento = isset_empty_and_set($_SESSION['procedimiento']);
$varSesiones = isset_empty_and_set($_SESSION['sesiones']);
$varPersonalAtiende = isset_empty_and_set($_SESSION['personalAtiende']);
$varDxPrev = isset_empty_and_set($_SESSION['dxPrev']);
$varDxAdicional = isset_empty_and_set($_SESSION['dxAdicional']);
$varDxNuevo = isset_empty_and_set($_SESSION['dxNuevo']);


if(isset($_POST["guardar"])){



    if( !empty($_POST['historia_clinica']) ) {

        $historia_clinica = $_POST['historia_clinica'];

        $date=new DateTime(); //this returns the current date time
        $date = $date->setTimezone(new DateTimeZone('America/Bogota'));
        $result = $date->format('Y-m-d H:i:s');
        $date_time = $result;
        #echo $date_time;


        #================ARCHIVOS==================
        #================ARCHIVOS==================
        #================ARCHIVOS==================
        #================ARCHIVOS==================
        #================ARCHIVOS==================

        $file_link = "";
        #echo $file_name = $_FILES['files']['name'][0]."<br>";
        #list($nom, $tip) = explode(".", $file_name,2);
        #echo $tip;

        if($_FILES['files']['size'][0] > 0){
            $errors= array();
            foreach($_FILES['files']['tmp_name'] as $key => $tmp_name ){
                $file_name = $key.$_FILES['files']['name'][$key];
                list($nom, $tip) = explode(".", $file_name,2);
                $tip = ".".$tip;
                $file_name = md5($file_name);
                $file_name=$file_name.$tip;
                $file_size =$_FILES['files']['size'][$key];
                $file_tmp =$_FILES['files']['tmp_name'][$key];
                $file_type=$_FILES['files']['type'][$key];  
                
                /*if($file_size > 2097152){
                    $errors[]='File size must be less than 2 MB';
                }*/     

                $desired_dir="archivos";
                if(empty($errors)==true){
                    if(is_dir($desired_dir)==false){
                        mkdir("$desired_dir", 0700);        // Create directory if it does not exist
                    }
                    if(is_dir("$desired_dir/".$file_name)==false){
                        move_uploaded_file($file_tmp,$desired_dir."/".$file_name);
                    }else{                                  //rename the file if another one exist
                        $new_dir="archivos/".$file_name.time();
                        rename($file_tmp,$new_dir) ;                
                    }       
                }else{
                    print_r($errors);
                }
                $file_link = $file_link.$safiroSunRute.$desired_dir."/".$file_name."*+*";
                $tmp_file = $safiroSunRute.$desired_dir."/".$file_name;
                #echo "<a href='".$tmp_file."' target='_blank'>".$tmp_file."</a><br>";
                #echo $_SESSION["session_username"];
                #echo time();
            }
            if(empty($error)){
                #echo "NUEVO ARCHIVO: ";
                #echo "nombre: ";
                #echo $file_name; 
                #echo "/size: ";
                #echo $file_size;
                #echo "/tipo: ";
                #echo $file_type;
                #echo "Success";
            }
        }

        #=================BD=================
        #=================BD=================
        #=================BD=================
        #=================BD=================
        #=================BD=================
/*
        $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $numrows = $db->numRows($query);

        if($numrows==1){

            $sql="INSERT INTO historia_clinica
            (cometarios_medico, link_archivo, fecha_hora, documento_paciente) 

            VALUES('$historia_clinica', '$file_link', '$date_time', '$varDocumento')"; 

            $result=$db->query($sql);


            if($result){
                #$message = "Información agregada correctamente";
                create_popup('Información agregada correctamente');
            } else {
                #$message = "Error al ingresar la historia clinica!";
                create_popup('Error al ingresar la historia clinica!');
            }
        } else {
            #$message = "Porfavor ingresa la historia clinica!";
            create_popup('Porfavor ingresa la historia clinica!');
        }
*/
    }
}
?>
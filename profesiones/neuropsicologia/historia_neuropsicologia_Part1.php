<?php
if (isset($_SESSION['documento'])) {
    $varDocumento = $_SESSION['documento'];
    
    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
    $numrows = $db->numRows($query);
 
    $quiery_info_orden = $db->query("SELECT * FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
    #$query=mysqli_query($con,"SELECT * FROM pacientes WHERE documento='$varDocumento' AND uploaded_by='$varSessionBoss'");
    #$numrows=mysqli_num_rows($query);

    if($numrows==1)
    {   #guardar datos del paciente para mostrarse en otras paginas
        $row = $db->fetch($query);
        $row_info_orden = $db->fetch($quiery_info_orden);

        $_SESSION['tipoDocumento'] = $row{"tipo_de_documento"};
        $_SESSION['Nombre_1'] = $row{"primer_nombre"};
        $_SESSION['Nombre_2'] = $row{"segundo_nombre"};
        $_SESSION['Apellido_1'] = $row{"primer_apellido"};
        $_SESSION['Apellido_2'] = $row{"segundo_apellido"};
        $_SESSION['nacimiento'] = $row{"fecha_nacimiento"};
        $_SESSION['Edad'] = $row{"edad"};
        $_SESSION['Genero'] = $row{"sexo"};
        $_SESSION['EstadoCivil'] = $row{"estado_civil"};
        $_SESSION['EPS'] = $row{"eps"};

        $_SESSION['Dir_1'] = $row{"direccion_1"};
        $_SESSION['Dir_2'] = $row{"direccion_2"};
        $_SESSION['Telefono_paciente'] = $row{"telefono"};
        $_SESSION['Celular_paciente'] = $row{"celular"};
        $_SESSION['Ciudad_paciente'] = $row{"ciudad"};
        $_SESSION['Medico'] = $row{"nombre_medico"};
        $_SESSION['Entidad'] = $row{"entidad"};
        $_SESSION['Regimen'] = $row{"regimen"};

        $_SESSION['Remitido'] = $row{"remitido_por"};
        $_SESSION['AcompananteNombre'] = $row{"acompanante"};
        $_SESSION['AcompananteTel'] = $row{"telefono_acomp"};
        $_SESSION['acompananteParentezco'] = $row{"parentesco_acomp"};
        $_SESSION['Mail'] = $row{"email"};
        $_SESSION['Origen'] = $row{"origen"};
        $_SESSION['Nacionalidad'] = $row{"nacionalidad"};
        $_SESSION['Ocupacion'] = $row{"ocupacion"};
        $_SESSION['Empresa'] = $row{"empresa"};

        $_SESSION['ConyugueNombre'] = $row{"conyugue"};
        $_SESSION['ConyugueDocumento'] = $row{"documento_conyugue"};
        $_SESSION['ConyugueNacimiento'] = $row{"fecha_nacimiento_conyugue"};
        $_SESSION['ConyugueDireccion'] = $row{"direccion_conyugue"};
        $_SESSION['ConyugueTelefono'] = $row{"telefono_conyugue"};
        $_SESSION['Hijo1Nombre'] = $row{"nombre_hijo1"};
        $_SESSION['Hijo1Sexo'] = $row{"sexo_hijo1"};
        $_SESSION['Hijo1Edad'] = $row{"edad_hijo1"};
        $_SESSION['Hijo2Nombre'] = $row{"nombre_hijo2"};
        $_SESSION['Hijo2Sexo'] = $row{"sexo_hijo2"};
        $_SESSION['Hijo2Edad'] = $row{"edad_hijo2"};
        $_SESSION['Padre'] = $row{"padre"};
        $_SESSION['PadreEdad'] = $row{"edad_padre"};
        $_SESSION['Madre'] = $row{"madre"};
        $_SESSION['MadreEdad'] = $row{"edad_madre"};

        $_SESSION['Hobbie'] = $row{"hobbie"};
        $_SESSION['Etnia'] = $row{"etnia"};
        $_SESSION['Dominancia'] = $row{"dominancia"};
        $_SESSION['Region'] = $row{"region"};
        $_SESSION['Deporte'] = $row{"deporte"};
        $_SESSION['Colegio'] = $row{"colegio"};
        $_SESSION['Escolaridad'] = $row{"escolaridad"};
        $_SESSION['Rendimiento'] = $row{"rendimiento"};


        $_SESSION['numAutorizacion'] = $row_info_orden["numero_autorizacion"];
        $_SESSION['TipoConsulta'] = $row_info_orden["tipo_consulta"];
        $_SESSION['especialidad'] = $row_info_orden["especialidad"];
        $_SESSION['procedimiento'] = $row_info_orden["procedimiento"];
        $_SESSION['sesiones'] = $row_info_orden["num_sesiones"];
        $_SESSION['personalAtiende'] = $row_info_orden["personal_atiende"];
        $_SESSION['dxPrev'] = $row_info_orden["dx_previo"];
        $_SESSION['dxAdicional'] = $row_info_orden["dx_adicional"];

        $_SESSION['dxNuevo'] = $db->getOne("SELECT dx_posterior FROM historia_clinica WHERE documento_paciente=?s",$varDocumento);
    }

}
#variables de la pagina
//On page 2
$varDocumento = isset_empty_and_set($_SESSION['documento']);
$varTipoDocumento = isset_empty_and_set($_SESSION['tipoDocumento']);
$varDocumento = isset_empty_and_set($_SESSION['documento']);
$varID = isset_empty_and_set($_SESSION['tipoDocumento']);
$varNom1 = isset_empty_and_set($_SESSION['Nombre_1']);
$varNom2 = isset_empty_and_set($_SESSION['Nombre_2']);
$varApe1 = isset_empty_and_set($_SESSION['Apellido_1']);
$varApe2 = isset_empty_and_set($_SESSION['Apellido_2']);
$varBorn = isset_empty_and_set($_SESSION['nacimiento']);
$varEdad = isset_empty_and_set($_SESSION['Edad']);
$varGenero = isset_empty_and_set($_SESSION['Genero']);
$varEstCivil = isset_empty_and_set($_SESSION['EstadoCivil']);
$varEPS = isset_empty_and_set($_SESSION['EPS']);

$varDireccion_1 = isset_empty_and_set($_SESSION['Dir_1']);
$varDireccion_2 = isset_empty_and_set($_SESSION['Dir_2']);
$varTelefono = isset_empty_and_set($_SESSION['Telefono_paciente']);
$varCelular = isset_empty_and_set($_SESSION['Celular_paciente']);
$varCiudad = isset_empty_and_set($_SESSION['Ciudad_paciente']);
$varMedico = isset_empty_and_set($_SESSION['Medico']);
$varEntidad = isset_empty_and_set($_SESSION['Entidad']);
$varRegimen = isset_empty_and_set($_SESSION['Regimen']);

$varRemitido = isset_empty_and_set($_SESSION['Remitido']);
$varAcompana = isset_empty_and_set($_SESSION['AcompananteNombre']);
$varAcopanaTel = isset_empty_and_set($_SESSION['AcompananteTel']);
$varAcopanaParentezco = isset_empty_and_set($_SESSION['acompananteParentezco']);
$varMail = isset_empty_and_set($_SESSION['Mail']);
$varOrigen = isset_empty_and_set($_SESSION['Origen']);
$varNacionalidad = isset_empty_and_set($_SESSION['Nacionalidad']);
$varOcupacion = isset_empty_and_set($_SESSION['Ocupacion']);
$varEmpresa = isset_empty_and_set($_SESSION['Empresa']);

$varConyugueNom = isset_empty_and_set($_SESSION['ConyugueNombre']);
$varConyugueDoc = isset_empty_and_set($_SESSION['ConyugueDocumento']);
$varConyugueNacim = isset_empty_and_set($_SESSION['ConyugueNacimiento']);
$varConyugueDir = isset_empty_and_set($_SESSION['ConyugueDireccion']);
$varConyugueTel = isset_empty_and_set($_SESSION['ConyugueTelefono']);
$varHijo1 = isset_empty_and_set($_SESSION['Hijo1Nombre']);
$varHijo1Sex = isset_empty_and_set($_SESSION['Hijo1Sexo']);
$varHijo1Edad = isset_empty_and_set($_SESSION['Hijo1Edad']);
$varHijo2 = isset_empty_and_set($_SESSION['Hijo2Nombre']);
$varHijo2Sex = isset_empty_and_set($_SESSION['Hijo2Sexo']);
$varHijo2Edad = isset_empty_and_set($_SESSION['Hijo2Edad']);
$varPadre = isset_empty_and_set($_SESSION['Padre']);
$varPadreEdad = isset_empty_and_set($_SESSION['PadreEdad']);
$varMadre = isset_empty_and_set($_SESSION['Madre']);
$varMadreEdad = isset_empty_and_set($_SESSION['MadreEdad']);

$varHobbie = isset_empty_and_set($_SESSION['Hobbie']);
$varEtnia = isset_empty_and_set($_SESSION['Etnia']);
$varDominancia = isset_empty_and_set($_SESSION['Dominancia']);
$varRegion = isset_empty_and_set($_SESSION['Region']);
$varDeporte = isset_empty_and_set($_SESSION['Deporte']);
$varColegio = isset_empty_and_set($_SESSION['Colegio']);
$varEscolaridad = isset_empty_and_set($_SESSION['Escolaridad']);
$varRendimiento = isset_empty_and_set($_SESSION['Rendimiento']);

$varNumAutorizacion = isset_empty_and_set($_SESSION['numAutorizacion']);
$varTipoConsulta = isset_empty_and_set($_SESSION['TipoConsulta']);
$varEspecialidad = isset_empty_and_set($_SESSION['especialidad']);
$varProcedimiento = isset_empty_and_set($_SESSION['procedimiento']);
$varSesiones = isset_empty_and_set($_SESSION['sesiones']);
$varPersonalAtiende = isset_empty_and_set($_SESSION['personalAtiende']);
$varDxPrev = isset_empty_and_set($_SESSION['dxPrev']);
$varDxAdicional = isset_empty_and_set($_SESSION['dxAdicional']);

$varDxNuevo = isset_empty_and_set($_SESSION['dxNuevo']);


if(isset($_POST["guardar"])){
    $date=new DateTime(); //this returns the current date time
    $date = $date->setTimezone(new DateTimeZone('America/Bogota'));
    $result = $date->format('Y-m-d H:i:s');
    $date_time = $result;
    #echo $date_time;


    if( isset_and_empty($_POST['historia_clinica']) ) {
        $historia_clinica = $_POST['historia_clinica'];
    }else {
        $historia_clinica = null;
    }

    #================ARCHIVOS==================
    #================ARCHIVOS==================

    $file_link = null;
    #echo $file_name = $_FILES['files']['name'][0]."<br>";
    #list($nom, $tip) = explode(".", $file_name,2);
    #echo $tip;
    $filesCountDown = count($_FILES['files']['name']);
    if($_FILES['files']['size'][0] > 0){
        $errors= array();
        foreach($_FILES['files']['tmp_name'] as $key => $tmp_name ){
            $file_name = $key.$_FILES['files']['name'][$key];
            list($nom, $tip) = explode(".", $file_name,2);
            if ($tip == 'jpe') {
                $tip = 'jpg';
            }
            $tip = ".".$tip;
            $file_name = md5($file_name);
            $file_size =$_FILES['files']['size'][$key];
            $file_tmp =$_FILES['files']['tmp_name'][$key];
            $file_type=$_FILES['files']['type'][$key];  

            /*if($file_size > 2097152){
                $errors[]='File size must be less than 2 MB';
            }*/     

            $desired_dir="archivos";

            if(empty($errors)==true){
                if(is_dir($desired_dir) == false){
                    mkdir("$desired_dir", 0700);        // Create directory if it does not exist
                }
                if(is_dir($desired_dir."/".$file_name) == false){
                    $file_name=$file_name.$tip;
                    move_uploaded_file($file_tmp,$desired_dir."/".$file_name);
                }else{                                  
                    //rename the file if another one exist
                    $new_name = $desired_dir."/".$file_name.time();
                    $new_name = $new_name.$tip;
                    move_uploaded_file($file_tmp,$new_name) ;               
                }       

            }else{
                print_r($errors);
            }
            $file_link = $ZafiroRute.$desired_dir."/".$file_name;
            #echo "<a href='".$file_link."' target='_blank'>".$file_link."</a><br>";
            if ($filesCountDown > 1) {
                $file_link = $file_link."*+*";
                $filesCountDown = $filesCountDown - 1;
            }
            $tmp_file = $ZafiroRute.$desired_dir."/".$file_name;

            #echo "<a href='".$tmp_file."' target='_blank'>".$tmp_file."</a><br>";
            #echo $_SESSION["session_username"];
            #echo time();
        }
        if(empty($error)){
            #echo "NUEVO ARCHIVO: ";
            #echo "nombre: ";
            #echo $file_name; 
            #echo "/size: ";
            #echo $file_size;
            #echo "/tipo: ";
            #echo $file_type;
            #echo "Success";
        }
    }

    #=================DX_POSTERIOR=================
    #=================DX_POSTERIOR=================
    #=================DX_POSTERIOR=================
    #=================DX_POSTERIOR=================
    #=================DX_POSTERIOR=================

    if( isset_and_empty($_POST['post_dx']) ) {
        $DxPosterior = $_POST['post_dx'];

        $_SESSION["dx_posterior"] = $DxPosterior;
    } else {
        $DxPosterior = "";
    }

    if (isset_and_empty($_POST['coupon_question'])) {
        if ($_POST['coupon_question'] != 1) {
            $DxPosterior = null;
        }
    }
    
    #=================BD=================
    #=================BD=================

    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
    $numrows = $db->numRows($query);
    #$query=mysqli_query($con,"SELECT * FROM pacientes WHERE documento='$varDocumento' AND uploaded_by='$varSessionBoss'");
    #$numrows=mysqli_num_rows($query);

    if($numrows==1)
    {
        $sql="INSERT INTO historia_clinica
        (documento_paciente, dx_posterior, medic_name, cometarios_medico, link_archivo, fecha_hora) 

        VALUES(?s,?s,?s,?s,?s,?s)";

        $result = $db->query($sql,  Arreglar_String($varDocumento),
                                    Arreglar_String($DxPosterior),
                                    Arreglar_String($_SESSION["session_username"]),
                                    Arreglar_String($historia_clinica),
                                    Arreglar_String($file_link),
                                    Arreglar_String($date_time));


        if($result){
            #$message = "Información agregada correctamente";
            create_popup('Información agregada correctamente');;
        } else {
            #$message = "Error al ingresar la historia clinica!";
            create_popup('Error al ingresar la historia clinica!');
        }
    } else {
        #$message = "Porfavor ingresa la historia clinica!";
        create_popup('Porfavor ingresa la historia clinica!');
    }


#unset($_POST["guardar"]);
}
?>

<?php

if (isset($_SESSION['documento'])) {
    $varDocumento = $_SESSION['documento'];
    
    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
    $numrows = $db->numRows($query);

    if($numrows==1)
    {    #guardar datos del paciente para mostrarse en otras paginas
        $row = $db->fetch($query);

        $_SESSION['tipoDocumento'] = $row{"tipo_de_documento"};
        $_SESSION['Nombre_1'] = $row{"primer_nombre"};
        $_SESSION['Nombre_2'] = $row{"segundo_nombre"};
        $_SESSION['Apellido_1'] = $row{"primer_apellido"};
        $_SESSION['Apellido_2'] = $row{"segundo_apellido"};
        $_SESSION['nacimiento'] = $row{"fecha_nacimiento"};
        $_SESSION['Edad'] = $row{"edad"};
        $_SESSION['Genero'] = $row{"sexo"};
        $_SESSION['EstadoCivil'] = $row{"estado_civil"};
        $_SESSION['EPS'] = $row{"eps"};

        $_SESSION['Dir_1'] = $row{"direccion_1"};
        $_SESSION['Dir_2'] = $row{"direccion_2"};
        $_SESSION['Telefono_paciente'] = $row{"telefono"};
        $_SESSION['Celular_paciente'] = $row{"celular"};
        $_SESSION['Ciudad_paciente'] = $row{"ciudad"};
        $_SESSION['Medico'] = $row{"nombre_medico"};
        $_SESSION['Entidad'] = $row{"entidad"};
        $_SESSION['Regimen'] = $row{"regimen"};

        $_SESSION['Remitido'] = $row{"remitido_por"};
        $_SESSION['AcompananteNombre'] = $row{"acompanante"};
        $_SESSION['AcompananteTel'] = $row{"telefono_acomp"};
        $_SESSION['acompananteParentezco'] = $row{"parentesco_acomp"};
        $_SESSION['Mail'] = $row{"email"};
        $_SESSION['Origen'] = $row{"origen"};
        $_SESSION['Nacionalidad'] = $row{"nacionalidad"};
        $_SESSION['Ocupacion'] = $row{"ocupacion"};
        $_SESSION['Empresa'] = $row{"empresa"};

        $_SESSION['ConyugueNombre'] = $row{"conyugue"};
        $_SESSION['ConyugueDocumento'] = $row{"documento_conyugue"};
        $_SESSION['ConyugueNacimiento'] = $row{"fecha_nacimiento_conyugue"};
        $_SESSION['ConyugueDireccion'] = $row{"direccion_conyugue"};
        $_SESSION['ConyugueTelefono'] = $row{"telefono_conyugue"};
        $_SESSION['Hijo1Nombre'] = $row{"nombre_hijo1"};
        $_SESSION['Hijo1Sexo'] = $row{"sexo_hijo1"};
        $_SESSION['Hijo1Edad'] = $row{"edad_hijo1"};
        $_SESSION['Hijo2Nombre'] = $row{"nombre_hijo2"};
        $_SESSION['Hijo2Sexo'] = $row{"sexo_hijo2"};
        $_SESSION['Hijo2Edad'] = $row{"edad_hijo2"};
        $_SESSION['Padre'] = $row{"padre"};
        $_SESSION['PadreEdad'] = $row{"edad_padre"};
        $_SESSION['Madre'] = $row{"madre"};
        $_SESSION['MadreEdad'] = $row{"edad_madre"};

        $_SESSION['Hobbie'] = $row{"hobbie"};
        $_SESSION['Etnia'] = $row{"etnia"};
        $_SESSION['Dominancia'] = $row{"dominancia"};
        $_SESSION['Region'] = $row{"region"};
        $_SESSION['Deporte'] = $row{"deporte"};
        $_SESSION['Colegio'] = $row{"colegio"};
        $_SESSION['Escolaridad'] = $row{"escolaridad"};
        $_SESSION['Rendimiento'] = $row{"rendimiento"};
    }elseif($numrows==0){
        #guardar datos del paciente para mostrarse en otras paginas
        $_SESSION['tipoDocumento'] = "";
        $_SESSION['Nombre_1'] = "";
        $_SESSION['Nombre_2'] = "";
        $_SESSION['Apellido_1'] = "";
        $_SESSION['Apellido_2'] = "";
        $_SESSION['nacimiento'] = "";
        $_SESSION['Edad'] = "";
        $_SESSION['Genero'] = "";
        $_SESSION['EstadoCivil'] = "";
        $_SESSION['EPS'] = "";

        $_SESSION['Dir_1'] = "";
        $_SESSION['Dir_2'] = "";
        $_SESSION['Telefono_paciente'] = "";
        $_SESSION['Celular_paciente'] = "";
        $_SESSION['Ciudad_paciente'] = "";
        $_SESSION['Medico'] = "";
        $_SESSION['Entidad'] = "";
        $_SESSION['Regimen'] = "";

        $_SESSION['Remitido'] = "";
        $_SESSION['AcompananteNombre'] = "";
        $_SESSION['AcompananteTel'] = "";
        $_SESSION['acompananteParentezco'] = "";
        $_SESSION['Mail'] = "";
        $_SESSION['Origen'] = "";
        $_SESSION['Nacionalidad'] = "";
        $_SESSION['Ocupacion'] = "";
        $_SESSION['Empresa'] = "";

        $_SESSION['ConyugueNombre'] = "";
        $_SESSION['ConyugueDocumento'] = "";
        $_SESSION['ConyugueNacimiento'] = "";
        $_SESSION['ConyugueDireccion'] = "";
        $_SESSION['ConyugueTelefono'] = "";
        $_SESSION['Hijo1Nombre'] = "";
        $_SESSION['Hijo1Sexo'] = "";
        $_SESSION['Hijo1Edad'] = "";
        $_SESSION['Hijo2Nombre'] = "";
        $_SESSION['Hijo2Sexo'] = "";
        $_SESSION['Hijo2Edad'] = "";
        $_SESSION['Padre'] = "";
        $_SESSION['PadreEdad'] = "";
        $_SESSION['Madre'] = "";
        $_SESSION['MadreEdad'] = "";

        $_SESSION['Hobbie'] = "";
        $_SESSION['Etnia'] = "";
        $_SESSION['Dominancia'] = "";
        $_SESSION['Region'] = "";
        $_SESSION['Deporte'] = "";
        $_SESSION['Colegio'] = "";
        $_SESSION['Escolaridad'] = "";
        $_SESSION['Rendimiento'] = "";
    }
}


#variables de la pagina
$varDocumento = isset_and_empty($_SESSION['documento']);
$varTipoDocumento = isset_and_empty($_SESSION['tipoDocumento']);
$varDocumento = isset_and_empty($_SESSION['documento']);
$varID = isset_and_empty($_SESSION['tipoDocumento']);
$varNom1 = isset_and_empty($_SESSION['Nombre_1']);
$varNom2 = isset_and_empty($_SESSION['Nombre_2']);
$varApe1 = isset_and_empty($_SESSION['Apellido_1']);
$varApe2 = isset_and_empty($_SESSION['Apellido_2']);
$varBorn = isset_and_empty($_SESSION['nacimiento']);
$varEdad = isset_and_empty($_SESSION['Edad']);
$varGenero = isset_and_empty($_SESSION['Genero']);
$varEstCivil = isset_and_empty($_SESSION['EstadoCivil']);
$varEPS = isset_and_empty($_SESSION['EPS']);


$varDireccion_1 = isset_and_empty($_SESSION['Dir_1']);
$varDireccion_2 = isset_and_empty($_SESSION['Dir_2']);
$varTelefono = isset_and_empty($_SESSION['Telefono_paciente']);
$varCelular = isset_and_empty($_SESSION['Celular_paciente']);
$varCiudad = isset_and_empty($_SESSION['Ciudad_paciente']);
$varMedico = isset_and_empty($_SESSION['Medico']);
$varEntidad = isset_and_empty($_SESSION['Entidad']);
$varRegimen = isset_and_empty($_SESSION['Regimen']);

$varRemitido = isset_and_empty($_SESSION['Remitido']);
$varAcompana = isset_and_empty($_SESSION['AcompananteNombre']);
$varAcopanaTel = isset_and_empty($_SESSION['AcompananteTel']);
$varAcopanaParentezco = isset_and_empty($_SESSION['acompananteParentezco']);
$varMail = isset_and_empty($_SESSION['Mail']);
$varOrigen = isset_and_empty($_SESSION['Origen']);
$varNacionalidad = isset_and_empty($_SESSION['Nacionalidad']);
$varOcupacion = isset_and_empty($_SESSION['Ocupacion']);
$varEmpresa = isset_and_empty($_SESSION['Empresa']);

$varConyugueNom = isset_and_empty($_SESSION['ConyugueNombre']);
$varConyugueDoc = isset_and_empty($_SESSION['ConyugueDocumento']);
$varConyugueNacim = isset_and_empty($_SESSION['ConyugueNacimiento']);
$varConyugueDir = isset_and_empty($_SESSION['ConyugueDireccion']);
$varConyugueTel = isset_and_empty($_SESSION['ConyugueTelefono']);
$varHijo1 = isset_and_empty($_SESSION['Hijo1Nombre']);
$varHijo1Sex = isset_and_empty($_SESSION['Hijo1Sexo']);
$varHijo1Edad = isset_and_empty($_SESSION['Hijo1Edad']);
$varHijo2 = isset_and_empty($_SESSION['Hijo2Nombre']);
$varHijo2Sex = isset_and_empty($_SESSION['Hijo2Sexo']);
$varHijo2Edad = isset_and_empty($_SESSION['Hijo2Edad']);
$varPadre = isset_and_empty($_SESSION['Padre']);
$varPadreEdad = isset_and_empty($_SESSION['PadreEdad']);
$varMadre = isset_and_empty($_SESSION['Madre']);
$varMadreEdad = isset_and_empty($_SESSION['MadreEdad']);

$varHobbie = isset_and_empty($_SESSION['Hobbie']);
$varEtnia = isset_and_empty($_SESSION['Etnia']);
$varDominancia = isset_and_empty($_SESSION['Dominancia']);
$varRegion = isset_and_empty($_SESSION['Region']);
$varDeporte = isset_and_empty($_SESSION['Deporte']);
$varColegio = isset_and_empty($_SESSION['Colegio']);
$varEscolaridad = isset_and_empty($_SESSION['Escolaridad']);
$varRendimiento = isset_and_empty($_SESSION['Rendimiento']);

if(isset($_POST["guardar"])){
 
    #====================================datos-basicos============================================
    #====================================datos-basicos============================================
    #====================================datos-basicos============================================

    $varDocumento = isset_empty_and_set($_POST['documento']);
    $varID = isset_empty_and_set($_POST["ID"]);
    $varNom1 = isset_empty_and_set($_POST['nombre_1']);
    $varNom2 = isset_empty_and_set($_POST['nombre_2']);
    $varApe1 = isset_empty_and_set($_POST['apellido_1']);
    $varApe2 = isset_empty_and_set($_POST['apellido_2']);
    $varNombreCompleto = $varNom1;
    if ($varNom2 != '') {$varNombreCompleto = $varNombreCompleto." ".$varNom2;}
    if ($varApe1 != '') {$varNombreCompleto = $varNombreCompleto." ".$varApe1;}
    if ($varApe2 != '') {$varNombreCompleto = $varNombreCompleto." ".$varApe2;}
    $varBorn = $_POST['fecha-nacimiento'];
    if (isset($varBorn)) $varEdad = calcular_edad($varBorn);
    if (strpos($varBorn, '/') !== false) {
        $varBorn = str_replace("/","-",$varBorn);
    }
    $varGenero = isset_empty_and_set($_POST['sexo_paciente']);
    $varEstCivil = isset_empty_and_set($_POST['estado-civil']);
    $varEPS = isset_empty_and_set($_POST['eps']);
    if (isset_and_empty($varEPS)) {
        $varEPS = $db->getOne("SELECT id FROM eps WHERE nombre=?s ",$varEPS);
    }


    #====================================info personal============================================
    #====================================info personal============================================
    #====================================info personal============================================

    $varDireccion_1 = isset_empty_and_set($_POST['direccion_1']);
    $varDireccion_2 = isset_empty_and_set($_POST['direccion_2']);
    $varTelefono = isset_empty_and_set($_POST['telefono']);
    $varCelular = isset_empty_and_set($_POST['celular']);
    $varCiudad = isset_empty_and_set($_POST['ciudad']);
    if (isset_and_empty($varCiudad)) {
        $varCiudad = $db->getOne("SELECT id FROM ciudad WHERE nombre=?s ",$varCiudad);
    }
    $varMedico = isset_empty_and_set($_POST['medico']);
    $varEntidad = isset_empty_and_set($_POST['entidad']);
    $varRegimen = isset_empty_and_set($_POST['regimen']);


    #====================================EXTRAS============================================
    #====================================EXTRAS============================================
    #====================================EXTRAS============================================

    #OCUPACION-----

    #info acompanante   $var2 = isset_empty_and_set($var1);
    $varRemitido = isset_empty_and_set($_POST['remitido']);
    $varAcompana = isset_empty_and_set($_POST['acompana']);
    $varAcopanaTel = isset_empty_and_set($_POST['acompana-tel']);
    $varAcopanaParentezco = isset_empty_and_set($_POST['acompana-parentezco']);
    
    #informacion del paciente
    $varMail = isset_empty_and_set($_POST['mail']);
    $varOrigen = isset_empty_and_set($_POST['origen']);
    $varNacionalidad = isset_empty_and_set($_POST['nacionalidad']);
    $varOcupacion = isset_empty_and_set($_POST['ocupacion']);
    $varEmpresa = isset_empty_and_set($_POST['empresa']);

    #IMNFO FAMILIAR-------
    #info conyugue
    $varConyugueNom = isset_empty_and_set($_POST['conyugue']);
    $varConyugueDoc = isset_empty_and_set($_POST['conyugue_doc']);
    $varConyugueNacim = $_POST['conyugue_nacim'];
    $varConyugueDir = isset_empty_and_set($_POST['conyugue_direccion']);
    $varConyugueTel = isset_empty_and_set($_POST['conyugue_tel']);

    #info hijos
    $varHijo1 = isset_empty_and_set($_POST['hijo_1']);
    $varHijo1Sex = isset_empty_and_set($_POST['sexo_hijo_1']);
    $varHijo1Edad = isset_empty_and_set($_POST['hijo_1_edad']);
    $varHijo2 = isset_empty_and_set($_POST['hijo_2']);
    $varHijo2Sex = isset_empty_and_set($_POST['sexo_hijo_2']);
    $varHijo2Edad = isset_empty_and_set($_POST['hijo_2_edad']);


    #info padres
    $varPadre = isset_empty_and_set($_POST['nombre_padre']);
    $varPadreEdad = isset_empty_and_set($_POST['edad_padre']);
    $varMadre = isset_empty_and_set($_POST['nombre_madre']);
    $varMadreEdad = isset_empty_and_set($_POST['edad_madre']);


    #otra informacion-------
    $varHobbie = isset_empty_and_set($_POST['hobbie']);
    $varEtnia = isset_empty_and_set($_POST['etnia']);
    $varDominancia = isset_empty_and_set($_POST['dominancia']);
    $varRegion = isset_empty_and_set($_POST['religion']);
    $varDeporte = isset_empty_and_set($_POST['deporte']);
    $varColegio = isset_empty_and_set($_POST['colegio']);
    $varEscolaridad = isset_empty_and_set($_POST['escolaridad']);
    $varRendimiento = isset_empty_and_set($_POST['rendimiento']);


    #guardar datos del paciente para mostrarse en otras paginas 
    $_SESSION['documento'] = isset_and_set($varDocumento);
    $_SESSION['tipoDocumento'] = isset_and_set($varID);
    $_SESSION['Nombre_1'] = isset_and_set($varNom1);
    $_SESSION['Nombre_2'] = isset_and_set($varNom2);
    $_SESSION['Apellido_1'] = isset_and_set($varApe1);
    $_SESSION['Apellido_2'] = isset_and_set($varApe2);
    $_SESSION['NombreCompleto'] = isset_and_set($varNombreCompleto);
    $_SESSION['nacimiento'] = isset_and_set($varBorn);
    $_SESSION['Edad'] = isset_and_set($varEdad);
    $_SESSION['Genero'] = isset_and_set($varGenero);
    $_SESSION['EstadoCivil'] = isset_and_set($varEstCivil);
    $_SESSION['EPS'] = isset_and_set($varEPS);

    $_SESSION['Dir_1'] = isset_and_set($varDireccion_1);
    $_SESSION['Dir_2'] = isset_and_set($varDireccion_2);
    $_SESSION['Telefono_paciente'] = isset_and_set($varTelefono);
    $_SESSION['Celular_paciente'] = isset_and_set($varCelular);
    $_SESSION['Ciudad_paciente'] = isset_and_set($varCiudad);
    $_SESSION['Medico'] = isset_and_set($varMedico);
    $_SESSION['Entidad'] = isset_and_set($varEntidad);
    $_SESSION['Regimen'] = isset_and_set($varRegimen);

    $_SESSION['Remitido'] = isset_and_set($varRemitido);
    $_SESSION['AcompananteNombre'] = isset_and_set($varAcompana);
    $_SESSION['AcompananteTel'] = isset_and_set($varAcopanaTel);
    $_SESSION['acompananteParentezco'] = isset_and_set($varAcopanaParentezco);
    $_SESSION['Mail'] = isset_and_set($varMail);
    $_SESSION['Origen'] = isset_and_set($varOrigen);
    $_SESSION['Nacionalidad'] = isset_and_set($varNacionalidad);
    $_SESSION['Ocupacion'] = isset_and_set($varOcupacion);
    $_SESSION['Empresa'] = isset_and_set($varEmpresa);

    $_SESSION['ConyugueNombre'] = isset_and_set($varConyugueNom);
    $_SESSION['ConyugueDocumento'] = isset_and_set($varConyugueDoc);
    $_SESSION['ConyugueNacimiento'] = isset_and_set($varConyugueNacim);
    $_SESSION['ConyugueDireccion'] = isset_and_set($varConyugueDir);
    $_SESSION['ConyugueTelefono'] = isset_and_set($varConyugueTel);
    $_SESSION['Hijo1Nombre'] = isset_and_set($varHijo1);
    $_SESSION['Hijo1Sexo'] = isset_and_set($varHijo1Sex);
    $_SESSION['Hijo1Edad'] = isset_and_set($varHijo1Edad);
    $_SESSION['Hijo2Nombre'] = isset_and_set($varHijo2);
    $_SESSION['Hijo2Sexo'] = isset_and_set($varHijo2Sex);
    $_SESSION['Hijo2Edad'] = isset_and_set($varHijo2Edad);
    $_SESSION['Padre'] = isset_and_set($varPadre);
    $_SESSION['PadreEdad'] = isset_and_set($varPadreEdad);
    $_SESSION['Madre'] = isset_and_set($varMadre);
    $_SESSION['MadreEdad'] = isset_and_set($varMadreEdad);

    $_SESSION['Hobbie'] = isset_and_set($varHobbie);
    $_SESSION['Etnia'] = isset_and_set($varEtnia);
    $_SESSION['Dominancia'] = isset_and_set($varDominancia);
    $_SESSION['Region'] = isset_and_set($varRegion);
    $_SESSION['Deporte'] = isset_and_set($varDeporte);
    $_SESSION['Colegio'] = isset_and_set($varColegio);
    $_SESSION['Escolaridad'] = isset_and_set($varEscolaridad);
    $_SESSION['Rendimiento'] = isset_and_set($varRendimiento);



    #====================================BD============================================
    #====================================BD============================================
    #====================================BD============================================
    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento,$varSessionBoss);
    $numrows = $db->numRows($query);
    if($numrows==0)
    {
        if( !empty($_POST['documento']) && !empty($_POST["ID"])){

            $sql="INSERT INTO pacientes
            (uploaded_by,
            documento, 
            tipo_de_documento, 
            primer_nombre, 
            segundo_nombre, 
            primer_apellido, 
            segundo_apellido,
            nombre_completo, 
            fecha_nacimiento, 
            edad, 
            sexo, 
            estado_civil,
            eps, 

            direccion_1, 
            direccion_2, 
            telefono, 
            celular, 
            ciudad, 
            nombre_medico, 
            entidad, 
            regimen,

            remitido_por,
            acompanante,
            telefono_acomp,
            parentesco_acomp,
            email,
            origen,
            nacionalidad,
            ocupacion,
            empresa,

            conyugue,
            documento_conyugue,
            fecha_nacimiento_conyugue,
            direccion_conyugue,
            telefono_conyugue,
            nombre_hijo1,
            sexo_hijo1,
            edad_hijo1,
            nombre_hijo2,
            sexo_hijo2,
            edad_hijo2,
            padre,
            edad_padre,
            madre,
            edad_madre,

            hobbie,
            etnia,
            dominancia,
            region,
            deporte,
            colegio,
            escolaridad,
            rendimiento) 

            VALUES(
            ?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,
            ?s,?s,?s,?s,?s,?s,?s,?s,?s,
            ?s,?s,?s,?s,?s,?s,?s,?s,
            ?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,
            ?s,?s,?s,?s,?s,?s,?s,?s)";

            $result = $db->query($sql,   
                Arreglar_String($varSessionBoss),
                Arreglar_String($varDocumento),
                Arreglar_String($varID),
                Arreglar_String($varNom1),
                Arreglar_String($varNom2),
                Arreglar_String($varApe1),
                Arreglar_String($varApe2),
                Arreglar_String($varNombreCompleto),
                $varBorn,
                Arreglar_String($varEdad),
                Arreglar_String($varGenero),
                Arreglar_String($varEstCivil),
                Arreglar_String($varEPS),

                Arreglar_String($varDireccion_1),
                Arreglar_String($varDireccion_2),
                Arreglar_String($varTelefono),
                Arreglar_String($varCelular),
                Arreglar_String($varCiudad),
                Arreglar_String($varMedico),
                Arreglar_String($varEntidad),
                Arreglar_String($varRegimen),

                Arreglar_String($varRemitido),
                Arreglar_String($varAcompana),
                Arreglar_String($varAcopanaTel),
                Arreglar_String($varAcopanaParentezco),
                Arreglar_String($varMail),
                Arreglar_String($varOrigen),
                Arreglar_String($varNacionalidad),
                Arreglar_String($varOcupacion),
                Arreglar_String($varEmpresa),

                Arreglar_String($varConyugueNom),
                Arreglar_String($varConyugueDoc),
                $varConyugueNacim,
                Arreglar_String($varConyugueDir),
                Arreglar_String($varConyugueTel),
                Arreglar_String($varHijo1),
                Arreglar_String($varHijo1Sex),
                Arreglar_String($varHijo1Edad),
                Arreglar_String($varHijo2),
                Arreglar_String($varHijo2Sex),
                Arreglar_String($varHijo2Edad),
                Arreglar_String($varPadre),
                Arreglar_String($varPadreEdad),
                Arreglar_String($varMadre),
                Arreglar_String($varMadreEdad),

                Arreglar_String($varHobbie),
                Arreglar_String($varEtnia),
                Arreglar_String($varDominancia),
                Arreglar_String($varRegion),
                Arreglar_String($varDeporte),
                Arreglar_String($varColegio),
                Arreglar_String($varEscolaridad),
                Arreglar_String($varRendimiento));
            #$result=mysqli_query($con,$sql);


            if($result){
                #$message = "Información agregada correctamente";
                $popup_content = "<div class='popup-tittle centered'>Información agregada correctamente</div>     <div class='popup-text centered'>¿Desea editar la informacion de la orden?</div> <div class='popup-buttons'> <div class='buttons-wrapper'><div class='ok-button' onclick='redirect_to_info_orden()'>Si</div> <div class='no-button' onclick='close_popup()'>No</div></div>  </div>";
                
                create_popup($popup_content);
                #echo "<script> location.href='historia.php'; </script>"; 
            } else {


                #$message = "Error al ingresar datos del paciente! - ";
                create_popup('Error al ingresar datos del paciente! - ');
                retenerDatos();
            }
        }else {
            #$message = "Debe ingresar el numero de documento y/o tipo de documento!";
            create_popup('Debe ingresar el numero de documento y/o tipo de documento!');
            retenerDatos();
        }

    } else if ($numrows==1){
        if( !empty($_POST['documento']) && !empty($_POST["ID"])){

            $sql = "UPDATE `pacientes` SET \n"
            . " tipo_de_documento = ?s,\n"
            . " primer_nombre = ?s,\n"
            . " segundo_nombre = ?s,\n"
            . " primer_apellido = ?s,\n"
            . " segundo_apellido = ?s,\n"
            . " nombre_completo = ?s,\n"
            . " fecha_nacimiento = ?s,\n"
            . " edad = ?s,\n"
            . " sexo = ?s,\n"
            . " estado_civil = ?s,\n"
            . " eps = ?s,\n"
            . " direccion_1 = ?s,\n"
            . " direccion_2 = ?s,\n"
            . " telefono = ?s,\n"
            . " celular = ?s,\n"
            . " ciudad = ?s,\n"
            . " nombre_medico = ?s,\n"
            . " entidad = ?s,\n"
            . " regimen = ?s,\n"
            . " remitido_por = ?s,\n"
            . " acompanante = ?s,\n"
            . " telefono_acomp = ?s,\n"
            . " parentesco_acomp = ?s,\n"
            . " email = ?s,\n"
            . " origen = ?s,\n"
            . " nacionalidad = ?s,\n"
            . " ocupacion = ?s,\n"
            . " empresa = ?s,\n"
            . " conyugue = ?s,\n"
            . " documento_conyugue = ?s,\n"
            . " fecha_nacimiento_conyugue = ?s,\n"
            . " direccion_conyugue = ?s,\n"
            . " telefono_conyugue = ?s,\n"
            . " nombre_hijo1 = ?s,\n"
            . " sexo_hijo1 = ?s,\n"
            . " edad_hijo1 = ?s,\n"
            . " nombre_hijo2 = ?s,\n"
            . " sexo_hijo2 = ?s,\n"
            . " edad_hijo2 = ?s,\n"
            . " padre = ?s,\n"
            . " edad_padre = ?s,\n"
            . " madre = ?s,\n"
            . " edad_madre = ?s,\n"
            . " hobbie = ?s,\n"
            . " etnia = ?s,\n"
            . " dominancia = ?s,\n"
            . " region = ?s,\n"
            . " deporte = ?s,\n"
            . " colegio = ?s,\n"
            . " escolaridad = ?s,\n"
            . " rendimiento = ?s\n"
            . " WHERE `pacientes`.`documento` = ?s AND `pacientes`.`uploaded_by` = ?s";

            $result = $db->query($sql,
                Arreglar_String($varID),
                Arreglar_String($varNom1),
                Arreglar_String($varNom2),
                Arreglar_String($varApe1),
                Arreglar_String($varApe2),
                Arreglar_String($varNombreCompleto),
                $varBorn,
                Arreglar_String($varEdad),
                Arreglar_String($varGenero),
                Arreglar_String($varEstCivil),
                Arreglar_String($varEPS),

                Arreglar_String($varDireccion_1),
                Arreglar_String($varDireccion_2),
                Arreglar_String($varTelefono),
                Arreglar_String($varCelular),
                Arreglar_String($varCiudad),
                Arreglar_String($varMedico),
                Arreglar_String($varEntidad),
                Arreglar_String($varRegimen),

                Arreglar_String($varRemitido),
                Arreglar_String($varAcompana),
                Arreglar_String($varAcopanaTel),
                Arreglar_String($varAcopanaParentezco),
                Arreglar_String($varMail),
                Arreglar_String($varOrigen),
                Arreglar_String($varNacionalidad),
                Arreglar_String($varOcupacion),
                Arreglar_String($varEmpresa),

                Arreglar_String($varConyugueNom),
                Arreglar_String($varConyugueDoc),
                $varConyugueNacim,
                Arreglar_String($varConyugueDir),
                Arreglar_String($varConyugueTel),
                Arreglar_String($varHijo1),
                Arreglar_String($varHijo1Sex),
                Arreglar_String($varHijo1Edad),
                Arreglar_String($varHijo2),
                Arreglar_String($varHijo2Sex),
                Arreglar_String($varHijo2Edad),
                Arreglar_String($varPadre),
                Arreglar_String($varPadreEdad),
                Arreglar_String($varMadre),
                Arreglar_String($varMadreEdad),

                Arreglar_String($varHobbie),
                Arreglar_String($varEtnia),
                Arreglar_String($varDominancia),
                Arreglar_String($varRegion),
                Arreglar_String($varDeporte),
                Arreglar_String($varColegio),
                Arreglar_String($varEscolaridad),
                Arreglar_String($varRendimiento),
                $varDocumento,
                $varSessionBoss);

            if($result){
                #$message = "Información editada correctamente";
                $popup_content = "<div class='popup-tittle centered'>Información agregada correctamente</div>     <div class='popup-text centered'>¿Desea editar la informacion de la orden?</div> <div class='popup-buttons'> <div class='buttons-wrapper'><div class='ok-button' onclick='redirect_to_info_orden()'>Si</div> <div class='no-button' onclick='close_popup()'>No</div></div>  </div>";
                
                create_popup($popup_content);
            } else {
                #$message = "Error al editar datos del paciente! - ";
                create_popup('Error al editar datos del paciente! - ');
                retenerDatos();
            }
        }else {
            #$message = "Debe ingresar el numero de documento y/o tipo de documento!";
            create_popup('Debe ingresar el numero de documento y/o tipo de documento!');
            #----------------retener los datos ingresados--------------
            retenerDatos();
        }
    }
}
?>


<?php 
function retenerDatos()
{
    $_SESSION['documento'] = isset_empty_and_set($_POST['documento']);
    $_SESSION['tipoDocumento'] = isset_empty_and_set($_POST["ID"]);
    $_SESSION['Nombre_1'] = isset_empty_and_set($_POST['nombre_1']);
    $_SESSION['Nombre_2'] = isset_empty_and_set($_POST['nombre_2']);
    $_SESSION['Apellido_1'] = isset_empty_and_set($_POST['apellido_1']);
    $_SESSION['Apellido_2'] = isset_empty_and_set($_POST['apellido_2']);
    $_SESSION['nacimiento'] = isset_empty_and_set($_POST['fecha-nacimiento']);
    $_SESSION['Genero'] = isset_empty_and_set($_POST['sexo_paciente']);
    $_SESSION['EstadoCivil'] = isset_empty_and_set($_POST['estado-civil']);

    $_SESSION['Dir_1'] = isset_empty_and_set($_POST['direccion_1']);
    $_SESSION['Dir_2'] = isset_empty_and_set($_POST['direccion_2']);
    $_SESSION['Telefono_paciente'] = isset_empty_and_set($_POST['telefono']);
    $_SESSION['Celular_paciente'] = isset_empty_and_set($_POST['celular']);
    $_SESSION['Ciudad_paciente'] = isset_empty_and_set($_POST['ciudad']);
    $_SESSION['Medico'] = isset_empty_and_set($_POST['medico']);
    $_SESSION['Entidad'] = isset_empty_and_set($_POST['entidad']);
    $_SESSION['Regimen'] = isset_empty_and_set($_POST['regimen']);
    $_SESSION['Remitido'] = isset_empty_and_set($_POST['remitido']);

    $_SESSION['AcompananteNombre'] = isset_empty_and_set($_POST['acompana']);
    $_SESSION['AcompananteTel'] = isset_empty_and_set($_POST['acompana-tel']);
    $_SESSION['acompananteParentezco'] = isset_empty_and_set($_POST['acompana-parentezco']);
    $_SESSION['Mail'] = isset_empty_and_set($_POST['mail']);
    $_SESSION['Origen'] = isset_empty_and_set($_POST['origen']);
    $_SESSION['Nacionalidad'] = isset_empty_and_set($_POST['nacionalidad']);
    $_SESSION['Ocupacion'] = isset_empty_and_set($_POST['ocupacion']);
    $_SESSION['Empresa'] = isset_empty_and_set($_POST['empresa']);

    $_SESSION['ConyugueNombre'] = isset_empty_and_set($_POST['conyugue']);
    $_SESSION['ConyugueDocumento'] = isset_empty_and_set($_POST['conyugue_doc']);
    $_SESSION['ConyugueNacimiento'] = isset_empty_and_set($_POST['conyugue_nacim']);
    $_SESSION['ConyugueDireccion'] = isset_empty_and_set($_POST['conyugue_direccion']);
    $_SESSION['ConyugueTelefono'] = isset_empty_and_set($_POST['conyugue_tel']);
    $_SESSION['Hijo1Nombre'] = isset_empty_and_set($_POST['hijo_1']);
    $_SESSION['Hijo1Sexo'] = isset_empty_and_set($_POST['sexo_hijo_1']);
    $_SESSION['Hijo1Edad'] = isset_empty_and_set($_POST['hijo_1_edad']);
    $_SESSION['Hijo2Nombre'] = isset_empty_and_set($_POST['hijo_2']);
    $_SESSION['Hijo2Sexo'] = isset_empty_and_set($_POST['sexo_hijo_2']);
    $_SESSION['Hijo2Edad'] = isset_empty_and_set($_POST['hijo_2_edad']);
    $_SESSION['Padre'] = isset_empty_and_set($_POST['nombre_padre']);
    $_SESSION['PadreEdad'] = isset_empty_and_set($_POST['edad_padre']);
    $_SESSION['Madre'] = isset_empty_and_set($_POST['nombre_madre']);
    $_SESSION['MadreEdad'] = isset_empty_and_set($_POST['edad_madre']);

    $_SESSION['Hobbie'] = isset_empty_and_set($_POST['hobbie']);
    $_SESSION['Etnia'] = isset_empty_and_set($_POST['etnia']);
    $_SESSION['Dominancia'] = isset_empty_and_set($_POST['dominancia']);
    $_SESSION['Region'] = isset_empty_and_set($_POST['religion']);
    $_SESSION['Deporte'] = isset_empty_and_set($_POST['deporte']);
    $_SESSION['Colegio'] = isset_empty_and_set($_POST['colegio']);
    $_SESSION['Escolaridad'] = isset_empty_and_set($_POST['escolaridad']);
    $_SESSION['Rendimiento'] = isset_empty_and_set($_POST['rendimiento']);
}
?>
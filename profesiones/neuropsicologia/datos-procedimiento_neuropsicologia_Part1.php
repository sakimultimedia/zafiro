<?php 

if (isset($_SESSION['documento'])) {
    # code...
    $varDocumento = $_SESSION['documento'];

    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
    $numrows = $db->numRows($query);

    $query_info_orden = $db->query("SELECT * FROM informacion_orden WHERE id_paciente=?s",$varDocumento);

    if($numrows==1)
    {   
        $row_pacientes = $db->fetch($query);
        $row_info_orden = $db->fetch($query_info_orden);

        #guardar datos del paciente para mostrarse en otras paginas
        $_SESSION['Medico'] = $row_pacientes["nombre_medico"];
        $_SESSION['Entidad'] = $row_pacientes["entidad"];
        $_SESSION['Remitido'] = $row_pacientes["remitido_por"];


        $_SESSION['numAutorizacion'] = $row_info_orden["numero_autorizacion"];
        $_SESSION['tipoConsulta'] = $row_info_orden["tipo_consulta"];
        $_SESSION['especialidad'] = $row_info_orden["especialidad"];
        $_SESSION['procedimiento'] = $row_info_orden["procedimiento"];
        $_SESSION['sesiones'] = $row_info_orden["num_sesiones"];
        $_SESSION['personalAtiende'] = $row_info_orden["personal_atiende"];
        $_SESSION['dxPrev'] = $row_info_orden["dx_previo"];
        $_SESSION['dxAdicional'] = $row_info_orden["dx_adicional"];
    }

}


if(isset($_POST["guardar"])){

    $varNumAutorizacion = isset_empty_and_set($_POST["autorizacion"]);
    $varTipoConsulta = isset_empty_and_set($_POST["tipo_consulta"]);
    $varEspecialidad = isset_empty_and_set($_POST["interno"]);
    $varProcedimiento = isset_empty_and_set($_POST["procedimiento"]);
    $varSesiones = isset_empty_and_set($_POST["sesion"]);
    $varPersonalAtiende = isset_empty_and_set($_POST["atiende"]);
    $varDxPrev = isset_empty_and_set($_POST["prev_dx"]);
    $varDxAdicional = isset_empty_and_set($_POST["new_dx"]);

    $varEntidad = isset_empty_and_set($_POST["entidad"]);
    $varMedico = isset_empty_and_set($_POST["medico"]);
    $varRemitido = isset_empty_and_set($_POST["referido"]);

    $_SESSION['numAutorizacion'] = isset_empty_and_set($varNumAutorizacion);
    $_SESSION['tipoConsulta'] = isset_empty_and_set($varTipoConsulta);
    $_SESSION['especialidad'] = isset_empty_and_set($varEspecialidad);
    $_SESSION['procedimiento'] = isset_empty_and_set($varProcedimiento);
    $_SESSION['sesiones'] = isset_empty_and_set($varSesiones);
    $_SESSION['personalAtiende'] = isset_empty_and_set($varPersonalAtiende);
    $_SESSION['dxPrev'] = isset_empty_and_set($varDxPrev);
    $_SESSION['dxAdicional'] = isset_empty_and_set($varDxAdicional);

    $_SESSION['Entidad'] = isset_empty_and_set($varEntidad);
    $_SESSION['Medico'] = isset_empty_and_set($varMedico);
    $_SESSION['Remitido'] = isset_empty_and_set($varRemitido);


    $idPaciente = $_SESSION['documento']; 
    #echo "id paciente = ".$idPaciente;
    $query = $db->query("SELECT * FROM informacion_orden WHERE numero_autorizacion=?s",$varNumAutorizacion);
    $numrows = $db->numRows($query);

    if($numrows==0){
        if(isset_and_empty($varNumAutorizacion)){
            $sql="INSERT INTO informacion_orden
            (numero_autorizacion, id_paciente, entidad, medico, tipo_consulta, referido_por, especialidad, procedimiento, num_sesiones, personal_atiende, dx_previo, dx_adicional) 
            VALUES( ?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s,?s)"; 

            $result = $db->query($sql, Arreglar_String($varNumAutorizacion),
                                            Arreglar_String($idPaciente),
                                            Arreglar_String($varEntidad),
                                            Arreglar_String($varMedico),
                                            Arreglar_String($varTipoConsulta),
                                            Arreglar_String($varRemitido),
                                            Arreglar_String($varEspecialidad),
                                            Arreglar_String($varProcedimiento),
                                            Arreglar_String($varSesiones),
                                            Arreglar_String($varPersonalAtiende),
                                            Arreglar_String($varDxPrev),
                                            Arreglar_String($varDxAdicional));
            if($result){
                #echo "se subio...";
                #$message = "Información agregada correctamente";
                create_popup('Información agregada correctamente');    
            } else {
                #$message = "Error al ingresar la informacion de la orden!";
                create_popup('Error al ingresar la informacion de la orden!');
                retenerDatos();
            }
        }else{
            #$message = "Debes ingresar el numero de autorizacion!";
            create_popup('Debes ingresar el numero de autorizacion!');
            retenerDatos();
        } 
    } elseif ($numrows==1) {
        if(isset_and_empty($varNumAutorizacion)){
            $sql = "UPDATE `informacion_orden` SET \n"
            . " id_paciente = ?s,\n"
            . " entidad = ?s,\n"
            . " medico = ?s,\n"
            . " tipo_consulta = ?s,\n"
            . " referido_por = ?s,\n"
            . " especialidad = ?s,\n"
            . " procedimiento = ?s,\n"
            . " num_sesiones = ?s,\n"
            . " personal_atiende = ?s,\n"
            . " dx_previo = ?s,\n"
            . " dx_adicional = ?s\n"
            . " WHERE `informacion_orden`.`numero_autorizacion` = ?s";

            $result = $db->query($sql,      Arreglar_String($idPaciente),
                                            Arreglar_String($varEntidad),
                                            Arreglar_String($varMedico),
                                            Arreglar_String($varTipoConsulta),
                                            Arreglar_String($varRemitido),
                                            Arreglar_String($varEspecialidad),
                                            Arreglar_String($varProcedimiento),
                                            Arreglar_String($varSesiones),
                                            Arreglar_String($varPersonalAtiende),
                                            Arreglar_String($varDxPrev),
                                            Arreglar_String($varDxAdicional),
                                            Arreglar_String($varNumAutorizacion));
            if($result){
                #echo "se subio...";
                #$message = "Información editada correctamente";  
                create_popup('Información editada correctamente');  
            } else {
                #$message = "Error al ingresar la informacion de la orden!";
                create_popup('Error al ingresar la informacion de la orden!');
                retenerDatos();
            }
        }else{
            #$message = "Debes ingresar el numero de autorizacion!";
            create_popup('Debes ingresar el numero de autorizacion!');
            retenerDatos();
        }  
    }

    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s",$idPaciente);
    $numrows = $db->numRows($query);

    if ($numrows==1) {
        if(isset_and_empty($idPaciente)){
            $sql = "UPDATE `pacientes` SET \n"
            . " entidad = ?s,\n"
            . " nombre_medico = ?s,\n"
            . " remitido_por = ?s\n"
            . " WHERE `pacientes`.`documento` = ?s";

            $result = $db->query($sql,  Arreglar_String($varEntidad),
                                        Arreglar_String($varMedico),
                                        Arreglar_String($varRemitido),
                                        $idPaciente);
            if($result){
                #echo "se subio...";
                #$message = "Información editada correctamente";  
                create_popup('Información del paciente editada correctamente'); 
            } else {
                #$message = "Error al ingresar la informacion de la orden!";
                create_popup('Error al ingresar la informacion del paciente!');
                retenerDatos();
            }
        }else{
            #$message = "Debes ingresar el numero de autorizacion!";
            create_popup('Debes elegir un paciente antes de realizar la orden');
            retenerDatos();
        }  
    }
}


function retenerDatos()
{
    $_SESSION['numAutorizacion'] = isset_empty_and_set($_POST["autorizacion"]);
    $_SESSION['tipoConsulta'] = isset_empty_and_set($_POST["tipo_consulta"]);
    $_SESSION['especialidad'] = isset_empty_and_set($_POST["interno"]);
    $_SESSION['procedimiento'] = isset_empty_and_set($_POST["procedimiento"]);
    $_SESSION['sesiones'] = isset_empty_and_set($_POST["sesion"]);
    $_SESSION['personalAtiende'] = isset_empty_and_set($_POST["atiende"]);
    $_SESSION['dxPrev'] = isset_empty_and_set($_POST["prev_dx"]);
    $_SESSION['dxAdicional'] = isset_empty_and_set($_POST["new_dx"]);

    $_SESSION['Entidad'] = isset_empty_and_set($_POST["entidad"]);
    $_SESSION['Medico'] = isset_empty_and_set($_POST["medico"]);
    $_SESSION['Remitido'] = isset_empty_and_set($_POST["referido"]);
}
?>



<script type="text/javascript">
    function search_procediento(){
        var value = $("#autorizacion").val();

        if (value=="") {
            return;
        } 
            if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                var response = xmlhttp.responseText;
                var list = response.split("\n");
                //console.log(list);
                var fields = new Array();
                for (i = 0; i < (list.length)-1; i++) {
                    console.log(list[i]);
                    var field = list[i].split(" = ");
                    console.log(field);
                    fields[field[0]] = field[1];
                }
                //console.log(fields);
                //create_popup(response);
                //$db->getOne("SELECT nombre FROM tipo_consulta WHERE id=?s",fields["tipo_consulta"]);
                //$('#tipo_consulta [value='+fields["tipo_consulta"]+']').attr('selected',true);
                $("#tipo_consulta").val(fields["tipo_consulta"]);
                $("#interno").val(fields["especialidad"]);
                $("#procedimiento").val(fields["procedimiento"]);
                $("#sesion").val(fields["num_sesiones"]);
                $("#atiende").val(fields["personal_atiende"]);
                $("#prev_dx").val(fields["dx_previo"]);

                $("#entidad").val(fields["entidad"]);
                $("#medico").val(fields["medico"]);
                $("#referido").val(fields["referido"]);
     
            }
        }
        xmlhttp.open("GET","utils/search_procediento.php?aut="+value,true);
        xmlhttp.send();
    }

    function search_procediento2(val){
        var value = val;

        if (value=="") {
            return;
        } 
            if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                var response = xmlhttp.responseText;
                var list = response.split("\n");
                //console.log(list);
                var fields = new Array();
                for (i = 0; i < (list.length)-1; i++) {
                    console.log(list[i]);
                    var field = list[i].split(" = ");
                    console.log(field);
                    fields[field[0]] = field[1];
                }
                //console.log(fields);
                //create_popup(response);
                //$db->getOne("SELECT nombre FROM tipo_consulta WHERE id=?s",fields["tipo_consulta"]);
                //$('#tipo_consulta [value='+fields["tipo_consulta"]+']').attr('selected',true);
                $("#autorizacion").val(value);
                $("#tipo_consulta").val(fields["tipo_consulta"]);
                $("#interno").val(fields["especialidad"]);
                $("#procedimiento").val(fields["procedimiento"]);
                $("#sesion").val(fields["num_sesiones"]);
                $("#atiende").val(fields["personal_atiende"]);
                $("#prev_dx").val(fields["dx_previo"]);

                $("#entidad").val(fields["entidad"]);
                $("#medico").val(fields["medico"]);
                $("#referido").val(fields["referido"]);
     
            }
        }
        xmlhttp.open("GET","utils/search_procediento.php?aut="+value,true);
        xmlhttp.send();
    }
</script>

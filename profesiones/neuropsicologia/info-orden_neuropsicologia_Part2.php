<form class="ajaxform" action= "<?echo $ZafiroRute?>info-orden.php" method="POST" id="basic-data" mensajeExito = "Los datos básicos han sido correctamente guardados." mensajeError = "Ha habido un error y tus datos no fueron guardados. intentalo más tarde.">

    <div class="container">

            <section id="datos-procedimiento">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-label">
                            Información de la orden
                        </div>
                        <div class="title-separador"></div>
                    </div><!-- end col -->

                    <div class="col-sm-4">
                        <div class="top-label">
                            Agregar nueva autorización
                        </div>
                        <?php $varAut = $_SESSION['numAutorizacion'];?>
                        <input required type="text" name="autorizacion" id="autorizacion" placeholder="Escribe el tipo de autorización">

                        <div class="top-label">
                            Fecha de la orden
                        </div>
                        <input required type="date" name="fecha-orden" id="fecha-orden">
                        <script>
                            document.getElementById("fecha-orden").value = formatDate(new Date());
                        </script>
                    </div><!-- end col -->

                    <div class="col-sm-8">
                        <div class="top-label">
                            Autorizaciones del paciente
                        </div>
                           
                        <?php 

                        $query_info_orden = $db->query("SELECT * FROM informacion_orden WHERE id_paciente=?s",$_SESSION["documento"]);
                        $numrows = $db->numRows($query_info_orden);

                        for ($i=$numrows; $i > 0 ; $i--) {
                            $row_info_orden = $db->fetch($query_info_orden);

                            $varNumAutorizacion = $row_info_orden["numero_autorizacion"];
                            $varDate = $row_info_orden["fecha_procedimiento"];
                            
                            echo "<div id='the-info'>".
                                    "<div class='the-auth'>".
                                        "<b>Numero de la autorización:</b> <br>".$varNumAutorizacion.
                                    "</div>".
                                    "<div class='the-date'>".
                                        "<b>Fecha del procedimiento:</b> <br>".$varDate.
                                    "</div>".
                                "</div>";
                            
                            echo '<div class="separador-clinica"></div>';
                        }
                        ?>
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                <div class="col-sm-12">
                    <input type="submit" name="guardar" value="Guardar" class="send-btn">
                </div>

            </div>
            </section>

    </div><!-- end container -->
</form><!-- end form -->

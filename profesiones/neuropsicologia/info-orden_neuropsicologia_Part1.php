<?php

if (isset($_SESSION['documento'])) {
    $varDocumento = $_SESSION['documento'];
    
    $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
    $numrows = $db->numRows($query);

    if($numrows==1)
    {    #guardar datos del paciente para mostrarse en otras paginas
        $row = $db->fetch($query);
        $_SESSION['numAutorizacion'] = $db->getOne("SELECT numero_autorizacion 
                                                    FROM informacion_orden 
                                                    WHERE id_paciente=?s",$varDocumento);

    }elseif($numrows==0){
        #guardar datos del paciente para mostrarse en otras paginas
        $_SESSION['numAutorizacion'] = ""; 
    }
}


#variables de la pagina
$varNumAutorizacion = isset_empty_and_set($_SESSION['numAutorizacion']);

$varDocumento = isset_empty_and_set($_SESSION['documento']);
$varEntidad = isset_empty_and_set($_SESSION['Entidad']);
$varMedico = isset_empty_and_set($_SESSION['Medico']);
$varRemitido = isset_empty_and_set($_SESSION['Remitido']);

# ========  SI GUARDA EL FORM ========
if(isset($_POST["guardar"])){

    #===========================datos-procedimiento================================
    $varNumAutorizacion = isset_empty_and_set($_POST["autorizacion"]);
    $_SESSION['numAutorizacion'] = isset_and_set($varNumAutorizacion);

    $varFechaOrden = isset_empty_and_set($_POST['fecha-orden']);
    $_SESSION['FechaOrden'] = isset_and_set($varNumAutorizacion);

    #====================================BD=========================================

    $query = $db->query("SELECT * FROM informacion_orden WHERE numero_autorizacion=?s",$varNumAutorizacion);
    $numrows = $db->numRows($query);

    if($numrows==0){
        if(isset_and_empty($varNumAutorizacion)){
            $sql="INSERT INTO informacion_orden
            (numero_autorizacion, fecha_procedimiento, id_paciente, entidad, medico, referido_por) 
            VALUES( ?s,?s,?s,?s,?s,?s)"; 

            $result = $db->query($sql, Arreglar_String($varNumAutorizacion),
                                            Arreglar_String($varFechaOrden),
                                            Arreglar_String($varDocumento),
                                            Arreglar_String($varEntidad),
                                            Arreglar_String($varMedico),
                                            Arreglar_String($varRemitido));
            if($result){
                create_popup('Información agregada correctamente');    
            } else {
                create_popup('Error al ingresar la informacion de la orden!');
                retenerDatos();
            }
        }else{
            create_popup('Debes ingresar el numero de autorizacion!');
            retenerDatos();
        } 
    } elseif ($numrows==1) {
        if(isset_and_empty($varNumAutorizacion)){
            $sql = "UPDATE informacion_orden SET \n"
            . " fecha_procedimiento = ?s,\n"
            . " id_paciente = ?s,\n"
            . " entidad = ?s,\n"
            . " medico = ?s,\n"
            . " referido_por = ?s\n"   
            . " WHERE numero_autorizacion = ?s";

            $result = $db->query($sql,  Arreglar_String($varFechaOrden),
                                        Arreglar_String($varDocumento),
                                        Arreglar_String($varEntidad),
                                        Arreglar_String($varMedico),
                                        Arreglar_String($varRemitido),
                                        Arreglar_String($varNumAutorizacion));
            if($result){
                #$message = "Información editada correctamente";  
                create_popup('Información editada correctamente');  
            } else {
                #$message = "Error al ingresar la informacion de la orden!";
                create_popup('Error al ingresar la informacion de la orden!');
                retenerDatos();
            }
        }else{
            #$message = "Debes ingresar el numero de autorizacion!";
            create_popup('Debes ingresar el numero de autorizacion!');
            retenerDatos();
        }  
    }
}


function retenerDatos(){
    $_SESSION['numAutorizacion'] = isset_empty_and_set($_POST["autorizacion"]);
}
?>
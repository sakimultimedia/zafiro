
<section id="historias-pasadas">
    <?php 

    $query = $db->query("SELECT * FROM historia_clinica WHERE documento_paciente=?s",$varDocumento);
    $numrows = $db->numRows($query);

    for ($i=$numrows; $i > 0 ; $i--) {
        $row = $db->fetch($query);
        ?>

        <div class="row" style="padding: 0 10px;">
            <div class="big-label" style="margin-bottom: 25px;">
                <?php 
                $FECHA = $row['fecha_hora'];
                echo "Consulta de la fecha <span style='color: #A92F12;font-weight: 400;'>". $FECHA . "</span>"; 
                ?>
            </div>

            <div> 
                <?php 
                $txt = '';

                $medicName = $row['medic_name'];
                $Id_Profesion = $db->getOne("SELECT profesion FROM usuarios WHERE username =?s",$medicName);
                $txt = $txt. "<b style='font-size: 1em;'>Profesional a cargo de la consulta:</b> <br>".
                "<div style='font-size: 1em;'>".
                    $db->getOne("SELECT real_name FROM usuarios WHERE username =?s",$medicName).
                    " - "
                    .$db->getOne("SELECT nombre FROM profesiones WHERE id = ?s ",$Id_Profesion).
                "</div><br>";

                if (isset_and_empty($row['cometarios_medico'])){
                    $txt = $txt. "<div style='font-size: 1.2em;'> Durante la consulta de la fecha (".$FECHA.") se encontró lo siguiente :</div><br>";
                    $comentarioMedico = $row['cometarios_medico'];
                    $txt =  $txt. "<div style='font-size: 1em;'>". $comentarioMedico."</div><br>"; 
                }

                $dxPost = $row['dx_posterior'];
                if ($dxPost != "") {
                    $txt = $txt. "<div style='font-size: 1.2em;'><b Diagnostico de la ultima consulta:</b></div> <br>";
                    $txt = $txt. $dxPost."<br><br>";
                }


                $archivos_unidos = $row['link_archivo'];
                $archivos = explode("*+*", $archivos_unidos);

                $max = sizeof($archivos);
                if ($archivos_unidos != "") {
                    $txt = $txt. "<b style='font-size: 1.2em;'>Archivos relevantes para la consulta:</b> <br><br>";
                    for($j = 0; $j < $max;$j++){
                        $ext = pathinfo($archivos[$j], PATHINFO_EXTENSION);
                        if ($ext == "jpg" || $ext == "jpe" || $ext == "jpeg" || $ext == "png") {
                            $imgsrc1 = explode("archivos/", $archivos[$j]);
                            $imgsrc2 = "http://zafiroweb.co/filesManager.php?file=".$imgsrc1[1];
                            $img = "<div style='text-align: center;'><img style='width: 100%;' src=".$imgsrc2."></div>";
                            $txt = $txt. "<a href='".$archivos[$j]."' target='_blank'>".$img."</a><br>";                                         
                        }elseif ($ext == "pdf" ) {
                            $img = "<div style='text-align: center;'><img style='    width: 200px;' src='img/pdf.png'></div>";
                            $txt = $txt. "<a href='".$archivos[$j]."' target='_blank'>".$img."</a><br>";                                         
                        }elseif ($ext == "docx" || $ext == "doc" ) {
                            $img = "<div style='text-align: center;'><img style='width: 200px;' src='img/word.png'></div>";
                            $txt = $txt. "<a href='".$archivos[$j]."' target='_blank'>".$img."</a><br>";                                         
                        }else{
                            $txt = $txt. "<a href='".$archivos[$j]."' target='_blank'>".$archivos[$j]."</a><br>";
                        }
                            /*
                            $im = new imagick('file.pdf[0]');
                            $im->setImageFormat('jpg');
                            header('Content-Type: image/jpeg');
                            echo $im;
                            */
                        }
                    }

                    echo $txt;
                    ?>
                </div>
            </div>
    <?php
    }
    ?>

</section><!-- end historias pasadas -->
<section id="basicos_history">
        <div class="col-sm-12">
            <div class="section-label">
                Historia Clínica
            </div>
            <div class="title-separador"></div>
        </div><!-- end col -->

        <!--<div class="col-sm-12">
            <div class="section-label">
                <?php if (!empty($message)) {echo "<p class=\"error\">" . "MESSAGE: ". $message . "</p>";} ?>
            </div>
        </div> end col -->
        <?php 
        $Nom1 = $db->getOne("SELECT primer_nombre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $Nom2 = $db->getOne("SELECT segundo_nombre FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $Ape1 = $db->getOne("SELECT primer_apellido FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);
        $Ape2 = $db->getOne("SELECT segundo_apellido FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento, $varSessionBoss);

        $numPaciente = $db->getOne("SELECT numero_paciente FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento,$varSessionBoss);
        $NombreCompleto = $Nom1;
        if (isset_and_empty($Nom2)) {$NombreCompleto = $NombreCompleto." ".$Nom2;}
        if (isset_and_empty($Ape1)) {$NombreCompleto = $NombreCompleto." ".$Ape1;}
        if (isset_and_empty($Ape2)) {$NombreCompleto = $NombreCompleto." ".$Ape2;} 
        ?>
            <div style="padding: 0 10px;">

                <div class="big-label">
                    Datos personales del paciente <?php echo "# $numPaciente: $NombreCompleto"; ?>
                </div>

                <div class="row">
                    <div class="basic-hist">
                        <div class="col-sm-12">
                            <div class="sub-label">
                                Datos básicos
                            </div>
                            <div class="subtitle-separador"></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-hist-label">
                                <?php 
                                $varTipoDocumento = $db->getOne("SELECT nombre FROM tipo_documento WHERE id=?s",$varTipoDocumento);
                                echo $varTipoDocumento;
                                ?>
                            </div>
                            <div class="the-info">
                                <?php 
                                echo $varDocumento;
                                ?>
                            </div>
                        </div>
                        <?php 
                        if ($varNom1 != '' && $varNom1 != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Nombre</div>"."<div class='the-info'>".$varNom1."</div>"."</div>";
                        }
                        if ($varNom2 != '' && $varNom2 != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Segundo nombre</div>"."<div class='the-info'>".$varNom2."</div>"."</div>";
                        }
                        if ($varApe1 != '' && $varApe1 != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Primer apellido</div>"."<div class='the-info'>".$varApe1."</div>"."</div>";
                        }
                        if ($varApe2 != '' && $varApe2 != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Segundo Apellido</div>"."<div class='the-info'>".$varApe2."</div>"."</div>";
                        }
                        if ($varBorn != '' && $varBorn != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Fecha de nacimiento</div>"."<div class='the-info'>".$varBorn."</div>"."</div>";
                        }
                        if ($varEdad != '' && $varEdad != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Edad</div>"."<div class='the-info'>".$varEdad."</div>"."</div>";
                        }
                        if ($varGenero != '' && $varGenero != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Género</div>"."<div class='the-info'>".$varGenero."</div>"."</div>";
                        }
                        if ($varEstCivil != '' && $varEstCivil != ' ') {
                            $varEstCivil = $db->getOne("SELECT nombre FROM estado_civil WHERE id=?s",$varEstCivil);
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Estado civil</div>"."<div class='the-info'>".$varEstCivil."</div>"."</div>";
                        }
                        if ($varEPS != '' && $varEPS != ' ') {
                            $varEPS = $db->getOne("SELECT nombre FROM eps WHERE id=?s",$varEPS);
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Eps</div>"."<div class='the-info'>".$varEPS."</div>"."</div>";
                        }
                        ?>
                    </div><!-- end basic-hist -->
                </div><!-- end row -->

                <div class="separador-clinica"></div>

                <div class="row">
                    <div class="basic-hist">
                        <div class="col-sm-12">
                            <div class="sub-label">
                                Información Personal
                            </div>
                            <div class="subtitle-separador"></div>
                        </div>
                        <?php 
                        if ($varDireccion_1 != '' && $varDireccion_1 != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Dirección</div>"."<div class='the-info'>".$varDireccion_1."</div>"."</div>";
                        }
                        if ($varDireccion_2 != '' && $varDireccion_2 != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Dirección alternativa</div>"."<div class='the-info'>".$varDireccion_2."</div>"."</div>";
                        }
                        if ($varTelefono != '' && $varTelefono != ' ') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Teléfono</div>"."<div class='the-info'>".$varTelefono."</div>"."</div>";
                        }
                        if ($varCelular != '' && $varCelular != ' ' && $varCelular != '0') {
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Celular</div>"."<div class='the-info'>".$varCelular."</div>"."</div>";
                        }

                        if ($varCiudad != '' && $varCiudad != ' ') {
                            $varCiudad = $db->getOne("SELECT nombre FROM ciudad WHERE id=?s",$varCiudad);
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Ciudad</div>"."<div class='the-info'>".$varCiudad."</div>"."</div>";
                        }

                        if ($varMedico != '' && $varMedico != ' ') {
                            $varNombreMedico = $db->getOne("SELECT nombre FROM nombre_medico WHERE id=?s",$varMedico);
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Médico</div>"."<div class='the-info'>".$varNombreMedico."</div>"."</div>";
                        }
                        if ($varRegimen != '' && $varRegimen != ' ') {
                            $varRegimen = $db->getOne("SELECT nombre FROM regimen WHERE id=?s",$varRegimen);
                            echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Régimen</div>"."<div class='the-info'>".$varRegimen."</div>"."</div>";
                        }
                        ?>
                    </div><!-- end basic-hist -->
                </div><!-- end row -->

                <?php if (  ($varAcompana != '' && $varAcompana != ' ') ||
                    ($varAcopanaTel != '' && $varAcopanaTel != ' ' && $varAcopanaTel != '0') ||
                    ($varAcopanaParentezco != '' && $varAcopanaParentezco != ' ') ||
                    ($varMail != '' && $varMail != ' ') ||
                    ($varOrigen != '' && $varOrigen != ' ') ||
                    ($varNacionalidad != '' && $varNacionalidad != ' ') ||
                    ($varOcupacion != '' && $varOcupacion != ' ') ||
                    ($varEmpresa != '' && $varEmpresa != ' ')                   
                    ) { ?>
                    <div class="separador-clinica"></div>
                    <div class="row">
                        <div class="basic-hist">
                            <div class="col-sm-12">
                                <div class="sub-label">
                                    Ocupación
                                </div>
                                <div class="subtitle-separador"></div>
                            </div>
                            <?php 
                            if ($varAcompana != '' && $varAcompana != ' ') {
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Acompañante</div>"."<div class='the-info'>".$varAcompana."</div>"."</div>";
                            }
                            if ($varAcopanaTel != '' && $varAcopanaTel != ' ' && $varAcopanaTel != '0') {
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Teléfono del acompañante</div>"."<div class='the-info'>".$varAcopanaTel."</div>"."</div>";
                            }
                            if ($varAcopanaParentezco != '' && $varAcopanaParentezco != ' ') {
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Parentesco del acompañante</div>"."<div class='the-info'>".$varAcopanaParentezco."</div>"."</div>";
                            }
                            if ($varMail != '' && $varMail != ' ') {
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>E-mail</div>"."<div class='the-info'>".$varMail."</div>"."</div>";
                            }
                            if ($varOrigen != '' && $varOrigen != ' ') {
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Ciudad de nacimiento</div>"."<div class='the-info'>".$varOrigen."</div>"."</div>";
                            }
                            if ($varNacionalidad != '' && $varNacionalidad != ' ') {
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Nacionalidad</div>"."<div class='the-info'>".$varNacionalidad."</div>"."</div>";
                            }
                            if ($varOcupacion != '' && $varOcupacion != ' ') {
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Ocupación</div>"."<div class='the-info'>".$varOcupacion."</div>"."</div>";
                            }
                            if ($varEmpresa != '' && $varEmpresa != ' ') {
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Empresa</div>"."<div class='the-info'>".$varEmpresa."</div>"."</div>";
                            }
                            ?>
                        </div><!-- end basic-hist -->
                    </div><!-- end row -->
                    <?php } ?>


                    <?php if (  ($varConyugueNom != '' && $varConyugueNom != ' ') ||
                        ($varConyugueDoc != '' && $varConyugueDoc != ' ') ||
                        ($varConyugueNacim != '' && $varConyugueNacim != ' ' && $varConyugueNacim != '0000-00-00') ||
                        ($varConyugueDir != '' && $varConyugueDir != ' ') ||
                        ($varConyugueTel != '' && $varConyugueTel != ' ' && $varConyugueTel != '0') ||
                        ($varHijo1 != '' && $varHijo1 != ' ') ||
                        ($varHijo1Sex != '' && $varHijo1Sex != ' ') ||
                        ($varHijo1Edad != '' && $varHijo1Edad != ' ' && $varHijo1Edad != '0') ||
                        ($varHijo2 != '' && $varHijo2 != ' ') ||
                        ($varHijo2Sex != '' && $varHijo2Sex != ' ') ||
                        ($varHijo2Edad != '' && $varHijo2Edad != ' ' && $varHijo2Edad != '0') ||
                        ($varPadre != '' && $varPadre != ' ') ||
                        ($varPadreEdad != '' && $varPadreEdad != ' ' && $varPadreEdad != '0') ||
                        ($varMadre != '' && $varMadre != ' ') ||
                        ($varMadreEdad != '' && $varMadreEdad != ' ' && $varMadreEdad != '0')
                        ) { 
                        ?>
                        <div class="separador-clinica"></div>
                        <div class="row">
                            <div class="basic-hist">
                                <div class="col-sm-12">
                                    <div class="sub-label">
                                        Información Familiar
                                    </div>
                                    <div class="subtitle-separador"></div>
                                </div>
                                <?php 
                                if ($varConyugueNom != '' && $varConyugueNom != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Nombre del conyugue</div>"."<div class='the-info'>".$varConyugueNom."</div>"."</div>";
                                }
                                if ($varConyugueDoc != '' && $varConyugueDoc != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Documento de identidad del conyugue</div>"."<div class='the-info'>".$varConyugueDoc."</div>"."</div>";
                                }
                                if ($varConyugueNacim != '' && $varConyugueNacim != ' ' && $varConyugueNacim != '0000-00-00') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Fecha de nacimiento del conyugue</div>"."<div class='the-info'>".$varConyugueNacim."</div>"."</div>";
                                }
                                if ($varConyugueDir != '' && $varConyugueDir != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Dirección del conyugue</div>"."<div class='the-info'>".$varConyugueDir."</div>"."</div>";
                                }
                                if ($varConyugueTel != '' && $varConyugueTel != ' ' && $varConyugueTel != '0') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Teléfono del conyugue</div>"."<div class='the-info'>".$varConyugueTel."</div>"."</div>";
                                }
                                if ($varHijo1 != '' && $varHijo1 != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Nombre del primer hijo</div>"."<div class='the-info'>".$varHijo1."</div>"."</div>";
                                }
                                if ($varHijo1Sex != '' && $varHijo1Sex != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Sexo del primer hijo</div>"."<div class='the-info'>".$varHijo1Sex."</div>"."</div>";
                                }
                                if ($varHijo1Edad != '' && $varHijo1Edad != ' ' && $varHijo1Edad != '0') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Edad del primer hijo</div>"."<div class='the-info'>".$varHijo1Edad."</div>"."</div>";
                                }
                                if ($varHijo2 != '' && $varHijo2 != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Nombre del segundo hijo</div>"."<div class='the-info'>".$varHijo2."</div>"."</div>";
                                }
                                if ($varHijo2Sex != '' && $varHijo2Sex != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Sexo del segundo hijo</div>"."<div class='the-info'>".$varHijo2Sex."</div>"."</div>";
                                }
                                if ($varHijo2Edad != '' && $varHijo2Edad != ' ' && $varHijo2Edad != '0') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Edad del segundo hijo</div>"."<div class='the-info'>".$varHijo2Edad."</div>"."</div>";
                                }
                                if ($varPadre != '' && $varPadre != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Nombre del Padre</div>"."<div class='the-info'>".$varPadre."</div>"."</div>";
                                }
                                if ($varPadreEdad != '' && $varPadreEdad != ' ' && $varPadreEdad != '0') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Edad del Padre</div>"."<div class='the-info'>".$varPadreEdad."</div>"."</div>";
                                }
                                if ($varMadre != '' && $varMadre != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Nombre de la Madre</div>"."<div class='the-info'>".$varMadre."</div>"."</div>";
                                }
                                if ($varMadreEdad != '' && $varMadreEdad != ' ' && $varMadreEdad != '0') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Edad de la Madre</div>"."<div class='the-info'>".$varMadreEdad."</div>"."</div>";
                                }
                                ?>
                            </div><!-- end basic-hist -->
                        </div><!-- end row -->

                        <?php } ?>


                        <?php if (  ($varHobbie != '' && $varHobbie != ' ') || 
                            ($varEtnia != '' && $varEtnia != ' ') || 
                            ($varDominancia != '' && $varDominancia != ' ') || 
                            ($varRegion != '' && $varRegion != ' ') || 
                            ($varDeporte != '' && $varDeporte != ' ') || 
                            ($varColegio != '' && $varColegio != ' ') || 
                            ($varEscolaridad != '' && $varEscolaridad != ' ') || 
                            ($varRendimiento != '' && $varRendimiento != ' ')
                            ) { 
                            ?>

                            <div class="separador-clinica"></div>

                            <div class="row">
                                <div class="basic-hist">
                                    <div class="col-sm-12">
                                        <div class="sub-label">
                                            Otra información
                                        </div>
                                        <div class="subtitle-separador"></div>
                                    </div>
                                    <?php 
                                    if ($varHobbie != '' && $varHobbie != ' ') {
                                        echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Hobbie</div>"."<div class='the-info'>".$varHobbie."</div>"."</div>";
                                    }
                                    if ($varEtnia != '' && $varEtnia != ' ') {
                                        $varEtnia = $db->getOne("SELECT nombre FROM etnia WHERE id=?s",$varEtnia);
                                        echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Etnia</div>"."<div class='the-info'>".$varEtnia."</div>"."</div>";
                                    }
                                    if ($varDominancia != '' && $varDominancia != ' ') {
                                        $varDominancia = $db->getOne("SELECT nombre FROM dominancia WHERE id=?s",$varDominancia);
                                        echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Dominancia</div>"."<div class='the-info'>".$varDominancia."</div>"."</div>";
                                    }
                                    if ($varRegion != '' && $varRegion != ' ') {
                                        $varRegion = $db->getOne("SELECT nombre FROM religion WHERE id=?s",$varRegion);
                                        echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Religión</div>"."<div class='the-info'>".$varRegion."</div>"."</div>";
                                    }
                                    if ($varDeporte != '' && $varDeporte != ' ') {
                                        $varDeporte = $db->getOne("SELECT nombre FROM deportes WHERE id=?s",$varDeporte);
                                        echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Deporte</div>"."<div class='the-info'>".$varDeporte."</div>"."</div>";
                                    }
                                    if ($varColegio != '' && $varColegio != ' ') {
                                        echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Colegio</div>"."<div class='the-info'>".$varColegio."</div>"."</div>";
                                    }
                                    if ($varEscolaridad != '' && $varEscolaridad != ' ') {
                                        echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Escolaridad</div>"."<div class='the-info'>".$varEscolaridad."</div>"."</div>";
                                    }
                                    if ($varRendimiento != '' && $varRendimiento != ' ') {
                                        echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Rendimiento</div>"."<div class='the-info'>".$varRendimiento."</div>"."</div>";
                                    }
                                    ?>
                                </div><!-- end basic-hist -->
                            </div><!-- end row -->
                            <?php } ?>

                            <div class="separador-clinica"></div>

                            <div class="row">
                                <div class="basic-hist">
                            <?php 
                            if ($varNumAutorizacion != '' && $varNumAutorizacion != ' ') 
                                { 
                            ?>
                                    <div class="col-sm-12">
                                        <div class="sub-label">
                                            Información de la orden
                                        </div>
                                        <div class="subtitle-separador"></div>
                                    </div>
                            <?php                             
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Número de autorización</div>"."<div class='the-info'>".$varNumAutorizacion."</div>"."</div>";
                            
                                if ($varEntidad != '' && $varEntidad != ' ') {
                                    $varEntidad = $db->getOne("SELECT nombre FROM entidad WHERE id=?s",$varEntidad);
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Entidad</div>"."<div class='the-info'>".$varEntidad."</div>"."</div>";
                                }
                                if ($varMedico != '' && $varMedico != ' ') {
                                    $varNombreMedico = $db->getOne("SELECT nombre FROM nombre_medico WHERE id=?s",$varMedico);
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Medico</div>"."<div class='the-info'>".$varNombreMedico."</div>"."</div>";
                                }
                                if ($varTipoConsulta != '' && $varTipoConsulta != ' ') {
                                    $varTipoConsulta = $db->getOne("SELECT nombre FROM tipo_consulta WHERE id=?s",$varTipoConsulta);
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Tipo de consulta</div>"."<div class='the-info'>".$varTipoConsulta."</div>"."</div>";
                                }
                                if ($varRemitido != '' && $varRemitido != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Remitido por</div>"."<div class='the-info'>".$varRemitido."</div>"."</div>";
                                }
                                if ($varEspecialidad != '' && $varEspecialidad != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Especialidad</div>"."<div class='the-info'>".$varEspecialidad."</div>"."</div>";
                                }
                                if ($varProcedimiento != '' && $varProcedimiento != ' ') {
                                /*
                                #En el caso que se aplique solo el id de la fila entonces se puede usar este codigo. 

                                $idProcedimientoTmp = explode(" - ", $varProcedimiento);
                                $idProcedimiento = $idProcedimientoTmp[0];
                                $IdProcedimiento = mysqli_query($con,"SELECT * FROM cup_table WHERE id='".$idProcedimiento."'")->fetch_object()->id;
                                $NombreProcedimiento = mysqli_query($con,"SELECT * FROM cup_table WHERE id='".$idProcedimiento."'")->fetch_object()->nombre;
                                $varProcedimiento = $IdProcedimiento." - ".$NombreProcedimiento;
                                */
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Procedimiento</div>"."<div class='the-info'>".$varProcedimiento."</div>"."</div>";
                                }
                                if ($varSesiones != '' && $varSesiones != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Sesiones</div>"."<div class='the-info'>".$varSesiones."</div>"."</div>";
                                }
                                if ($varPersonalAtiende  != '' && $varPersonalAtiende  != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Personal que lo atiende</div>"."<div class='the-info'>".$varPersonalAtiende ."</div>"."</div>";
                                }
                                if ($varDxPrev != '' && $varDxPrev != ' ') {
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Diagnóstico previo</div>"."<div class='the-info'>".$varDxPrev."</div>"."</div>";
                                }
                                if ($varDxAdicional != '' && $varDxAdicional != ' ') {
                                /*
                                #En el caso que se aplique solo el id de la fila entonces se puede usar este codigo. 
                                
                                $idDxAdicionalTmp = explode(" - ", $varDxAdicional);
                                $idDxAdicional = $idDxAdicionalTmp[0];
                                $IdDxAdicional = mysqli_query($con,"SELECT * FROM diagnosticos WHERE id='".$idDxAdicional."'")->fetch_object()->id;
                                $NombreProcedimiento = mysqli_query($con,"SELECT * FROM diagnosticos WHERE id='".$idDxAdicional."'")->fetch_object()->nombre;
                                $varDxAdicional = $IdDxAdicional." - ".$NombreProcedimiento;
                                */
                                echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Diagnóstico adicional</div>"."<div class='the-info'>".$varDxAdicional."</div>"."</div>";
                                }
                                if ($varDxNuevo != '' && $varDxNuevo != ' ') {
                                    $varDxNuevo = $db->getOne("SELECT nombre FROM diagnosticos WHERE id=?s",$varDxNuevo);
                                    echo "<div class='col-sm-4'>"."<div class='info-hist-label'>Diagnóstico Nuevo</div>"."<div class='the-info'>".$varDxNuevo."</div>"."</div>";
                                }
                            }
                            ?>
                        </div><!-- end basic-hist -->
                    </div><!-- end row -->
                    <?php 
                    if ($varNumAutorizacion != '' && $varNumAutorizacion != ' ') 
                        {
                            echo '<div class="separador-clinica"></div>';
                        }
                    ?>
                </div>
    </section><!-- end basic-history-->
    
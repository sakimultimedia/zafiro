<?php include "header.php" ?>

<?php

if(isset($_POST["continuar-id"])){

	if (!empty($_POST["documento"])) {
		# code...
		$varDocumento = $_POST["documento"];

		$query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento,$varSessionBoss);
		$numrows = $db->numRows($query);

		if($numrows==1)
		{	
			$_SESSION['documento'] = $varDocumento;
			
			#guardar datos del paciente para mostrarse en otras paginas
			$_SESSION['tipoDocumento'] = $db->getOne("SELECT tipo_de_documento FROM pacientes WHERE documento=?s AND uploaded_by=?s", $varDocumento, $varSessionBoss);
			echo "<script> location.href='historia.php'; </script>";
			#header("Location: historia.php");	
		} else {
			$_SESSION['documento'] = $varDocumento;
			echo "<script> location.href='nueva-historia.php'; </script>";
			#header("Location: nueva-historia.php");
		}
	}else{
		#$message = "Todos los campos son requeridos!";
		echo "<script type='text/javascript'>alert('Todos los campos son requeridos!');</script>";
	}
}
?>


<!--===========================================
===============================================
Seccion 1 -->
<section id="documento-id">
	<div class="container">

		<div class="col-sm-12">
			<div class="section-label">
				Documento de Identidad
			</div>
			<div class="title-separador"></div>
		</div><!-- end col -->

		<!--<div class="row">
			<div class="col-sm-12" style="text-align:center;">
				<?php #if (!empty($message)) {echo "<p class=\"error\">" . "ERROR: ". $message . "</p>";} ?>
			</div>
		</div> end col -->

		<div class="row">
			<div class="col-sm-12">
				<div class="explicacion">
					Por favor, introduce el número del documento para verificar si su historia médica ya existe. Si no es así, se abrirá automáticamente una ventana para crear una historia nueva.
				</div>
			</div>
		</div>

		<form class="ajaxform" action= "<?echo $ZafiroRute?>documento-id.php" method="POST" id="basic-data" mensajeExito = "Los datos básicos han sido correctamente guardados." mensajeError = "Ha habido un error y tus datos no fueron guardados. intentalo más tarde.">

			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					<div class="top-label">
						Documento de identidad
					</div>
					<input type="number" list="docIdentidad" name="documento" id="documento"  class="awesomplete" data-minchars="0" placeholder="Escribe el número de documento">
					<datalist id="docIdentidad">
						<?php 
						$query = $db->query("SELECT * FROM pacientes WHERE uploaded_by=?s",$varSessionBoss);
						$numrows = $db->numRows($query);	
						//$query=mysqli_query($con,"SELECT * FROM pacientes WHERE uploaded_by='$varSessionBoss'");
						//$numrows=mysqli_num_rows($query);
						for ($i=1; $i <= $numrows ; $i++) { 
							$row = mysqli_fetch_array($query);
							$search = $db->getOne("SELECT documento FROM pacientes WHERE documento=?s",$row['documento']);
							//$search = mysqli_query($con,"SELECT * FROM pacientes WHERE documento='".$row['documento']."'");
							echo "<option value='".$search."'>";
						}
						?>
					</datalist>
				</div><!-- end col -->
				<div class="col-sm-4"></div>
			</div><!-- end row -->

			<div class="row">
				<div class="col-sm-12" style="text-align:center;">
					<input type="submit" value="Continuar" class="send-btn" name="continuar-id">
				</div>
			</div>

			
		</form><!-- end form -->
	</div>

</section>

 <!--===========================================
===============================================
Footer -->

<?php include "footer.php" ?>

<?php
ob_start();
session_start();

require 'includes/tfpdf.php';
require_once "includes/connection.php";
require "utils/scripts.php";
//define('FPDF_FONTPATH','includes/font');
if(0){
    error_reporting(E_ALL);
}else{
    error_reporting(0);
}   


if (isset($_SESSION["documento"])) {
    $varDocumento = $_SESSION["documento"];
}
$varSessionUsername = $_SESSION["session_username"];
$varSessionBoss = $db->getOne("SELECT boss FROM usuarios WHERE username=?s",$_SESSION['session_username']);


class PDF extends TFPDF
{
// Cabecera de página
    function Header(){ 
        // Add a Unicode font (uses UTF-8)
            //$this->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
            //$this->SetFont('OpenSans','',20);
            //$this->SetTextColor(255, 88, 49);
        // Logo
            #$this->Image('logo_pb.png',10,8,33);
        // Arial bold 15
            #$this->SetFont('Arial','B',15);
        // Movernos a la derecha
            //$this->Cell(80);
        // Título
            //$this->Cell(30,10,'Historia clínica',0,0,'C');
        // Salto de línea
            //$this->Ln(10);
            //$this->Cell(80);

            //$this->SetFillColor(255, 88, 49);
            //$this->Cell(30,0.5,"",0,0,'C',true);

            //$this->Ln(20);
    }


// Pie de página
    function Footer()
    {   
        $this->SetTextColor(85, 85, 85);
// Posición: a 1,5 cm del final
        $this->SetY(-15);
// Arial italic 8
        $this->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
        $this->SetFont('OpenSans','',8);
#$this->SetFont('Arial','I',8);
// Número de página
        $this->Cell(0,10,'Página '.$this->PageNo().'/{nb}',0,0,'C');
        $this->SetTextColor(85, 85, 85);
    }
}

if(isset($_SESSION["session_username"])){

    if(isset($varDocumento) && $varDocumento != ""){
// Creación del objeto de la clase heredada

$query_pacientes = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento,$varSessionBoss);
$row_pacientes = $db->fetch($query_pacientes);

//Consultas a la BD
$SQLID = $row_pacientes["tipo_de_documento"];
$SQLNom1 = $row_pacientes["primer_nombre"];
$SQLNom2 = $row_pacientes["segundo_nombre"];
$SQLApe1 = $row_pacientes["primer_apellido"];
$SQLApe2 = $row_pacientes["segundo_apellido"];
$SQLBorn = $row_pacientes["fecha_nacimiento"];
list($ano,$mes,$dia) = explode("-",$SQLBorn);
$SQLBorn = "$dia/$mes/$ano";
$SQLEdad = $row_pacientes["edad"];
$SQLGenero = $row_pacientes["sexo"];
$SQLEstCivil = $row_pacientes["estado_civil"];

$SQLDireccion_1 = $row_pacientes["direccion_1"];
$SQLDireccion_2 = $row_pacientes["direccion_2"];
$SQLTelefono = $row_pacientes["telefono"];
$SQLCelular = $row_pacientes["celular"];
$SQLCiudad = $row_pacientes["ciudad"];
$SQLMedico = $row_pacientes["nombre_medico"];
$SQLEntidad = $row_pacientes["entidad"];
$SQLRegimen = $row_pacientes["regimen"];

$SQLRemitido = $row_pacientes["remitido_por"];
$SQLAcompana = $row_pacientes["acompanante"];
$SQLAcopanaTel = $row_pacientes["telefono_acomp"];
$SQLAcopanaParentezco = $row_pacientes[""];
$SQLMail = $row_pacientes["email"];
$SQLOrigen = $row_pacientes["origen"];
$SQLNacionalidad = $row_pacientes["nacionalidad"];
$SQLOcupacion = $row_pacientes["ocupacion"];
$SQLEmpresa = $row_pacientes["empresa"];

$SQLConyugueNom = $row_pacientes["conyugue"];
$SQLConyugueDoc = $row_pacientes["documento_conyug"];
$SQLConyugueNacim = $row_pacientes["fecha_nac"];
$SQLConyugueDir = $row_pacientes["direccion_conyug"];
$SQLConyugueTel = $row_pacientes["telefono_conyugu"];
$SQLHijo1 = $row_pacientes["nombre_hijo1"];
$SQLHijo1Sex = $row_pacientes["sexo_hijo1"];
$SQLHijo1Edad = $row_pacientes["edad_hijo1"];
$SQLHijo2 = $row_pacientes["nombre_hijo2"];
$SQLHijo2Sex = $row_pacientes["sexo_hijo2"];
$SQLHijo2Edad = $row_pacientes["edad_hijo2"];
$SQLPadre = $row_pacientes["padre"];
$SQLPadreEdad = $row_pacientes["edad_padre"];
$SQLMadre = $row_pacientes["madre"];
$SQLMadreEdad = $row_pacientes["edad_madre"];

$SQLHobbie = $row_pacientes["hobbie"];
$SQLEtnia = $row_pacientes["etnia"];
$SQLDominancia = $row_pacientes["dominancia"];
$SQLRegion = $row_pacientes["region"];
$SQLDeporte = $row_pacientes["deporte"];
$SQLColegio = $row_pacientes["colegio"];
$SQLEscolaridad = $row_pacientes["escolaridad"];
$SQLRendimiento = $row_pacientes["rendimiento"];

$query_informacion_orden = $db->query("SELECT * FROM informacion_orden WHERE id_paciente=?s",$varDocumento);
$row_informacion_orden = $db->fetch($query_informacion_orden);

$SQLNumAutorizacion= $row_informacion_orden["numero_autorizacion"];
$SQLTipoConsulta= $row_informacion_orden["tipo_consulta"];
$SQLEspecialidad= $row_informacion_orden["especialidad"];
$SQLProcedimiento= $row_informacion_orden["procedimiento"];
$SQLSesiones= $row_informacion_orden["num_sesiones"];
$SQLPersonalAtiende= $row_informacion_orden["personal_atiende"];
$SQLDxPrev= $row_informacion_orden["dx_previo"];
$SQLDxAdicional= $row_informacion_orden["dx_adicional"];
$SQLDxNuevo = $row_informacion_orden["dx_posterior FROM "];


########################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

//Se eliminan los campos vacios
$SQLID = isset_empty_and_set($SQLID);
$SQLNom1 = isset_empty_and_set($SQLNom1);
$SQLNom2 = isset_empty_and_set($SQLNom2);
$SQLApe1 = isset_empty_and_set($SQLApe1);
$SQLApe2 = isset_empty_and_set($SQLApe2);
$SQLBorn = isset_empty_and_set($SQLBorn);
$SQLBorn = isset_empty_and_set($SQLBorn);
$SQLEdad = isset_empty_and_set($SQLEdad);
$SQLGenero = isset_empty_and_set($SQLGenero);
$SQLEstCivil = isset_empty_and_set($SQLEstCivil);

$SQLDireccion_1 = isset_empty_and_set($SQLDireccion_1);
$SQLDireccion_2 = isset_empty_and_set($SQLDireccion_2);
$SQLTelefono = isset_empty_and_set($SQLTelefono);
$SQLCelular = isset_empty_and_set($SQLCelular);
$SQLCiudad = isset_empty_and_set($SQLCiudad);
$SQLMedico = isset_empty_and_set($SQLMedico);
$SQLEntidad = isset_empty_and_set($SQLEntidad);
$SQLRegimen = isset_empty_and_set($SQLRegimen);
$SQLRemitido = isset_empty_and_set($SQLRemitido);

$SQLAcompana = isset_empty_and_set($SQLAcompana);
$SQLAcopanaTel = isset_empty_and_set($SQLAcopanaTel);
$SQLAcopanaParentezco = isset_empty_and_set($SQLAcopanaParentezco);
$SQLMail = isset_empty_and_set($SQLMail);
$SQLOrigen = isset_empty_and_set($SQLOrigen);
$SQLNacionalidad = isset_empty_and_set($SQLNacionalidad);
$SQLOcupacion = isset_empty_and_set($SQLOcupacion);
$SQLEmpresa = isset_empty_and_set($SQLEmpresa);

$SQLConyugueNom = isset_empty_and_set($SQLConyugueNom);
$SQLConyugueDoc = isset_empty_and_set($SQLConyugueDoc);
if (isset_empty_and_set($SQLConyugueNacim) == "0000-00-00") {
    $SQLConyugueNacim = "";
}else{
    $SQLConyugueNacim = isset_empty_and_set($SQLConyugueNacim) == "0000-00-00";
}
$SQLConyugueDir = isset_empty_and_set($SQLConyugueDir);
$SQLConyugueTel = isset_empty_and_set($SQLConyugueTel);
$SQLHijo1 = isset_empty_and_set($SQLHijo1);
$SQLHijo1Sex = isset_empty_and_set($SQLHijo1Sex);
$SQLHijo1Edad = isset_empty_and_set($SQLHijo1Edad);
$SQLHijo2 = isset_empty_and_set($SQLHijo2);
$SQLHijo2Sex = isset_empty_and_set($SQLHijo2Sex);
$SQLHijo2Edad = isset_empty_and_set($SQLHijo2Edad);
$SQLPadre = isset_empty_and_set($SQLPadre);
$SQLPadreEdad = isset_empty_and_set($SQLPadreEdad);
$SQLMadre = isset_empty_and_set($SQLMadre);
$SQLMadreEdad = isset_empty_and_set($SQLMadreEdad);

$SQLHobbie = isset_empty_and_set($SQLHobbie);
$SQLEtnia = isset_empty_and_set($SQLEtnia);
$SQLDominancia = isset_empty_and_set($SQLDominancia);
$SQLRegion = isset_empty_and_set($SQLRegion);
$SQLDeporte = isset_empty_and_set($SQLDeporte);
$SQLColegio = isset_empty_and_set($SQLColegio);
$SQLEscolaridad = isset_empty_and_set($SQLEscolaridad);
$SQLRendimiento = isset_empty_and_set($SQLRendimiento);
$SQLNumAutorizacion = isset_empty_and_set($SQLNumAutorizacion);
$SQLTipoConsulta = isset_empty_and_set($SQLTipoConsulta);
$SQLEspecialidad = isset_empty_and_set($SQLEspecialidad);
$SQLProcedimiento = isset_empty_and_set($SQLProcedimiento);
$SQLSesiones = isset_empty_and_set($SQLSesiones);
$SQLPersonalAtiende = isset_empty_and_set($SQLPersonalAtiende);
$SQLDxPrev = isset_empty_and_set($SQLDxPrev);
$SQLDxAdicional = isset_empty_and_set($SQLDxAdicional);
$SQLDxNuevo = isset_empty_and_set($SQLDxNuevo);

$pdf = new PDF();  
$pdf->AliasNbPages();  
$pdf->AddPage();

// Header, lo ponemos aca para que solo salga en una página y no en todas
$pdf->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
$pdf->SetFont('OpenSans','',20);
$pdf->SetTextColor(255, 88, 49);
$pdf->Cell(80);
$pdf->Cell(30,10,'Historia clínica',0,0,'C');
$pdf->Ln(10);
$pdf->Cell(80);
$pdf->SetFillColor(255, 88, 49);
$pdf->Cell(30,0.5,"",0,0,'C',true);

// Empieza el pdf

//Separador
    $pdf->Ln(10);
    $pdf->SetFillColor(236, 236, 236);
    $pdf->Cell(0,0.1,"",0,0,'C',true);
    $pdf->Ln(12);
//Separador

/*DATOS BASICOS*/
$pdf->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
$pdf->SetFont('OpenSans','',12);
$pdf->SetLineWidth(0.1);
$pdf->SetFontSize(15);
$pdf->SetTextColor(0, 0, 0);
$pdf->SetTextColor(255, 88, 49);

$pdf->Cell(80);
// Título
$pdf->Cell(30,10,'Datos basicos',0,0,'C');
// Salto de línea
$pdf->Ln(10);
$pdf->Cell(80);

$pdf->SetFillColor(255, 88, 49);
$pdf->Cell(30,0.5,"",0,0,'C',true);

$pdf->Ln(10);

$pdf->SetTextColor(0, 0, 0);
$pdf->SetFontSize(12);

$pdf->SetTextColor(255, 88, 49); 
$pdf->Write(10,'Documento: '); 
$pdf->SetTextColor(85, 85, 85);
$pdf->Write(10,$varDocumento); 
$pdf->Ln(8); 

$varID = $db->getOne("SELECT nombre FROM tipo_documento WHERE id=?s",$SQLID);
if ($varID != "") { 
    $pdf->SetTextColor(255, 88, 49); 
    $pdf->Write(10,'Tipo de documento: '); 
    $pdf->SetTextColor(85, 85, 85);
    $pdf->Write(10,$varID); 
    $pdf->Ln(8); }
if ($SQLNom1 != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Nombre: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLNom1);$pdf->Write(10,' ');}
if ($SQLNom2 != "") { $pdf->Write(10,$SQLNom2); $pdf->Write(10,' '); }
if ($SQLApe1 != "") { $pdf->Write(10,$SQLApe1); $pdf->Write(10,' ');}
if ($SQLApe2 != "") { $pdf->Write(10,$SQLApe2); $pdf->Ln(8);}
if ($SQLBorn != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Fecha de nacimiento: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLBorn);$pdf->Write(10,' '); $pdf->Ln(8);}
if ($SQLEdad != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Edad: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLEdad);$pdf->Write(10,' '); $pdf->Ln(8);}
if ($SQLGenero != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Edad: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLGenero);$pdf->Write(10,' '); $pdf->Ln(8);}
$varEstCivil = $db->getOne("SELECT nombre FROM estado_civil WHERE id=?s",$SQLEstCivil);
if ($varEstCivil != "") { 
    $pdf->SetTextColor(255, 88, 49); 
    $pdf->Write(10,'Estado civil: '); 
    $pdf->SetTextColor(85, 85, 85);
    $pdf->Write(10,$varEstCivil); 
    $pdf->Ln(8); }

//Separador
    $pdf->Ln(10);
    $pdf->SetFillColor(236, 236, 236);
    $pdf->Cell(0,0.1,"",0,0,'C',true);
    $pdf->Ln(10);
//Separador

$pdf->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
        $pdf->SetFont('OpenSans','',12);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFontSize(15);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetTextColor(255, 88, 49);
/*INFO PERSONAL*/

        $pdf->Cell(80);
// Título
        $pdf->Cell(30,10,'Información Personal',0,0,'C');
// Salto de línea
        $pdf->Ln(10);
        $pdf->Cell(80);

        $pdf->SetFillColor(255, 88, 49);
        $pdf->Cell(30,0.5,"",0,0,'C',true);

        $pdf->Ln(10);

        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFontSize(12);



if ($SQLDireccion_1 != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Dirección: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLDireccion_1);$pdf->Write(10,' '); $pdf->Ln(8);}
if ($SQLDireccion_2 != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Dirección alternativa: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLDireccion_2);$pdf->Write(10,' '); $pdf->Ln(8);}
if ($SQLTelefono != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Teléfono: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLTelefono);$pdf->Write(10,' '); $pdf->Ln(8);}
if ($SQLCelular != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Celular: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLCelular);$pdf->Write(10,' '); $pdf->Ln(8);}
$varCiudad =  $db->getOne("SELECT nombre FROM ciudad WHERE id=?s",$SQLCiudad);
if ($varCiudad != "") { 
    $pdf->SetTextColor(255, 88, 49); 
    $pdf->Write(10,'Ciudad: '); 
    $pdf->SetTextColor(85, 85, 85);
    $pdf->Write(10,$varCiudad); 
    $pdf->Ln(8); }
$varMedico = $db->getOne("SELECT nombre FROM nombre_medico WHERE id=?s",$SQLMedico);
if ($varMedico != "") { 
    $pdf->SetTextColor(255, 88, 49); 
    $pdf->Write(10,'Médico tratante: '); 
    $pdf->SetTextColor(85, 85, 85);
    $pdf->Write(10,$varMedico); 
    $pdf->Ln(8); }
$varEntidad = $db->getOne("SELECT nombre FROM entidad WHERE id=?s",$SQLEntidad);
if ($varEntidad != "") { 
    $pdf->SetTextColor(255, 88, 49); 
    $pdf->Write(10,'Entidad: '); 
    $pdf->SetTextColor(85, 85, 85);
    $pdf->Write(10,$varEntidad); 
    $pdf->Ln(8); }
$varRegimen = $db->getOne("SELECT nombre FROM regimen WHERE id=?s",$SQLRegimen);
if ($varRegimen != "") { 
    $pdf->SetTextColor(255, 88, 49); 
    $pdf->Write(10,'Régimen: '); 
    $pdf->SetTextColor(85, 85, 85);
    $pdf->Write(10,$varRegimen); 
    $pdf->Ln(8); }

//Separador
    $pdf->Ln(10);
    $pdf->SetFillColor(236, 236, 236);
    $pdf->Cell(0,0.1,"",0,0,'C',true);
    $pdf->Ln(10);
//Separador

 $pdf->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
        $pdf->SetFont('OpenSans','',12);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFontSize(15);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetTextColor(255, 88, 49);
/*INFO PERSONAL*/
if (($SQLRemitido != '' )||
    ($SQLAcompana != '' )||
    ($SQLAcopanaTel != '' )||
    ($SQLAcopanaParentezco != '' )||
    ($SQLMail != '' )||
    ($SQLOrigen != '' )||
    ($SQLNacionalidad != '' )||
    ($SQLOcupacion != '' )||
    ($SQLEmpresa != '')) {
        // Título
        $pdf->Cell(80);
        $pdf->Cell(30,10,'Ocupación',0,0,'C');

        $pdf->Ln(10);
        $pdf->Cell(80);

        $pdf->SetFillColor(255, 88, 49);
        $pdf->Cell(30,0.5,"",0,0,'C',true);

        $pdf->Ln(10);

        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFontSize(12);

// Salto de línea
            

    if ($SQLRemitido != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Remitido por: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLRemitido);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLAcompana != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Acompañante: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLAcompana);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLAcopanaTel != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Teléfono del acompañante: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLAcopanaTel);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLAcopanaParentezco != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Parentezco del acompañante: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLAcopanaParentezco);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLMail != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'E-mail del paciente: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLMail);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLOrigen != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Lugar de nacimiento: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLOrigen);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLNacionalidad != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Nacionalidad: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLNacionalidad);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLOcupacion != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Ocupación: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLOcupacion);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLEmpresa != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Empresa: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLEmpresa);$pdf->Write(10,' '); $pdf->Ln(8);}

//Separador
    $pdf->Ln(10);
    $pdf->SetFillColor(236, 236, 236);
    $pdf->Cell(0,0.1,"",0,0,'C',true);
    $pdf->Ln(10);
//Separador
}

$pdf->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
        $pdf->SetFont('OpenSans','',12);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFontSize(15);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetTextColor(255, 88, 49);
/*INFO PERSONAL*/
if (($SQLConyugueNom != '') ||
    ($SQLConyugueDoc != '') ||
    ($SQLConyugueNacim != '') ||
    ($SQLConyugueDir != '') ||
    ($SQLConyugueTel != '') ||
    ($SQLHijo1 != '') ||
    ($SQLHijo1Sex != '') ||
    ($SQLHijo1Edad != '') ||
    ($SQLHijo2 != '') ||
    ($SQLHijo2Sex != '') ||
    ($SQLHijo2Edad != '') ||
    ($SQLPadre != '') ||
    ($SQLPadreEdad != '') ||
    ($SQLMadre != '') ||
    ($SQLMadreEdad != '')) {

    $pdf->Cell(80);
// Título
    $pdf->Cell(30,10,'Información Familiar',0,0,'C');
// Salto de línea
    $pdf->Ln(10);
    $pdf->Cell(80);

    $pdf->SetFillColor(255, 88, 49);
    $pdf->Cell(30,0.5,"",0,0,'C',true);

    $pdf->Ln(10);

    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFontSize(12);


    if ($SQLConyugueNom != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Nombre del conyugue: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLConyugueNom);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLConyugueDoc != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Documento del conyugue: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLConyugueDoc);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLConyugueNacim != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Fecha de nacimiento del conyugue: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLConyugueNacim);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLConyugueDir != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Dirección del conyugue: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLConyugueDir);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLConyugueTel != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Teléfono del conyugue: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLConyugueTel);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLHijo1 != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Nombre del primer hijo: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLHijo1);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLHijo1Sex != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Género del primer hijo: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLHijo1Sex);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLHijo1Edad != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Edad del primer hijo: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLHijo1Edad);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLHijo2 != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Nombre del segundo hijo: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLHijo2);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLHijo2Sex != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Género del segundo hijo: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLHijo2Sex);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLHijo2Edad != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Edad del segundo hijo: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLHijo2Edad);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLPadre != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Nombre del padre: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLPadre);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLPadreEdad != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Edad del padre: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLPadreEdad);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLMadre != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Nombre de la madre: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLMadre);$pdf->Write(10,' '); $pdf->Ln(8);}
    if ($SQLMadreEdad != "") { $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Edad de la madre: '); $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLMadreEdad);$pdf->Write(10,' '); $pdf->Ln(8);}


//Separador
    $pdf->Ln(10);
    $pdf->SetFillColor(236, 236, 236);
    $pdf->Cell(0,0.1,"",0,0,'C',true);
    $pdf->Ln(10);
//Separador
}

$pdf->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
        $pdf->SetFont('OpenSans','',12);
        $pdf->SetLineWidth(0.1);
        $pdf->SetFontSize(15);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetTextColor(255, 88, 49);
/*OTRA INFO*/
$varEtnia =  $db->getOne("SELECT nombre FROM etnia WHERE id=?s",$SQLEtnia);
$varDominancia = $db->getOne("SELECT nombre FROM dominancia WHERE id=?s",$SQLDominancia);
$varRegion = $db->getOne("SELECT nombre FROM religion WHERE id=?s",$SQLRegion);
$varDeporte = $db->getOne("SELECT nombre FROM deportes WHERE id=?s",$SQLDeporte);
if (($SQLHobbie != '') ||
    ($varEtnia != '') ||
    ($varDominancia != '') ||
    ($varRegion != '') ||
    ($varDeporte != '') ||
    ($SQLColegio != '') ||
    ($SQLEscolaridad != '') ||
    ($SQLRendimiento != '')) {
        $pdf->Cell(80);
// Título
        $pdf->Cell(30,10,'Otra información',0,0,'C');
// Salto de línea
        $pdf->Ln(10);
        $pdf->Cell(80);

        $pdf->SetFillColor(255, 88, 49);
        $pdf->Cell(30,0.5,"",0,0,'C',true);

        $pdf->Ln(10);

        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFontSize(12);


    if ($SQLHobbie != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Hobbie: ');
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLHobbie);
        $pdf->Write(10,' '); $pdf->Ln(8);
    }
    
    if ($varEtnia != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Etnia: '); 
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$varEtnia); 
        $pdf->Ln(8); }
    
    if ($varDominancia != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Dominancia: '); 
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$varDominancia); 
        $pdf->Ln(8); }
    
    if ($varRegion != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Religión: '); 
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$varRegion); 
        $pdf->Ln(8); }
    
    if ($varDeporte != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Régimen: '); 
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$varDeporte); 
        $pdf->Ln(8); }
    if ($SQLColegio != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Colegio: '); 
        $pdf->SetTextColor(85, 85, 85); 
        $pdf->Write(10,$SQLColegio);
        $pdf->Write(10,' '); $pdf->Ln(8);
    }
    if ($SQLEscolaridad != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Nivel de escolaridad: '); $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLEscolaridad);$pdf->Write(10,' '); $pdf->Ln(8);
    }
    if ($SQLRendimiento != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Rendimiento: '); $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLRendimiento);$pdf->Write(10,' '); $pdf->Ln(8);
    }

    //Separador
        $pdf->Ln(10);
        $pdf->SetFillColor(236, 236, 236);
        $pdf->Cell(0,0.1,"",0,0,'C',true);
        $pdf->Ln(10);
    //Separador
}

$varTipoConsulta = $db->getOne("SELECT nombre FROM tipo_consulta WHERE id=?s",$SQLTipoConsulta);
$varProcedimiento = $db->getOne("SELECT nombre FROM cup_table WHERE id=?s",$SQLProcedimiento);
if (($SQLNumAutorizacion != '') ||
    ($varTipoConsulta != '') ||
    ($SQLEspecialidad != '') ||
    ($varProcedimiento != '') ||
    ($SQLSesiones != '') ||
    ($SQLPersonalAtiende != '') ||
    ($SQLDxPrev != '') ||
    ($SQLDxAdicional != '') ||
    ($SQLDxNuevo != '')) {

    $pdf->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
    $pdf->SetFont('OpenSans','',12);
    $pdf->SetLineWidth(0.1);
    $pdf->SetFontSize(15);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetTextColor(255, 88, 49);
/*INFO DE LA ORDEN*/

        $pdf->Cell(80);
// Título
        $pdf->Cell(30,10,'Información de la orden',0,0,'C');
// Salto de línea
        $pdf->Ln(10);
        $pdf->Cell(80);

        $pdf->SetFillColor(255, 88, 49);
        $pdf->Cell(30,0.5,"",0,0,'C',true);

        $pdf->Ln(10);

        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFontSize(12);


    if ($SQLNumAutorizacion != "") { 
        $pdf->SetTextColor(255, 88, 49); $pdf->Write(10,'Número de autorización: '); 
        $pdf->SetTextColor(85, 85, 85);$pdf->Write(10,$SQLNumAutorizacion);
        $pdf->Write(10,' '); $pdf->Ln(8);
    }

    
    if ($varTipoConsulta != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Tipo de consulta: '); $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$varTipoConsulta); $pdf->Ln(8); 
    }

    if ($SQLEspecialidad != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Especialidad: '); $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLEspecialidad);$pdf->Write(10,' '); $pdf->Ln(8);
    }

    
    if ($varProcedimiento != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Procedimiento: '); 
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$varProcedimiento); 
        $pdf->Ln(8); 
    }

    if ($SQLSesiones != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Número de sesiones: '); $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLSesiones);$pdf->Write(10,' '); $pdf->Ln(8);
    }

    if ($SQLPersonalAtiende != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Personal que lo atendió: '); $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLPersonalAtiende);$pdf->Write(10,' '); $pdf->Ln(8);
    }

    if ($SQLDxPrev != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Diagnóstico previo: '); 
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLDxPrev); 
        $pdf->Ln(8); 
    }

    if ($SQLDxAdicional != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Diagnóstico adicional: '); 
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLDxAdicional); 
        $pdf->Ln(8); 
    }

    if ($SQLDxNuevo != "") { 
        $pdf->SetTextColor(255, 88, 49); 
        $pdf->Write(10,'Disgnóstico nuevo: '); 
        $pdf->SetTextColor(85, 85, 85);
        $pdf->Write(10,$SQLDxNuevo); 
        $pdf->Ln(8); 
    }
}

//CONSULTAS//
//CONSULTAS//
$query = $db->query("SELECT * FROM historia_clinica WHERE documento_paciente=?s",$varDocumento);
$numrows = $db->numRows($query);

        for ($i=$numrows; $i > 0 ; $i--) {
            $row = $db->fetch($query);
            $pdf->AddPage();
            $pdf->SetFont('OpenSans','',12);
            $pdf->SetLineWidth(0.1);
            // subtitulo para cada consulta
            $FECHA = $row['fecha_hora'];
            $pdf->SetFontSize(12);
            $pdf->SetTextColor(255, 88, 49);
            $pdf->Write(10,"Consulta de la fecha ". $FECHA);
            //Separador
                $pdf->Ln(10);
                $pdf->SetFillColor(255, 88, 49);
                $pdf->Cell(0,0.1,"",0,0,'C',true);
                $pdf->Ln(5);
            //Separador

            $pdf->SetFontSize(12);
            $medicName = $db->getOne("SELECT medic_name FROM historia_clinica WHERE documento_paciente=?s",$varDocumento);
            $profesion = $db->getOne("SELECT profesion FROM usuarios WHERE username =?s",$medicName);
            $nombre = $db->getOne("SELECT real_name FROM usuarios WHERE username =?s",$medicName) ." - ". $db->getOne("SELECT nombre FROM profesiones WHERE id=?s",$profesion);
            $pdf->SetTextColor(16, 126, 224);
            $pdf->Write(10,"Profesional a cargo de la consulta: ");
            $pdf->Ln(8);
            $pdf->SetTextColor(85, 85, 85);
            $pdf->Write(10,$nombre);
            // Salto de línea
            $pdf->Ln(15);

            $pdf->SetTextColor(16, 126, 224);
            $pdf->Write(10,"Durante la consulta de la fecha (".$FECHA.") se encontró lo siguiente:\n");
            $pdf->SetTextColor(85, 85, 85);
            $comentarioMedico = $row['cometarios_medico'];
            $pdf->Write(10,$comentarioMedico);
            $pdf->Ln(15);    

            $dxPost = $row['dx_posterior'];
            if ($dxPost != "") {
                $pdf->SetTextColor(16, 126, 224);
                $pdf->Write(10,"Diagnostico de la ultima consulta: \n");
                $pdf->SetTextColor(85, 85, 85);
                $pdf->Write(10,$dxPost);
                $pdf->Ln(15);
            }

            $archivos_unidos = $row['link_archivo'];
            $archivos = explode("*+*", $archivos_unidos);

            $max = sizeof($archivos);
            if ($archivos_unidos != "") {
                $pdf->SetTextColor(16, 126, 224);
                $pdf->Write(10,"Archivos relevantes para la consulta: \n");
                $pdf->Ln(8); 
                for($j = 0; $j < $max;$j++){
                    $ext = pathinfo($archivos[$j], PATHINFO_EXTENSION);
                    if ($ext == "jpg" || $ext == "jpe" || $ext == "jpeg" || $ext == "png") {
                        $imgsrc1 = explode("archivos/", $archivos[$j]);
                        $imgsrc2 = "http://zafiroweb.co/filesManager.php?file=".$imgsrc1[1];
                        $pdf->Image($imgsrc2,50,null,100);
                        $pdf->Ln(8);
                    }elseif ($ext == "pdf" ) {
                        $imgsrc2 = "http://zafiroweb.co/img/pdf.png";
                        $pdf->Image($imgsrc2,null,null,18); 
                        $pdf->SetTextColor(16, 126, 224);
                        $pdf->Cell(20);
                        $pdf->Cell(0,-18,$archivos[$j]);  
                        $pdf->Ln(8);                   
                    }elseif ($ext == "docx" || $ext == "doc") {
                        $imgsrc2 = "http://zafiroweb.co/img/word.png";
                        $pdf->Image($imgsrc2,null,null,18); 
                        $pdf->SetTextColor(16, 126, 224);
                        $pdf->Cell(20);
                        $pdf->Cell(0,-18,$archivos[$j]);
                        $pdf->Ln(8);                      
                    }else{
                        $pdf->SetTextColor(16, 126, 224);
                        $pdf->Write(10,$archivos[$j]);
                        $pdf->Ln(8);
                    }
                    
                }
            }
            //Separador
            $pdf->Ln(10);
            $pdf->SetFillColor(236, 236, 236);
            $pdf->Cell(0,0.1,"",0,0,'C',true);
            $pdf->Ln(10);
            //Separador
        }
        $pdf->Output();
        ob_end_flush();
    }else{

        $pdf = new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->AddFont('OpenSans','','OpenSans-Regular.ttf',true);
        $pdf->SetFont('OpenSans','',12);
        $pdf->SetTextColor(255, 88, 49);
        $pdf->Cell(40,10, "documento = ");
        $pdf->Cell(40,10, $_SESSION["documento"]);
        $pdf->Cell(40,10, $varDocumento,1);

        //$pdf->Cell(40,10,'¡No has escogido ningún paciente para mostrar su historia clínica!');
        $pdf->Output();
        ob_end_flush();
    }


}else {
    $_GET["from_page"] = $actual_link;
    echo "<script> location.href='login.php?link=".$_SERVER[REQUEST_URI]."'; </script>";
    #header("Location: login.php?link="."$_SERVER[REQUEST_URI]");
}
?>

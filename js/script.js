var pos = 0;
var cantidad = 0;
var limitDer = 0;
var cantPantalla = 0;
var anchoItem = 0;
var restante = 0;

$(document).ready(function(){
	$("#loader").hide();
	
	// Mover los slides
	var cantidad = $(".item").length;
	cantPantalla = parseInt($(".carrusel").attr("lola"));
	anchoItem = (100/cantPantalla);
	anchoscroll = (100/cantPantalla*cantidad);
	restante = (cantPantalla-1)*anchoItem;
	$(".scroll").css("width",anchoscroll+"%");
	$(".item").css("width",100/cantidad+"%");
	limitDer = (cantidad-1)*(anchoItem)-restante;

	// Ancla
	$('a[href^="#"]').on('click',function (e) {
      	e.preventDefault();

      	var target = this.hash,
      	$target = $(target);

      	$('html, body').stop().animate({
          	'scrollTop': $target.offset().top
     	 	}, 900, 'swing', function () {
          	window.location.hash = target;
      	});
    });

	//Mostrar diagnostico nuevo
    $(".answer").hide();
	$(".coupon_question").click(function() {
	    if($(this).is(":checked")) {
	        $(".answer").toggle();
	    } else {
	        $(".answer").hide();
	    }
	});

    //FOR SMOOTH SCROLLING
    $('a[href*=#]').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
    && location.hostname == this.hostname) {
      var $target = $(this.hash);
      $target = $target.length && $target
      || $('[name=' + this.hash.slice(1) +']');
      if ($target.length) {
        var targetOffset = $target.offset().top;
        $('html,body')
        .animate({scrollTop: targetOffset}, 1000);
       return false;
      }
    }
  });
	
});// end document ready

// Botones del slider
$("#btn-next").click(function(){
	if (pos>-limitDer) {
		pos -= anchoItem;
		$(".scroll").css("margin-left",pos+"%");
		console.log("Holi :)");
	};
});

$("#btn-prev").click(function(){
	if (pos<0) {
		pos += anchoItem;
		$(".scroll").css("margin-left",pos+"%");
	};
});

// Hacer un acordeón
$("#acordeon-trigger").click(function(){
	$("#categoria li").slideToggle();
});


console.log(window.location);

if (document.getElementById('files')) {
	document.getElementById('files').addEventListener('change', function(e) {
	  var list = document.getElementById('filelist');
	  list.innerHTML = '';
	  for (var i = 0; i < this.files.length; i++) {
	    list.innerHTML += (i + 1) + '. ' + this.files[i].name + '\n';
	  }
	  if (list.innerHTML == '') list.style.display = 'none';
	  else list.style.display = 'block';
	});
}

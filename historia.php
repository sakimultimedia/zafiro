<?php require_once("includes/connection.php"); ?>
<?php include "header.php" ?>

<?php 
#restringir acceso---------------------------------

#Buscar acceso del usuario en la BD
#$query_acceso = mysqli_query($con,"SELECT * FROM usuarios WHERE username='".$_SESSION["session_username"]."'");
if ($_SESSION["session_type"] != 'administrador') {
	echo "<script> location.href='historia-estatica.php'; </script>";
#header("Location: historia-estatica.php");
}

//********************NEUROPSICOLOGIA*****NEUROPSICOLOGIA*****NEUROPSICOLOGIA*****NEUROPSICOLOGIA**************
//********************NEUROPSICOLOGIA*****NEUROPSICOLOGIA*****NEUROPSICOLOGIA*****NEUROPSICOLOGIA**************

if ($profesion == "1") {
    include "profesiones/neuropsicologia/historia_neuropsicologia_Part1.php";
}elseif ($profesion == "2") {
    include "profesiones/medicina_veterinaria/historia_veterinaria_Part1.php";
}

?>


<!--
// HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML
// HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML
// HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML
// HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML // HTML
--> 
<!--================================= Seccion 1 ===============================-->

<section id="change-section">
	<div class="container" style="display: flex; flex-wrap:wrap;justify-content: flex-end;">
	<!--
	<a href="nueva-historia.php">Datos Básicos</a>
	<a href="datos-procedimiento.php">Info. de la orden</a>
	<a href="historia.php">Hist. Clínica</a>
-->
<?php 
	#restringir acceso---------------------------------
if ($_SESSION["session_type"] != 'administrador') {
	echo '<a href="nueva-historia.php">Datos Básicos</a>';
}else{
	echo '<a href="nueva-historia.php">Datos Básicos</a>';
	echo '<a href="datos-procedimiento.php">Info. de la orden</a>';
	echo '<a href="historia.php" style="background-color:#FF5831">Hist. Clínica</a>';
}	
?>	
</div>
</section>

<!--================================= Seccion 2 ===============================-->

<section id="medical_history">
	<div class="container">
    <?php
    if ($profesion == "1") {
    	 include "profesiones/neuropsicologia/historia_neuropsicologia_Part2.php";
    }elseif ($profesion == "2") {
        include "profesiones/medicina_veterinaria/historia_veterinaria_Part2.php";
    }
    ?>
	<section id="diligecia-consulta">
		<!--=============DILIGENCIAR CONSULTA==================
		#================DILIGENCIAR CONSULTA==================
		#================DILIGENCIAR CONSULTA==================
		#================DILIGENCIAR CONSULTA==================
		#================DILIGENCIAR CONSULTA===============-->

		<div class="row" style="padding: 0 10px;">
			<div class="big-label">
				Datos de la consulta del día de hoy
			</div>
		</div>

		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<div class="explicacion" style="margin-top: 40px;">
					Recuerda que una vez guardes la entrada, ésta no podrá ser modificada y quedará inscrita dentro de la historia médica con su fecha y hora de envío.
				</div>
			</div>
			<div class="col-sm-2"></div>
		</div>

		<form class="ajaxform" action= "<?echo $ZafiroRute?>historia.php" method="POST" id="basic-data" mensajeExito = "Los datos básicos han sido correctamente guardados." mensajeError = "Ha habido un error y tus datos no fueron guardados. intentalo más tarde." enctype="multipart/form-data">

			<div class="row">
				<div class="col-sm-12">
					<div class="top-label">
						Información de la Consulta
					</div>
					<textarea required rows="6" name="historia_clinica" id="historia_clinica" placeholder="Escribe tu comentario aquí..."></textarea>
				</div><!-- end col -->
			</div><!-- end row -->

			<div class="row">
				<div class="col-sm-12">
					<div class="top-label" style="margin-bottom: 30px;">
						Agregar un archivo a la consulta
					</div>
					<input type="file" class="thefile" name="files[]" id="files" multiple="" />
					<pre id="filelist" style="display:none;"></pre>
				</div><!-- end col -->
			</div><!-- end row -->

			<div class="row">
				<div class="col-sm-12 theflex special">
					<div class="checkbox">
						<label>
							<input type="checkbox" class="coupon_question" name="coupon_question" value="1"/>
						</label>
					</div>


					<div class="check-label">
						Esta es la última sesión
					</div>

				</div>
			</div>

			<div class="row answer">
				<div class="col-sm-12">
					<div class="top-label">
						Cód. del diagnóstico posterior
					</div>
					<?php #$dxPost = isset_empty_and_set($_SESSION['dx_posterior']); ?>
					<input value="<?php #echo $dxPost; ?>" list="diagnosticos" name="post_dx" id="post_dx" data-minchars="1" class="awesomplete" placeholder="Busca el diagnóstico">
					<datalist id="diagnosticos">
						<?php 
						$query = $db->query("SELECT * FROM diagnosticos");
    					$numrows = $db->numRows($query);

						for ($i=1; $i <= $numrows ; $i++) { 
							$row = mysqli_fetch_array($query);
							$search = $db->getOne("SELECT nombre FROM diagnosticos WHERE id=?s",$row['id']);
							echo "<option value='".$row['id']." - ".$search."'>";
						}
						?>

					</datalist>
				</div><!-- end col -->
			</div><!-- end row -->


			<div class="row">
				<div class="col-sm-12">
					<input type="submit" name="guardar" value="Guardar" class="send-btn">
				</div>
			</div>

		</form><!-- end form -->
		<!--=============CONSULTAS VIEJAS==================
		#================CONSULTAS VIEJAS==================
		#================CONSULTAS VIEJAS==================
		#================CONSULTAS VIEJAS==================
		#================CONSULTAS VIEJAS===============-->
	</section><!-- end diligencia-->

    <?php
    if ($profesion == "1") {
        include "profesiones/neuropsicologia/historia_neuropsicologia_Part3.php";
    }elseif ($profesion == "2") {
        include "profesiones/medicina_veterinaria/historia_veterinaria_Part3.php";
    }
    ?>
    

	</div><!-- end container-->

</section>

		<section id="cambiar-paciente">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<a href="documento-id.php" class="cambio-persona" style="margin-bottom: 10px;">
							Cambiar paciente
						</a>
					</div>
				</div>
			</div>
		</section>

		<section id="imprimir">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<a href="fpdf.php" target="_blank" class="imprimir">
							Descargar / Imprimir
						</a>
					</div>
				</div>
			</div>
		</section>

<!--===========================================
===============================================
Footer -->

<?php include "footer.php" ?>

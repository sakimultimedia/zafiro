<footer>
<div class="container">
	<div class="col-sm-4">
		<div class="footer-title">
			Contacto
		</div>
		<div class="footer-sep"></div>
		<div class="footer-item">
			<a href="#" mailto="zafiro_web@gmail.com">E-mail: zafiro_web@gmail.com</a>
		</div>
		<div class="footer-item">
			Soporte técnico: 316 343 2475
		</div>
	</div>
	<div class="col-sm-4">
		<div class="footer-title">
			Mapa del sitio
		</div>
		<div class="footer-sep"></div>
		<div class="footer-item">
			<a href="#">Crear historia</a>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="footer-title">
			Otros materiales
		</div>
		<div class="footer-sep"></div>
		<div class="footer-item">
			<a href="manual.php">Manual de uso</a>
		</div>
	</div>
</div>

</footer>

<div class="orange">
	<div class="container">
		<div class="col-sm-12">
			<div class="footer-item">
				<a href="#">Desarrollado por Saki Multimedia</a>
			</div>
		</div>	
	</div>
</div>



<!-- Jquery 1.11.3  -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- Javascript stylesheet -->
<script src="js/awesomplete.min.js"></script>

<script type="text/javascript" src="js/script.js"></script>


</body>
</html>
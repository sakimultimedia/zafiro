<?php 
session_start();

date_default_timezone_set('America/Bogota');

if(1){
	error_reporting(E_ALL ^ E_NOTICE);
}else{
	error_reporting(0);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0" />
	
	<title>Zafiro</title>


	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/zafiro-icon.png">

	<!-- Awesome font -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

	<!-- Print bootstrap option -->
	<link rel="stylesheet" type="text/css" media="print" href="bootstrap.min.css">
	
	<!-- Awesomecomplete css -->
	<link rel="stylesheet" type="text/css" href="css/awesomplete.css" />

	<!-- Hoja de estilos -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<!-- PHP SCRIPTS -->
<?php 
require ("includes/connection.php");
include ("utils/scripts.php"); 
?>
<script type="text/javascript">
	//dont reload
function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && ((node.type=="text") || (node.type=="number") || (node.type=="date") || (node.type=="radio")  || (node.type=="checkbox")) )  {return false;}
}

document.onkeypress = stopRKey;

    //desabilitar el boton de atras 
    (function (global) { 

        if(typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";

            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {            
            noBackPlease();

            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };          
        }

    })(window);
</script>

<!-- LOADER -->
<div id="loader">
	<div class="loader-img">
		<img src="img/loading.gif" alt="Loader"/>
	</div>
</div>

<?php 

//$currentCookieParams = session_get_cookie_params(); 

$rootDomain = 'zafiroweb.co/'; 
$ZafiroRute = 'http://zafiroweb.co/';

	#$lifetime=3600;
  	#session_set_cookie_params($lifetime);
$varSessionUsername = $_SESSION["session_username"];

if(!isset($varSessionUsername)) {

	echo "<script> location.href='login.php'; </script>";	
		#header("location:login.php");
} else {
    $varSessionBoss = $db->getOne("SELECT boss FROM usuarios WHERE username=?s", $_SESSION['session_username']);

    $profesion = $db->getOne("SELECT profesion FROM usuarios WHERE username=?s", $_SESSION['session_username']);

    $nombreProfesion = $db->getOne("SELECT nombre FROM profesiones WHERE id=?s", $profesion);
    /*
	$varSessionBoss = mysqli_query($con,"SELECT * FROM usuarios WHERE username='".$_SESSION['session_username']."'")->fetch_object()->boss;

	$profesion = mysqli_query($con,"SELECT * FROM usuarios WHERE username='".$_SESSION['session_username']."'")->fetch_object()->profesion;

	$nombreProfesion = mysqli_query($con,"SELECT * FROM profesiones WHERE id='".$profesion."'")->fetch_object()->nombre;
    */

	//cambiar la url de la pagina para ocultar la direccion real
echo '<script type="text/javascript">window.history.pushState("", "", "http://zafiroweb.co/ZafiroWebPlatform-'.$nombreProfesion.'");</script>';
?>


	<body>
		<header>
			<nav class="navbar navbar-default">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">
						<? 
                        $nombre = $db->getOne("SELECT real_name FROM usuarios WHERE username=?s", $_SESSION["session_username"]);
						//$nombre = mysqli_query($con,"SELECT * FROM usuarios WHERE username='".$_SESSION["session_username"]."'")->fetch_object()->real_name;
						$tmp = explode(" ", $nombre);
						$nombre = ($tmp[0] != "") ? $tmp[0] : $tmp[1];
						?>
							<div class="bienvenido">Hola, <span><?php echo $nombre; ?></span></div>
							<!--<p><a href="logout.php">Finalice</a> sesión aquí!</p>-->
						</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<!--<li><a href="proximamente.php">Citas</a></li>-->
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Historia<span class="caret"></span></a>
								<ul class="dropdown-menu">
							<!--
								<li><a href="documento-id.php">Nueva historia</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="documento-id.php">Buscar Historia</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="historia-estatica.php">Estatica</a></li>
							-->
							<?php 
								#restringir acceso---------------------------------
							if ($_SESSION["session_type"] != 'administrador') {
								echo '<li><a href="documento-id.php">Nuevo paciente</a></li>';
								echo '<li role="separator" class="divider"></li>';
								echo '<li><a href="buscar-historia.php">Ver historia clínica</a></li>';
								echo '<li role="separator" class="divider"></li>';
								echo '<li><a href="buscar-historia.php?go=nueva-historia.php&reason=para editar los datos basicos">Editar datos del paciente</a></li>';
                                echo '<li role="separator" class="divider"></li>';
                                echo '<li><a href="buscar-historia.php?go=info-orden.php&reason=para editar la información de la orden">Editar información de la orden</a></li>';
							}else{
								echo '<li><a href="documento-id.php">Nuevo paciente</a></li>';
								echo '<li role="separator" class="divider"></li>';
								echo '<li><a href="buscar-historia.php?reason=para editar la historia clinica">Editar historia clínica</a></li>';
								echo '<li role="separator" class="divider"></li>';
								echo '<li><a href="buscar-historia.php?go=historia-estatica.php&reason=para imprimir la historia clinica">Imprimir historia</a></li>';
							}	
							?>
						</ul>
					</li>
						<!--<li><a href="proximamente.php">Facturación</a></li>
						<li><a href="proximamente.php">Ingresos</a></li>
						<li><a href="proximamente.php">Reportes</a></li>-->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cuenta<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="logout.php">Cerrar Sesión</a></li>
							</ul>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</header>

	<?php } ?>

<?php session_start(); ?>
<?php 
    #echo "redirigiendo a..."."<br>";
    $file = "archivos/".$_GET["file"];
    
    #readfile("archivos/".$_GET["file"]);

    if (file_exists($file)) {
    $ext = pathinfo($_GET["file"], PATHINFO_EXTENSION);
    switch ($ext) {
    case 'pdf':
        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename='PDF'");
        @readfile($file);
        break;
    case 'jpg':
        header('Content-type: image/jpg');
        header("Content-Disposition: inline; filename='JPG'");
        break;
    case 'jpeg':
        header('Content-type: image/jpg');
        header("Content-Disposition: inline; filename='JPG'");
        break;
    case 'png':
        header('Content-type: image/png');
        header("Content-Disposition: inline; filename='PNG'");
        break;
    default:
        header("Content-Disposition: attachment; filename='$file'"); 
        #code to be executed if $ext is different from all labels;
    }

    #pude mejorar para quiza abrir desde el naegador
    readfile($file);
    exit;
}
 ?>
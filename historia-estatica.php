<?php require_once("includes/connection.php"); ?>
<?php include "header.php" ?>

<?php 
    if ($profesion == "1") {
   	 	#variables de la pagina
        include "profesiones/neuropsicologia/historia-estatica_neuropsicologia_Part1.php";
    }elseif ($profesion == "2") {
        include "profesiones/medicina_veterinaria/historia-estatica_veterinaria_Part1.php"; #revisar esto
    }
?>

<!--===========================================
===============================================
Datos del paciente -->

<section id="medical_history">
	<div class="container-fluid" style="margin-top:-70px;">
		<div class="row">
			<div class="col-sm-12" style="background-color: #272727;">
				<div class="section-label-estatica">
					Historia Clínica
				</div>
				<div class="title-separador-estatica"></div>
			</div><!-- end col -->
		</div>
	</div>

	<?php 
    if ($profesion == "1") {
   	 	#Todo el resto... xD
        include "profesiones/neuropsicologia/historia-estatica_neuropsicologia_Part2.php";
    }elseif ($profesion == "2") {
        include "profesiones/medicina_veterinaria/historia-estatica_veterinaria_Part2.php";
    }
	?>

</section>



<section id="imprimir">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<a href="fpdf.php" target="_blank" class="imprimir">
					Descargar / Imprimir
				</a>
			</div>
		</div>
	</div>
</section>

 <!--===========================================
===============================================
Footer -->

<?php include "footer.php" ?>


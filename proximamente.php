<?php include "header.php" ?>

<!--===========================================
===============================================
Seccion 1 -->
<div class="container" style="padding-top:50px;padding-bottom:50px;">
	<div class="row">
		<div class="col-sm-12">
			<div class="section-label">
				Muy pronto estará disponible esta sección
			</div>
			<div class="title-separador"></div>
		</div><!-- end col -->
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="explicacion">
				Estimado cliente, estamos trabajando sobre esta sección para buscar su comodidad. Agradecemos su interés y esperamos que pueda tener acceso a ella muy pronto.
			</div>
			<div class="explicacion" style="margin-top: 30px; text-align: right; font-style: italic;">
				- Equipo de <b>Desarrollo</b>
			</div>
		</div>
	</div>
</div>

 <!--===========================================
===============================================
Footer -->

<?php include "footer.php" ?>
<?php require_once("includes/connection.php"); ?>
<?php include "header.php" ?>


<?php 
if(isset($_POST['continuar-buscar-nombre'])){
	if (!empty($_POST["nombre_buscar"])) {
		$error_nombre = buscarPaciente($db, 'nombre_buscar', $varSessionBoss);
	}else{
		$error_nombre = "Escribe un nombre para realizar la busqueda!";
	}
}

if(isset($_POST['continuar-buscar-id'])){
	if (!empty($_POST["documento_buscar"])) {
		$error_documento = buscarPaciente($db, 'documento_buscar', $varSessionBoss);
	}else{
		$error_documento = "Escribe un numero de documento para realizar la busqueda!";
	}
}
?>

<!--===========================================
===============================================
Buscar por id o nombre-->
<?php
if (isset($_GET['reason'])) {
         $reason  = " ".$_GET['reason']; 
}else{
    $reason  = " para ver la historia clinica";
}
?>
<section id="documento-id">
	<div class="container">
		<div class="col-sm-12">
			<div class="section-label">
				<?php echo "Buscar paciente".$reason; ?> 
			</div>
			<div class="title-separador"></div>
		</div><!-- end col -->
		
		<div class="row">
			<div class="col-sm-12">
				<div class="explicacion">
					Busca la historia que necesitas buscando el <b>Nombre y Apellido</b> del paciente
				</div>
			</div>
		</div><?php
		if (isset($_GET['go'])) {
				 $action  = $ZafiroRute."buscar-historia.php"."?go=".$_GET['go']; 
		}else{
			$action  = $ZafiroRute."buscar-historia.php";
		}
		?>
		<form class="ajaxform" action= "<? echo $action ?>" method="POST" id="basic-data">

			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<div class="top-label">
						Nombre del paciente
					</div>
					<!-- <input type="text" name="nombre_buscar" id="nombre_buscar" placeholder="Busca la historia por el nombre"> -->
					<input list="nomPaciente" name="nombre_buscar" id="nombre_buscar"  class="awesomplete" data-minchars="1" placeholder="Busca la historia por el nombre">
					<datalist id="nomPaciente">
						<?php 

						$query = $db->query("SELECT * FROM pacientes WHERE uploaded_by=?s",$varSessionBoss);
						$numrows = $db->numRows($query);
						
						for ($i=1; $i <= $numrows ; $i++) { 
							$row = $db->fetch($query);
							$search = $db->getOne("SELECT nombre_completo FROM pacientes WHERE documento=?s",$row['documento']);
							echo "<option value='".$search."'>";
						}
						?>
					</datalist>
				</div><!-- end col -->
				<div class="col-sm-3"></div>
			</div><!-- end row -->

			<div class="row">
				<div class="col-sm-12" style="text-align:center;">
					<input type="submit" value="Buscar por nombre" class="send-btn" name="continuar-buscar-nombre" style="width:300px;">
				</div>
			</div>

			<div class="col-sm-12">
				<div class="error-label">
					<?php if (!empty($error_nombre)) {echo "<p class=\"error\">" . "Mensaje: ". $error_nombre . "</p>";} ?>
					</div>
				</div><!-- end col -->

			</form><!-- end form -->

			<div class="separador"></div>

			<div class="row">
				<div class="col-sm-12">
					<div class="explicacion">
						También puedes buscarlo pos su <b>Documento de Identidad</b> si no tienes su nombre
					</div>
				</div>
			</div>


			<form class="ajaxform" action= "<? echo $action ?>" method="POST" id="basic-data">

				<div class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						<div class="top-label">
							Documento de identidad
						</div>
						<!--<input type="text" name="documento_buscar" id="documento_buscar" placeholder="Busca la historia por el número de documento">-->

						<input list="docIdentidad" name="documento_buscar" id="documento_buscar"  class="awesomplete" data-minchars="1" placeholder="Busca la historia por el número de documento">
						<datalist id="docIdentidad">
							<?php 
							$query = $db->query("SELECT * FROM pacientes WHERE uploaded_by=?s",$varSessionBoss);
							$numrows = $db->numRows($query);

							for ($i=1; $i <= $numrows ; $i++) { 
								$row = $db->fetch($query);
								$search = $db->getOne("SELECT documento FROM pacientes WHERE documento=?s",$row['documento']);
								echo "<option value='".$search."'>";
							}
							?>
						</datalist>
					</div><!-- end col -->
					<div class="col-sm-3"></div>
				</div><!-- end row -->

				<div class="row">
					<div class="col-sm-12" style="text-align:center;">
						<input type="submit" value="Buscar por documento" class="send-btn" name="continuar-buscar-id" style="width:300px;">
					</div>
				</div>

				<div class="col-sm-12">
					<div class="error-label">
						<?php if (!empty($error_documento)) {echo "<p class=\"error\">" . "Mensaje: ". $error_documento . "</p>";} ?>
						</div>
					</div><!-- end col -->

				</form><!-- end form -->
			</div>

		</section>

 <!--===========================================
===============================================
Footer -->

<?php include "footer.php" ?>

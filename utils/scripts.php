<?php 
//some utils
function calcular_edad($fechanacimiento){
    list($ano,$mes,$dia) = explode("-",$fechanacimiento);
    $ano_diferencia  = date("Y") - $ano;
    $mes_diferencia = date("m") - $mes;
    $dia_diferencia   = date("d") - $dia;
    if ($dia_diferencia <= 0 && $mes_diferencia <= 0)
        $ano_diferencia--;
    return $ano_diferencia;
}

function today_date(){
    $year = date("Y");
    $month = date("m");
    $day = date("d");
    return "$year-$month-$day";
}

function isset_and_empty($var){
    if (isset($var)) {
        $trimedVar = trim($var);
        return !empty($trimedVar);
    }else{
        return false;
    }
}

function isset_empty_and_set($var){
    if (isset($var)) {
        $trimedVar = trim($var);
        if(!empty($trimedVar)){
            return $trimedVar;
        }
    }else{      
        return "";
    }
}

function isset_empty_and_set_NULL($var){
    if (isset($var)) {
        $trimedVar = trim($var);
        if(!empty($trimedVar)){
            return $trimedVar;
        }
    }else{      
        return NULL;
    }
}

function empty_and_set($var){
    $trimedVar = trim($var);
    if(!empty($trimedVar)){
        return $trimedVar; 
    }else{      
        return "";
    }
}

function isset_and_set($var){
    if (isset($var)) {
        return $var;
    }
}

function Arreglar_String($var){
    if (isset($var)) {
        return ucwords (strtolower(trim($var)));
    }else{
        return null;
    }
}

function empty_pdf($pdf,$string,$var,$ln){
    if (isset($var)) {
        $trimedVar = trim($var);
        if(!empty($trimedVar)){
            $pdf->SetTextColor(255, 88, 49);
            $pdf->Write(10,$string);
            $pdf->SetTextColor(85, 85, 85);
            $pdf->Write(10,$var);
            $pdf->Write(10,' '); 
            $pdf->Ln($ln);
        }
    }
}

function put_dots($val){
    $array = str_split($val);
    $count = 0;
    $txt = "";
    $count2 = 0;
    $dot = true;
    for ($i=count($array); $i > 0; $i--) { 
        $count++;
        
        if ($count%3 == 1 && $count2 > 0 && $i <= 3) {
            $txt = $txt."'"; 
            $dot = false;
        }
        if ($count%3 == 1 && $count > 1 && $dot ) {
            $txt = $txt.".";
            $count2++;
        }
        $txt = $txt.$array[$i-1];
    }
    return strrev($txt);
}

function buscarPaciente($db, $inputName, $varSessionBoss){
    #$_SESSION["input2"] = mysqli_query($con,"SELECT * FROM pacientes WHERE nombre_completo='".$_POST[$inputName]."'")->fetch_object()->documento;  
    if (!empty($_POST[$inputName])) {

        if ($inputName == 'documento_buscar') {
            $varDocumento = $_POST[$inputName];
            #$_SESSION["input"] = 'documento por id = '.$varDocumento;
        }
        if ($inputName == 'nombre_buscar') {

            $id_por_nombre = $db->query("SELECT * FROM pacientes WHERE nombre_completo=?s",$_POST[$inputName]);
            $numrows1= $db->numRows($id_por_nombre);

            if($numrows1==1)
            { 
                $row1 = $db->fetch($id_por_nombre);
                $varDocumento = $row1['documento'];
            }else{
                $varDocumento = "";
            }
            #$_SESSION["input"] = 'documento por name = '.$varDocumento;
        }

        
        $query = $db->query("SELECT * FROM pacientes WHERE documento=?s AND uploaded_by=?s",$varDocumento,$varSessionBoss);
        $numrows = $db->numRows($query);

        if($numrows==1)
        {   
            $row = $db->fetch($query);
            $_SESSION['documento'] = $varDocumento;
                #echo "session EXISTENTE llamada: ".$_SESSION['documento'];
                #guardar datos del paciente para mostrarse en otras paginas
            $_SESSION['tipoDocumento'] = $row1['tipo_de_documento'];
            $_SESSION['Nombre_1'] = $row1['primer_nombre'];
            $_SESSION['Nombre_2'] = $row1['segundo_nombre'];
            $_SESSION['Apellido_1'] = $row1['primer_apellido'];
            $_SESSION['Apellido_2'] = $row1['segundo_apellido'];
            $_SESSION['nacimiento'] = $row1['fecha_nacimiento'];
            $_SESSION['Edad'] = $row1['edad'];
            $_SESSION['Genero'] = $row1['sexo'];
            $_SESSION['EstadoCivil'] = $row1['estado_civil'];
            $_SESSION['Dir_1'] = $row1['direccion_1'];
            $_SESSION['Dir_2'] = $row1['direccion_2'];
            $_SESSION['Telefono_paciente'] = $row1['telefono'];
            $_SESSION['Celular_paciente'] = $row1['celular'];
            $_SESSION['Ciudad_paciente'] = $row1['ciudad'];
            $_SESSION['Medico'] = $row1['nombre_medico'];
            $_SESSION['Entidad'] = $row1['entidad'];
            $_SESSION['Regimen'] = $row1['regimen'];
            $_SESSION['Remitido'] = $row1['remitido_por'];
            $_SESSION['AcompananteNombre'] = $row1['acompanante'];
            $_SESSION['AcompananteTel'] = $row1['telefono_acomp'];
            $_SESSION['acompananteParentezco'] = $row1['parentesco_acomp'];
            $_SESSION['Mail'] = $row1['email'];
            $_SESSION['Origen'] = $row1['origen'];
            $_SESSION['Nacionalidad'] = $row1['nacionalidad'];
            $_SESSION['Ocupacion'] = $row1['ocupacion'];
            $_SESSION['Empresa'] = $row1['empresa'];
            $_SESSION['ConyugueNombre'] = $row1['conyugue'];
            $_SESSION['ConyugueDocumento'] = $row1['documento_conyugue'];
            $_SESSION['ConyugueNacimiento'] = $row1['fecha_nacimiento_conyugue'];
            $_SESSION['ConyugueDireccion'] = $row1['direccion_conyugue'];
            $_SESSION['ConyugueTelefono'] = $row1['telefono_conyugue'];
            $_SESSION['Hijo1Nombre'] = $row1['nombre_hijo1'];
            $_SESSION['Hijo1Sexo'] = $row1['sexo_hijo1'];
            $_SESSION['Hijo1Edad'] = $row1['edad_hijo1'];
            $_SESSION['Hijo2Nombre'] = $row1['nombre_hijo2'];
            $_SESSION['Hijo2Sexo'] = $row1['sexo_hijo2'];
            $_SESSION['Hijo2Edad'] = $row1['edad_hijo2'];
            $_SESSION['Padre'] = $row1['padre'];
            $_SESSION['PadreEdad'] = $row1['edad_padre'];
            $_SESSION['Madre'] = $row1['madre'];
            $_SESSION['MadreEdad'] = $row1['edad_madre'];
            $_SESSION['Hobbie'] = $row1['hobbie'];
            $_SESSION['Etnia'] = $row1['etnia'];
            $_SESSION['Dominancia'] = $row1['dominancia'];
            $_SESSION['Region'] = $row1['region'];
            $_SESSION['Deporte'] = $row1['deporte'];
            $_SESSION['Colegio'] = $row1['colegio'];
            $_SESSION['Escolaridad'] = $row1['escolaridad'];
            $_SESSION['Rendimiento'] = $row1['rendimiento'];

            if ($_GET['go'] != "") {
                echo "<script> location.href='".$_GET['go']."'; </script>";
            }else{
                echo "<script> location.href='historia.php'; </script>";
            }

            $error_documento = "";
            $error_nombre = "";
        } else {

            if ($inputName == 'documento_buscar') {
                $error_documento = "No se encontro ningun paciente con ese documento";
                return $error_documento;
            }
            if ($inputName == 'nombre_buscar') {
                $error_nombre = "No se encontro ningun paciente con ese nombre";
                return $error_nombre;
            }
        }
    }
}

// function alert($msg){
//     echo "<script type='text/javascript'>alert(Error\n$msg);</script>";
// }

// version PHP
function create_popup($cont){
    echo '<div id="dialog-box-cover"><div class="dialog-box"><div class="close-popup" onclick="close_popup()"><i class="fa fa-times" aria-hidden="true"></i></div><div class="content-wrapper">'. $cont .'</div></div><div id="close-popup-background" class="close-popup" onclick="close_popup()" ></div></div>';
}

 ?>

<script type="text/javascript">

    // Version JS
    function create_popup(cont){
        $("body").append('<div id="dialog-box-cover"><div class="dialog-box"><div class="close-popup" onclick="close_popup()"><i class="fa fa-times" aria-hidden="true"></i></div><div class="content-wrapper">'+ cont +'</div></div></div>');
    }
    
    function close_popup(){
        $("#dialog-box-cover").remove();
    }

    function redirect_to_info_orden(){
        location.href='info-orden.php';
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
</script>
